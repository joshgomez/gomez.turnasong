﻿namespace Gomez.TurnASong.Configurations
{
    public static class RouteContant
    {
        public const string API = "api/";
        public const string API_MULTIMEDIA = API + "multimedia/";
        public const string API_IDENTITY = API + "identity/";
    }
}
