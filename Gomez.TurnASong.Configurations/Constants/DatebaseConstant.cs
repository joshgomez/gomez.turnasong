﻿namespace Gomez.TurnASong.Configurations.Constants
{
    public static class DatebaseConstant
    {
        public const string DEFAULT_CONNECTION = "DefaultConnection";
        public const string DATABASE = "TurnASong";

        public const int TOTAL_COLLECTIONS = 3;
        public const string COLLECTION_IDENTITIES = "identities";
        public const string COLLECTION_APPLICATION = "application";
        public const string COLLECTION_LEASES = "leases";
    }
}
