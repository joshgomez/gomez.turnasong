﻿namespace Gomez.Core.Multimedia
{
    public class MediaSearchQuery
    {
        public MediaSearchQuery()
        {
            Order = 0;
            MaxResults = 50;
            Type = "video";
        }

        /// <summary>
        /// Default type is video
        /// </summary>
        public string Type { get; set; }
        public string Query { get; set; }
        public int? Order { get; set; }
        public int MaxResults { get; set; }
        public DurationType VideoDuration { get; set; }
    }
}
