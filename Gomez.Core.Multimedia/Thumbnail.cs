﻿namespace Gomez.Core.Multimedia
{
    public class Thumbnail : IThumbnail
    {
        public string StandardUrl { get; set; }
        public string MediumUrl { get; set; }
        public string HighUrl { get; set; }
    }
}
