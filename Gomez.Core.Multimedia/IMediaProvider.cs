﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.Core.Multimedia
{
    public interface IMediaProvider
    {
        Task<IVideo<IThumbnail>> GetVideoByIdAsync(string id);
        Task<IEnumerable<IVideo<IThumbnail>>> SearchAsync(MediaSearchQuery searchTerms);
    }
}
