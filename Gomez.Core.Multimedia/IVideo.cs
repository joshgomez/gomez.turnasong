﻿namespace Gomez.Core.Multimedia
{
    public interface IVideo<T> where T : IThumbnail
    {
        string Id { get; set; }
        double DurationAsSeconds { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        T Thumbnails { get; set; }
    }
}
