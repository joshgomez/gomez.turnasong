﻿namespace Gomez.Core.Multimedia
{
    public enum DurationType
    {
        Any,
        Short,
        Medium,
        Long,
    }
}
