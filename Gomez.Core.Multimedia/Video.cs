﻿namespace Gomez.Core.Multimedia
{
    public class Video : Video<IThumbnail>
    {

    }

    public class Video<T> : IVideo<T> where T : IThumbnail
    {
        public string Id { get; set; }
        public double DurationAsSeconds { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public T Thumbnails { get; set; }
    }
}
