﻿namespace Gomez.Core.Multimedia
{
    public interface IThumbnail
    {
        string StandardUrl { get; set; }
        string MediumUrl { get; set; }
        string HighUrl { get; set; }
    }
}
