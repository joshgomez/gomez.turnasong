﻿using Gomez.TurnASong.Models.Areas.Identity;
using Newtonsoft.Json;

namespace Gomez.TurnASong.Models.Areas.Common
{
    public class UserIdentity
    {
        public UserIdentity()
        {

        }

        public UserIdentity(ApplicationUser user)
        {
            Id = user.Id;
            Name = user.Nickname;
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
