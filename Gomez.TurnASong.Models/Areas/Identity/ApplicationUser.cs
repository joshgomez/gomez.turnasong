﻿using Newtonsoft.Json;

namespace Gomez.TurnASong.Models.Areas.Identity
{
    /// <summary>
    /// Add properties releated to the current project here
    /// </summary>
    public class ApplicationUser : AspNetCore.Identity.Cosmos.Models.DocumentDbIdentityUser<ApplicationRole>
    {
        public ApplicationUser() : base()
        {

        }

        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }

        [JsonProperty(PropertyName = "roomId")]
        public string RoomId { get; set; }
    }
}
