﻿using Gomez.Core.DocumentDb;
using Newtonsoft.Json;

namespace Gomez.TurnASong.Models.Areas.Identity
{
    /// <summary>
    /// Add properties releated to the current project here
    /// </summary>
    public class ApplicationUserData : Core.DocumentDb.DefaultDocumentEntity, IPartitionedDocumentEntity
    {
        public ApplicationUserData() : base()
        {
            Init();
        }

        public void Init()
        {
            Experience = 0;
            Level = 1;
        }

        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }

        [JsonProperty(PropertyName = "experience")]
        public long Experience { get; set; }

        [JsonProperty(PropertyName = "level")]
        public int Level { get; set; }

        private string _partitionKey;

        /// <summary>
        /// PartitionKey
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = Id;
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}
