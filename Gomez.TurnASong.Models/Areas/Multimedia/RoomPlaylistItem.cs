﻿using Gomez.TurnASong.Models.Areas.Multimedia.RoomModels;
using Newtonsoft.Json;
using System;

namespace Gomez.TurnASong.Models.Areas.Multimedia
{
    public class RoomPlaylistItem : Core.DocumentDb.DefaultDocumentEntity
    {
        public RoomPlaylistItem()
        {
            Id = Guid.NewGuid().ToString();
            CreatedAt = DateTime.UtcNow;
            Type = this.GetType().Name;
        }

        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "roomId")]
        public string RoomId { get; set; }

        [JsonProperty(PropertyName = "media")]
        public RoomMedia Media { get; set; }

        private string _partitionKey;

        /// <summary>
        /// PartitionKey
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = RoomId;
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}
