﻿using Newtonsoft.Json;
using System;

namespace Gomez.TurnASong.Models.Areas.Multimedia
{
    public class RoomUserState : Core.DocumentDb.DefaultDocumentEntity
    {
        public RoomUserState()
        {
            CreatedAt = DateTime.UtcNow;
            Type = this.GetType().Name;
        }

        public RoomUserState(string userId, string roomId) : this()
        {
            Id = $"{userId}_{roomId}_state";
        }

        [JsonProperty(PropertyName = "mediaPlayerState")]
        public MediaPlayerStateType MediaPlayerState { get; set; }

        private string _partitionKey;

        /// <summary>
        /// PartitionKey
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null && Id?.Contains("_") == true)
                {
                    _partitionKey = Id.Split("_")[1];
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}
