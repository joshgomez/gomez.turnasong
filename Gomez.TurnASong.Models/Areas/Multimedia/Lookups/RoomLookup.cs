﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Models.Areas.Multimedia.RoomModels;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using Gomez.Core.Web.JsonConverters.Newtonsoft;

namespace Gomez.TurnASong.Models.Areas.Multimedia.Lookups
{
    public class RoomLookup : DocumentEntity, IPartitionedDocumentEntity, ITypedDocumentEntity
    {
        public RoomLookup()
        {
            PartitionKey = nameof(RoomLookup);
            Type = this.GetType().Name;
        }

        public RoomLookup(Room entity) : this()
        {
            Id = $"{entity.Id}_lookup";
            Name = entity.Name;
        }

        [JsonProperty(PropertyName = "currentMedia")]
        public RoomCurrentMedia CurrentMedia { get; set; }

        [MaxLength(256)]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "normalizedName")]
        public string NormalizedName => Name.ToUpperInvariant();

        [JsonProperty(PropertyName = "onlineCount")]
        public int OnlineCount { get; set; }

        /// <summary>
        /// PartitionKey
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey { get; set; }

        [JsonConverter(typeof(UnixDateTimeJsonConverter))]
        [JsonProperty(PropertyName = "_ts")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
    }
}
