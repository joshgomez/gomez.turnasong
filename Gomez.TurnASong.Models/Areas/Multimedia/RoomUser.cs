﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.TurnASong.Models.Areas.Multimedia
{
    public class RoomUser : Core.DocumentDb.DefaultDocumentEntity
    {
        public RoomUser()
        {
            CreatedAt = DateTime.UtcNow;
            Type = this.GetType().Name;
        }

        public RoomUser(string userId, string roomId) : this()
        {
            Id = $"{userId}_{roomId}";
        }

        [NotMapped]
        [JsonIgnore]
        public string UserId => this.Id?.Split('_')[0] ?? null;


        [JsonProperty(PropertyName = "nickname")]
        public string Nickname { get; set; }

        [JsonProperty(PropertyName = "isActive")]
        public bool IsActive { get; set; }

        private string _partitionKey;

        /// <summary>
        /// PartitionKey, the room id
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null && Id?.Contains("_") == true)
                {
                    _partitionKey = Id.Split("_")[1];
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}
