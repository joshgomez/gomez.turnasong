﻿using Gomez.Core.Multimedia;
using Newtonsoft.Json;

namespace Gomez.TurnASong.Models.Areas.Multimedia.RoomModels
{
    public class RoomMedia : IVideo<RoomMediaThumbnail>
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "durationAsSeconds")]
        public double DurationAsSeconds { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "thumbnails")]
        public RoomMediaThumbnail Thumbnails { get; set; }
    }
}
