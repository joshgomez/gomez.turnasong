﻿using Gomez.Core.Multimedia;
using Newtonsoft.Json;

namespace Gomez.TurnASong.Models.Areas.Multimedia.RoomModels
{
    public class RoomMediaThumbnail : IThumbnail
    {
        [JsonProperty(PropertyName = "standardUrl")]
        public string StandardUrl { get; set; }

        [JsonProperty(PropertyName = "mediumUrl")]
        public string MediumUrl { get; set; }

        [JsonProperty(PropertyName = "highUrl")]
        public string HighUrl { get; set; }
    }
}
