﻿using AutoMapper;
using Gomez.TurnASong.Models.Areas.Common;
using Newtonsoft.Json;
using System;

namespace Gomez.TurnASong.Models.Areas.Multimedia.RoomModels
{
    public class RoomCurrentMedia : RoomMedia
    {
        private static readonly IMapper mapper = new MapperConfiguration(cfg => {
            cfg.CreateMap<RoomMedia, RoomCurrentMedia>();
        }).CreateMapper();

        [JsonProperty(PropertyName = "startTime")]
        public DateTime? StartTime { get; set; }

        [JsonProperty(PropertyName = "user")]
        public UserIdentity User { get; set; }

        public RoomCurrentMedia(RoomMedia media)
        {
            mapper.Map(media, this);
        }
    }
}
