﻿using System.ComponentModel.DataAnnotations;

namespace Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Room
{
    public class RoomListItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public int OnlineCount { get; set; }
    }
}
