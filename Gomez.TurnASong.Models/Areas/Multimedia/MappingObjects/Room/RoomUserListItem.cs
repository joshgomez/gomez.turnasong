﻿namespace Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Room
{
    public class RoomUserListItem
    {
        public string Id { get; set; }
        public string NickName { get; set; }
    }
}
