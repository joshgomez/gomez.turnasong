﻿namespace Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Media
{
    public class PlaylistItem
    {
        public PlaylistItem()
        {

        }

        public string Id { get; set; }
        public string VideoId { get; set; }
        public string Title { get; set; }
        public double DurationAsSeconds { get; set; }
        public string Thumbnail { get; set; }

        public string UserId { get; set; }
        public string Nickname { get; set; }
    }
}
