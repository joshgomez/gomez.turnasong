﻿namespace Gomez.TurnASong.Models.Areas.Multimedia
{
    public enum MediaPlayerStateType
    {
        NotReady = 0,
        Ready,
        Ended
    }
}
