﻿using Gomez.Core.DocumentDb;
using Gomez.Core.Multimedia;
using Gomez.TurnASong.Models.Areas.Common;
using Gomez.TurnASong.Models.Areas.Multimedia.RoomModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gomez.TurnASong.Models.Areas.Multimedia
{
    public class Room : Core.DocumentDb.DefaultDocumentEntity, IPartitionedDocumentEntity
    {
        public Room()
        {
            CreatedAt = DateTime.UtcNow;
            Type = this.GetType().Name;
        }

        [JsonProperty(PropertyName = "currentMedia")]
        public RoomCurrentMedia CurrentMedia { get; set; }

        [MaxLength(256)]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "owner")]
        public UserIdentity Owner { get; set; }

        [JsonProperty(PropertyName = "videoDuration")]
        public DurationType VideoDuration { get; set; }

        private string _partitionKey;

        /// <summary>
        /// PartitionKey
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = Id;
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}
