﻿using Gomez.TurnASong.BusinessLogic.Services.Areas.Identity;
using Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Common;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Gomez.TurnASong.Socket.Hubs.Shared;
using Gomez.TurnASong.ViewModels.Areas.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Socket.Hubs
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RoomHub : MainHub<IRoomConnectionMapping>
    {
        private readonly IStringLocalizer<RoomHub> @string;

        public RoomHub(IServiceScopeFactory factory, IStringLocalizer<RoomHub> localizer) : base(factory,HubConstant.HUBNAME_ROOM)
        {
            @string = localizer;
        }

        protected (IServiceScope scope, IRoomService roomService, IServiceProvider services) CreateRoomService()
        {
            var (scope, services) = _helper.CreateService();
            return (scope, services.GetRequiredService<IRoomService>(), services);
        }

        public static string GetDefaultGroupId(string roomId)
        {
            return $"{HubConstant.HUBNAME_ROOM}_{roomId}";
        }

        public async Task AddMediaToQueueAsync(string videoId)
        {
            var (scope, roomService, _) = CreateRoomService();
            using (scope)
            {
                var (room, user) = await roomService.AddMediaToQueueAsync(
                    this.Context.UserIdentifier, 
                    videoId);

                if(room != null)
                {
                    var groupId = GetDefaultGroupId(room.Id);
                    await Clients.Group(groupId)
                        .SendAsync("roomUpdated", room);

                    await NewMessage(user, groupId, @string["Have added a video to the queue."]);
                }
            }
        }

        public async Task UserMediaIsReady()
        {
            var (scope, roomService, services) = CreateRoomService();
            using (scope)
            {
                var connectionMapping = services.GetRequiredService<IRoomConnectionMapping>();
                var activeUsers = await connectionMapping.CountAsync(true);
                var (room,user) = await roomService.SetReadyAsync(activeUsers, this.Context.UserIdentifier);
                if(room != null)
                {
                    var groupId = GetDefaultGroupId(room.Id);
                    await Clients.Group(groupId)
                        .SendAsync("mediaUpdated", room, MediaPlayerStateType.Ready);
                    await NewMessage(room.CurrentMedia.User, groupId, @string["Is now playing {0}.",room.CurrentMedia.Title]);
                }
            }
        }

        public async Task UserMediaHasEnded()
        {
            var (scope, roomService, services) = CreateRoomService();
            using (scope)
            {
                var connectionMapping = services.GetRequiredService<IRoomConnectionMapping>();
                var activeUsers = await connectionMapping.CountAsync(true);
                var (room,_) = await roomService.SetEndedAsync(activeUsers, this.Context.UserIdentifier);
                if (room != null)
                {
                    await Clients.Group(GetDefaultGroupId(room.Id))
                        .SendAsync("mediaUpdated", room, MediaPlayerStateType.Ended);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="caller">Refresh only for the caller</param>
        /// <returns></returns>
        private async Task RefreshUserListAsync(IRoomUserRepository repo, RoomUser user, bool caller)
        {
            var users = await repo.GetAllActiveUsersInRoomAsync(user.PartitionKey).ToArrayAsync();
            if (caller)
            {
                await Clients.Caller
                    .SendAsync("userListRefreshed", users);
                return;
            }

            await Clients.Group(GetDefaultGroupId(user.PartitionKey))
                .SendAsync("userListRefreshed", users);
        }

        public async Task HasJoined()
        {
            var (scope, connectionMapping, services) = CreateConnectionMapping();
            using (scope)
            {
                var userRepo = services.GetRequiredService<IRoomUserRepository>();
                var roomLookupRepo = services.GetRequiredService<IRoomLookupRepository>();
                await foreach (var item in GetConnections().GroupBy(x => x.GroupId))
                {
                    var cg = await item.FirstAsync();
                    var roomId = cg.GroupId.Substring(GetDefaultGroupId("").Length);
                    var user = await userRepo.ReadAsync($"{Context.UserIdentifier}_{roomId}", roomId);
                    if (await item.CountAsync() == 1)
                    {
                        var totalOnlineCount = await connectionMapping.CountAsync(true, cg.GroupId);
                        await Clients.Groups(cg.GroupId)
                            .SendAsync("onlineCountUpdated", totalOnlineCount);
                        await roomLookupRepo.UpdateOnlineCountAsync(roomId, totalOnlineCount);
                        await NewMessage(user, cg.GroupId, @string["Has joined."]);
                        await RefreshUserListAsync(userRepo,user,false);
                        continue;
                    }

                    await RefreshUserListAsync(userRepo, user, true);
                }
            }
        }

        private async Task<RoomUser> GetCurrentUserAsync()
        {
            var currentGroup = await GetConnections().FirstOrDefaultAsync(x => x.ConnectionId == this.Context.ConnectionId);
            var (scope, connectionMapping, services) = CreateConnectionMapping();
            using (scope)
            {
                var roomId = currentGroup.GroupId.Substring(GetDefaultGroupId("").Length);
                var userRepo = services.GetRequiredService<IRoomUserRepository>();
                return await userRepo.ReadAsync($"{this.Context.UserIdentifier}_{roomId}", roomId);
            }
        }

        public override async Task OnConnectedAsync()
        {
            var (scope, roomService, _) = CreateRoomService();
            using (scope)
            {
                await roomService.JoinAsync(this.Context.UserIdentifier);
            }

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var (scope, connectionMapping, services) = CreateConnectionMapping();
            using (scope)
            {
                var roomLookupRepo = services.GetRequiredService<IRoomLookupRepository>();
                var userRepo = services.GetRequiredService<IRoomUserRepository>();
                var roomService = services.GetRequiredService<IRoomService>();
                var updateOnlineCountTasks = new List<(string groupId,string roomId)>();

                await foreach (var item in GetConnections()
                .GroupBy(x => x.GroupId))
                {
                    var cg = await item.FirstAsync();
                    var roomId = cg.GroupId.Substring(GetDefaultGroupId("").Length);
                    var user = await userRepo.ReadAsync($"{Context.UserIdentifier}_{roomId}", roomId);
                    if (await item.CountAsync() == 1)
                    {
                        await roomService.LeaveAsync(user.UserId);
                        await NewMessage(user, cg.GroupId, @string["Has left."]);
                        await RefreshUserListAsync(userRepo, user, false);
                        updateOnlineCountTasks.Add((cg.GroupId,roomId));
                    }
                }
          
                await base.OnDisconnectedAsync(exception);
                foreach (var (groupId, roomId) in updateOnlineCountTasks)
                {
                    await UpdateRoomOnlineCountAsync(connectionMapping, roomLookupRepo, groupId, roomId);
                }
            }
        }

        private async Task UpdateRoomOnlineCountAsync(
            IRoomConnectionMapping cm,
            IRoomLookupRepository roomLookupRepo,
            string groupId,
            string roomId)
        {
            var totalOnlineCount = await cm.CountAsync(true, groupId);
            await Clients.Groups(groupId)
                .SendAsync("onlineCountUpdated", totalOnlineCount);
            await roomLookupRepo.UpdateOnlineCountAsync(roomId, totalOnlineCount);
        }

        public async Task NewMessage(string message)
        {
            if(message.Length < 2 || message.Length > 256)
            {
                return;
            }

            var user = await GetCurrentUserAsync();
            await Clients.Group(GetDefaultGroupId(user.PartitionKey))
                .SendAsync("messageReceived",
                user.Nickname,
                _sanitizer.ToSafeHtml(message),
                user.UserId, false);
        }

        private Task NewMessage(ApplicationUser user, string groupId, string message)
        {
            var roomUser = new RoomUser(user.Id, user.RoomId) { Nickname = user.Nickname };
            return NewMessage(roomUser, groupId, message);
        }

        private Task NewMessage(RoomUser roomUser, string groupId, string message)
        {
            var user = new UserIdentityDto() { Id = roomUser.UserId, Name = roomUser.Nickname };
            return NewMessage(user, groupId, message);
        }

        private Task NewMessage(UserIdentityDto user, string groupId, string message)
        {
             return Clients.Group(groupId)
                .SendAsync("messageReceived",
                user.Name,
                _sanitizer.ToSafeHtml(message),
                user.Id, false);
        }

        public async Task NewPrivateMessage(string[] userIds, string message)
        {
            var user = await GetCurrentUserAsync();
            var connections = await GetConnections(userIds)
                .Where(x => x.GroupId == GetDefaultGroupId(user.PartitionKey))
                .Select(x => x.ConnectionId)
                .ToListAsync();

            connections.Add(this.Context.ConnectionId);

            await Clients.Clients(connections)
                .SendAsync("messageReceived",
                user.Nickname,
                _sanitizer.ToSafeHtml(message),
                user.UserId, true);
        }
    }
}
