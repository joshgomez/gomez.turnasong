﻿using Gomez.Core.Models.Exceptions;
using Gomez.Core.Web.Helpers;
using Gomez.Core.Web.Sockets;
using Gomez.TurnASong.BusinessLogic.Services.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.Socket.Helpers;
using Gomez.TurnASong.ViewModels.Areas.Identity.Users;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Socket.Hubs.Shared
{
    public abstract class MainHub<C> : Hub where C : IAsyncConnectionMapping
    {
        protected readonly SanitizeHelper _sanitizer = new SanitizeHelper();
        protected readonly ServiceHelper _helper;
        protected readonly string _hubName;

        protected MainHub(IServiceScopeFactory factory, string hubName)
        {
            _sanitizer.MaxLength = 255;
            _sanitizer.StripLength = true;
            _helper = new ServiceHelper(this, factory);
            _hubName = hubName;
        }

        public async Task Ping()
        {
            var (scope, connectionMapping, _) = CreateConnectionMapping();
            using (scope)
            {
                var date = await connectionMapping.GetExpireDateAsync(this.Context.UserIdentifier);
                await this.Clients.Caller.SendAsync("pinged", date);
            }
        }

        protected (IServiceScope scope, C connectionMapping, IServiceProvider services) CreateConnectionMapping()
        {
            var (scope, services) = _helper.CreateService();
            return (scope, services.GetRequiredService<C>(), services);
        }

        protected async Task<(ConnectionData user, string userId)> GetUserAsync()
        {
            var (scope, connectionMapping, _) = CreateConnectionMapping();
            using (scope) {
                var model = await connectionMapping.GetDataAsync(this.Context.UserIdentifier);
                if (model == null)
                {
                    this.Context.Abort();
                    return default;
                }

                return (model, this.Context.UserIdentifier);
            }

        }

        protected IAsyncEnumerable<IConnectionGroup> GetConnections()
        {
            var (scope, connectionMapping, _) = CreateConnectionMapping();
            using (scope)
            {
                return connectionMapping.GetConnectionsAsync(this.Context.UserIdentifier);
            }
        }

        protected async IAsyncEnumerable<IConnectionGroup> GetConnections(IEnumerable<string> userIds)
        {
            var (scope, connectionMapping, _) = CreateConnectionMapping();
            using (scope)
            {
                foreach (var strUserId in userIds)
                {
                    var connections = connectionMapping.GetConnectionsAsync(strUserId);
                    await foreach (var connection in connections)
                    {
                        yield return connection;
                    }
                }
            }
        }

        protected virtual string[] GetDefaultGroupIds(ApplicationUser user)
        {
            return new string[] { $"{_hubName}_{user.RoomId}" };
        }

        public override async Task OnConnectedAsync()
        {
            var (scope, connectionMapping, services) = CreateConnectionMapping();
            using (scope)
            {
                var userManager = services.GetRequiredService<ApplicationUserManager>();
                var user = await userManager.FindByIdAsync(this.Context.UserIdentifier);
                if(user == null || string.IsNullOrEmpty(user.RoomId))
                {
                    this.Context.Abort();
                    return;
                }

                try
                {
                    await connectionMapping.AddAsync(user.Id, this.Context.ConnectionId, null, GetDefaultGroupIds(user));
                }
                catch(TooManyException)
                {
                    await connectionMapping.RemoveAsync(user.Id);
                    await connectionMapping.AddAsync(user.Id, this.Context.ConnectionId, null, GetDefaultGroupIds(user));
                }

                foreach (var groupId in GetDefaultGroupIds(user))
                {
                    await _helper.AddToGroupAsync(groupId);
                }
            }

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var (scope, connectionMapping, services) = CreateConnectionMapping();
            using (scope)
            {
                var groupIds = await GetConnections()
                    .Where(x => x.ConnectionId == this.Context.ConnectionId)
                    .Select(x => x.GroupId).ToArrayAsync();

                await connectionMapping.RemoveAsync(this.Context.UserIdentifier, this.Context.ConnectionId);
                foreach (var groupId in groupIds)
                {
                    await _helper.RemoveFromGroupAsync(groupId);
                }
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
