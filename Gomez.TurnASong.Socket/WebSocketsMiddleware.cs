﻿using Gomez.Core.Web.Sockets;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Socket
{
    public class WebSocketsMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly WebSocketsBearerTokenMiddleware _middleWare;

        public WebSocketsMiddleware(RequestDelegate next)
        {
            _next = next;
            _middleWare = new WebSocketsBearerTokenMiddleware();
        }

        public Task Invoke(HttpContext httpContext)
        {
            return _middleWare.InvokeAsync(httpContext, _next);
        }
    }
}
