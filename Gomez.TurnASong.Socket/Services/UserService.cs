﻿using Gomez.TurnASong.Models.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;

namespace Gomez.TurnASong.Socket.Services
{
    public class UserService<THub> : IUserService where THub : Hub
    {
        protected readonly UserManager<ApplicationUser> _userManager;
        public UserService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }
    }
}
