﻿using Gomez.TurnASong.BusinessLogic.Services.Areas.Identity;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Gomez.TurnASong.Socket.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Socket.Services
{
    public class RoomBackgroundService : BackgroundService
    {
        private readonly IHubContext<RoomHub> _hubContext;
        private readonly IRoomConnectionMapping _connectionMapping;
        private readonly IRoomLookupRepository _roomLookupRepository;
        private readonly IRoomUserRepository _roomUserRepository;
        private readonly ILogger<RoomBackgroundService> _logger;
        private string _continuationToken = null;
        private long executionCount = 0;

        public RoomBackgroundService(
            IHubContext<RoomHub> hubContext,
            IRoomConnectionMapping connectionMapping,
            IRoomLookupRepository roomLookupRepository, 
            IRoomUserRepository roomUserRepository,
            ILogger<RoomBackgroundService> logger)
        {
            _hubContext = hubContext;
            _connectionMapping = connectionMapping;
            _roomLookupRepository = roomLookupRepository;
            _roomUserRepository = roomUserRepository;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                executionCount++;
                _logger.LogInformation(
                    "RoomBackgroundService is working. Count: {Count}", executionCount);

                var results = await _roomLookupRepository
                    .GetAllActiveRoomIdsAsync(_continuationToken);

                var tasks = new Task[results.Documents.Count];
                for (int i = 0; i < results.Documents.Count; i++)
                {
                    tasks[i] = UpdateRoomStatusAsync(results.Documents[i]);
                }

                await Task.WhenAll(tasks);

                _continuationToken = results.ContinuesToken;
                await Task.Delay(1000 * 30, stoppingToken);
            }
        }

        private async Task UpdateRoomStatusAsync(string roomId)
        {
            if(await ExpireUsersAsync(roomId))
            {
                await ActiveUsersAsync(roomId);
                _logger.LogInformation(
                    "Have expired users in room with id {RoomId}", roomId);
            }
        }
    
        private async Task<bool> ExpireUsersAsync(string roomId)
        {
            var groupId = RoomHub.GetDefaultGroupId(roomId);
            var hasExpiredUsers = false;
            await foreach (var expireUserCmpId in _connectionMapping.GetExpireUsersAsync(groupId))
            {
                var expireUserId = expireUserCmpId.Substring(0, expireUserCmpId.Length - "_cmp".Length);
                await _roomUserRepository.LeaveAsync(expireUserId, roomId);
                await _connectionMapping.RemoveAsync(expireUserId);

                hasExpiredUsers = true;
            }

            return hasExpiredUsers;
        }

        private async Task ActiveUsersAsync(string roomId)
        {
            var groupId = RoomHub.GetDefaultGroupId(roomId);
            var totalOnlineCount = await _connectionMapping.CountAsync(true, groupId);
            await _hubContext.Clients.Groups(groupId)
                .SendAsync("onlineCountUpdated", totalOnlineCount);
            await _roomLookupRepository.UpdateOnlineCountAsync(roomId, totalOnlineCount);
        }
    }
}
