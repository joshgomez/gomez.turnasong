﻿using Gomez.TurnASong.Socket.Hubs;
using Gomez.TurnASong.Socket.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace Gomez.TurnASong.Socket
{
    public static class Startup
    {
        public static void RegisterEndpoints(IEndpointRouteBuilder endpoints) 
        {
            endpoints.MapHub<RoomHub>("/hubs/room");
        }

        public static void RegisterService(IServiceCollection services)
        {
            services.AddHostedService<RoomBackgroundService>();
            services.AddSignalR();
        }

        /// <summary>
        /// Must be before UseAuthentication
        /// </summary>
        /// <param name="app"></param>
        public static void UseSignalR(IApplicationBuilder app)
        {
            app.UseMiddleware<WebSocketsMiddleware>();
        }

        /// <summary>
        /// Sending the access token in the query string is required due to
        /// a limitation in Browser APIs. We restrict it to only calls to the
        /// SignalR hub in this code.
        /// See https://docs.microsoft.com/aspnet/core/signalr/security#access-token-logging
        /// for more information about security considerations when using
        /// the query string to transmit the access token.
        /// </summary>
        /// <param name="context"></param>
        public static void RegisterJwtOnMessageReceived(MessageReceivedContext context)
        {
            var accessToken = context.Request.Query["access_token"];

            // If the request is for our hub...
            var path = context.HttpContext.Request.Path;
            if (!string.IsNullOrEmpty(accessToken) &&
                (path.StartsWithSegments("/hubs/")))
            {
                // Read the token out of the query string
                context.Token = accessToken;
            }
        }
    }
}
