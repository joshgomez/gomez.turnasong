﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Socket.Helpers
{
    public class ServiceHelper
    {
        private readonly Hub _hub;
        private readonly IServiceScopeFactory _factory;
        public ServiceHelper(Hub hub, IServiceScopeFactory factory)
        {
            _factory = factory;
            _hub = hub;
        }

        public (IServiceScope scope, IServiceProvider services)
            CreateService()
        {
            var serviceScope = _factory.CreateScope();
            var services = serviceScope.ServiceProvider;
            return (serviceScope, services);
        }

        public async Task AddToGroupAsync(string groupName)
        {
            await _hub.Groups.AddToGroupAsync(_hub.Context.ConnectionId, groupName);
        }

        public async Task RemoveFromGroupAsync(string groupName)
        {
            await _hub.Groups.RemoveFromGroupAsync(_hub.Context.ConnectionId, groupName);
        }
    }
}
