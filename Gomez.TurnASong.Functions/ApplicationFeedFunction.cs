using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Functions.Services;
using Microsoft.Azure.Documents;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Functions
{
    public class ApplicationFeedFunction
    {
        private readonly IApplicationFeedService _service;

        public ApplicationFeedFunction(IApplicationFeedService service)
        {
            _service = service;
        }

        [FunctionName(nameof(ApplicationFeedFunction))]
        public Task RunAsync([CosmosDBTrigger(
            databaseName: DatebaseConstant.DATABASE,
            collectionName: DatebaseConstant.COLLECTION_APPLICATION,
            ConnectionStringSetting = "ConnectionStrings:DefaultConnection",
            LeaseCollectionName = DatebaseConstant.COLLECTION_LEASES)]IReadOnlyList<Document> input, ILogger log)
        {
            log.LogInformation("Running {ApplicationFeedFunction}", nameof(ApplicationFeedFunction));
            return _service.HandleFeedAsync(input);
        }
    }
}
