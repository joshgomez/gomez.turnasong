﻿using Microsoft.Azure.Documents;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Functions.Services
{
    public interface IApplicationFeedService
    {
        Task HandleFeedAsync(IReadOnlyList<Document> input);
    }
}