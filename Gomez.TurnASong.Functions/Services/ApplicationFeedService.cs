﻿using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Microsoft.Azure.Documents;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gomez.TurnASong.Functions.Extensions;

namespace Gomez.TurnASong.Functions.Services
{
    public class ApplicationFeedService : IApplicationFeedService
    {
        private readonly ILogger<ApplicationFeedService> _logger;
        private readonly IRoomLookupRepository _roomLookupRepository;

        public ApplicationFeedService(IRoomLookupRepository roomLookupRepository, ILogger<ApplicationFeedService> logger)
        {
            _roomLookupRepository = roomLookupRepository;
            _logger = logger;
        }

        public async Task HandleFeedAsync(IReadOnlyList<Document> input)
        {
            if (input != null && input.Count > 0)
            {
                _logger.LogInformation("Documents modified {Count}",input.Count);
                foreach (var item in input)
                {
                    _logger.LogInformation("Sync modified {Room} document {Id}",nameof(Room), item.Id);
                    await _roomLookupRepository.SyncLookupAsync(await item.ReadAsAsync<Room>());
                }
            }
        }
    }
}
