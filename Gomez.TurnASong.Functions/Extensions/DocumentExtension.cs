﻿using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Functions.Extensions
{
    public static class DocumentExtension
    {
        public static async Task<T> ReadAsAsync<T>(this Document d)
        {
            using (var ms = new MemoryStream())
            using (var reader = new StreamReader(ms))
            {
                d.SaveTo(ms);
                ms.Position = 0;
                return JsonConvert.DeserializeObject<T>(await reader.ReadToEndAsync());
            }
        }
    }
}
