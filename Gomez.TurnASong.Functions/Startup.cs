﻿using Gomez.Core.DocumentDb;
using Gomez.Core.DocumentDb.Cosmos;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Functions.Services;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

[assembly: FunctionsStartup(typeof(Gomez.TurnASong.Functions.Startup))]
namespace Gomez.TurnASong.Functions
{
    public class Startup : FunctionsStartup
    {
        public Startup()
        {

        }

        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddLogging();
            builder.Services.AddDistributedMemoryCache();
            var serviceProvider = builder.Services.BuildServiceProvider();
            var configuration = serviceProvider.GetService<IConfiguration>();

            var dbConnectionStr = configuration.GetSection("ConnectionStrings:DefaultConnection").Get<string>();


            builder.Services
                .AddSingleton(x => new CosmosClient(dbConnectionStr, new CosmosClientOptions() { AllowBulkExecution = true }))
                .AddSingleton<IDocumentDbContext>(x => new CosmosDbContext(
                    x.GetService<CosmosClient>(),
                    DatebaseConstant.DATABASE,
                    x.GetService<IDistributedCache>(),
                    x.GetService<ILogger<CosmosDbContext>>()))
                .AddSingleton<IApplicationFeedService,ApplicationFeedService>()
                .AddSingleton<IRoomRepository,RoomRepository>();
        }
    }
}
