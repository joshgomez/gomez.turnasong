﻿using Gomez.Core.ViewModels.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Gomez.Core.Cosmos.Web.Services;
using Gomez.TurnASong.Configurations;
using Gomez.Core.Cosmos.Repositories;

namespace Gomez.TurnASong.Controllers.Api
{
    [Route(RouteContant.API + "[controller]")]
    public class TokenController : Gomez.Core.Cosmos.Web.Controllers.TokenApiController<ApplicationUser,ApplicationRole, TokenController>
    {
        public TokenController(
            IRefreshTokenRepository<ApplicationUser, ApplicationRole> refreshTokenRepository,
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory,
            IJwtTokenService<ApplicationRole, ApplicationUser> tokenService, 
            RoleManager<ApplicationRole> rm,
            SignInManager<ApplicationUser> sim,
            ILogger<TokenController> logger) : base(refreshTokenRepository, um, localizerFactory, tokenService, rm,sim, logger)
        {
        }

        [HttpPost]
        public Task<IActionResult> Post(LoginForm vm)
        {
            return base.Login(vm);
        }

        [HttpPost("Refresh")]
        public Task<IActionResult> PostRefresh(QueryRefreshTokenViewModel query)
        {
            return base.Refresh(query);
        }

        [HttpDelete]
        [Authorize]
        public Task<IActionResult> Delete()
        {
            return base.Revoke();
        }
    }
}
