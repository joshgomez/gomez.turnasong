﻿using Gomez.Core.Models.Exceptions;
using Gomez.Core.Web.Extensions;
using Gomez.TurnASong.BusinessLogic.Services.Areas.Identity;
using Gomez.TurnASong.Configurations;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.ViewModels.Areas.Identity.Profiles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Controllers.Api.Areas.Multimedia
{
    [ApiController]
    [Authorize]
    [Route(RouteContant.API_IDENTITY + "[controller]")]
    public class ProfilesController : Gomez.Core.Cosmos.Web.Controllers.MainApiController<ApplicationRole, ApplicationUser, RoomsController>
    {
        private readonly IProfileService _profileService;

        public ProfilesController(
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory,
            ILogger<RoomsController> logger,
            IProfileService profileService) : base(um,localizerFactory,logger)
        {
            _profileService = profileService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                return Ok(await _profileService.GetAsync(id));
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody] ProfilePatchDto request)
        {
            request.Id = this.User.GetUserId<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    return Ok(await _profileService.PatchAsync(request));
                }

                return BadRequest(ModelState
                    .SelectMany(x => x.Value.Errors)
                    .Select(x => x.ErrorMessage).FirstOrDefault());
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }
    }
}
