﻿using Gomez.Core.Configurations.Constants.Areas.Identity;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.Web.Extensions;
using Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia;
using Gomez.TurnASong.Configurations;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Controllers.Api.Areas.Multimedia
{
    [ApiController]
    [Authorize]
    [Route(RouteContant.API_MULTIMEDIA + "[controller]")]
    public class RoomsController : Gomez.Core.Cosmos.Web.Controllers.MainApiController<ApplicationRole, ApplicationUser, RoomsController>
    {
        private readonly IRoomService _roomService;

        public RoomsController(
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory,
            RoleManager<ApplicationRole> rm,
            ILogger<RoomsController> logger,
            IRoomService roomService) : base(um,localizerFactory,logger)
        {
            _roomService = roomService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(QueryIndexViewModel request)
        {
            return Ok(await _roomService.IndexAsync(request));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var room = await _roomService.GetAsync(id);
            if(room != null)
            {
                return Ok(room);
            }

            return NotFound();
        }

        [HttpGet("by-user")]
        public async Task<IActionResult> ByUser()
        {
            var userId = this.User.GetUserId<string>();
            var (room, _) = await _roomService.GetByUserAsync(userId);
            return Ok(room);
        }

        [HttpGet("{roomId}/join")]
        public async Task<IActionResult> Join(string roomId)
        {
            var userId = this.User.GetUserId<string>();
            var (room, _) = await _roomService.JoinAsync(userId, roomId);
            if(room == null)
            {
                return NotFound();
            }

            return Ok(room);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RoomPostDto request)
        {
            var userId = this.User.GetUserId<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    return Ok(await _roomService.PostAsync(userId,request));
                }

                return BadRequest(ModelState
                    .SelectMany(x => x.Value.Errors)
                    .Select(x => x.ErrorMessage).FirstOrDefault());
            }
            catch (ExistsException)
            {
                return Conflict();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody] RoomPatchDto request)
        {
            var userId = this.User.GetUserId<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    var isAdmin = this.User.IsInRole(RoleConstant.ADMINISTRATOR);
                    return Ok(await _roomService.PatchAsync(userId, request, isAdmin));
                }

                return BadRequest(ModelState
                    .SelectMany(x => x.Value.Errors)
                    .Select(x => x.ErrorMessage).FirstOrDefault());
            }
            catch (AccessDeniedException)
            {
                return Forbid("No permission to edit.");
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var userId = this.User.GetUserId<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    var isAdmin = this.User.IsInRole(RoleConstant.ADMINISTRATOR);
                    await _roomService.DeleteAsync(userId, id, isAdmin);
                    return NoContent();
                }

                return BadRequest(ModelState
                    .SelectMany(x => x.Value.Errors)
                    .Select(x => x.ErrorMessage).FirstOrDefault());
            }
            catch (ExistsException)
            {
                return Conflict("The room has active users.");
            }
            catch (AccessDeniedException)
            {
                return Forbid("No permission to delete.");
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }
    }
}
