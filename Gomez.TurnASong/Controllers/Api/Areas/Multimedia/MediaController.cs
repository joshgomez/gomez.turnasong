﻿using Gomez.Core.Web.Extensions;
using Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia;
using Gomez.TurnASong.Configurations;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.ViewModels.Areas.Multimedia.Media;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Controllers.Api.Areas.Multimedia
{
    [ApiController]
    [Authorize]
    [Route(RouteContant.API_MULTIMEDIA + "[controller]")]
    public class MediaController : Gomez.Core.Cosmos.Web.Controllers.MainApiController<ApplicationRole, ApplicationUser, RoomsController>
    {
        private readonly IRoomService _roomService;
        private readonly IMediaService _mediaService;

        public MediaController(
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory,
            ILogger<RoomsController> logger,
            IRoomService roomService,
            IMediaService mediaService) : base(um,localizerFactory,logger)
        {
            _roomService = roomService;
            _mediaService = mediaService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(QueryIndexViewModel request)
        {
            return Ok(await _mediaService.IndexAsync(request));
        }

        [HttpGet("my-queue")]
        public async Task<IActionResult> GetMyQueue(string continuationToken = null)
        {
            var userId = User.GetUserId<string>();
            return Ok(await _mediaService.GetMyItemQueueAsync(userId, continuationToken));
        }

        [HttpGet("my-history")]
        public async Task<IActionResult> GetMyHistory(string continuationToken = null)
        {
            var userId = User.GetUserId<string>();
            return Ok(await _mediaService.GetMyPlayedItemsAsync(userId, continuationToken));
        }

        [HttpGet("history")]
        public async Task<IActionResult> History(string continuationToken = null)
        {
            var userId = User.GetUserId<string>();
            return Ok(await _mediaService.GetAllPlayedItemsByUserRoomAsync(userId, continuationToken));
        }

        [HttpGet("waiting")]
        public async Task<IActionResult> Waiting()
        {
            var userId = User.GetUserId<string>();
            return Ok(await _mediaService.GetAllWaitingUsersByUserIdAsync(userId));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var userId = User.GetUserId<string>();
            await _mediaService.DeleteQueuedItemAsync(id, userId);
            return NoContent();
        }
    }
}
