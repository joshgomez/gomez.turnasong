﻿using Gomez.Core.Cosmos.Repositories;
using Gomez.Core.Cosmos.Web.Services;
using Gomez.TurnASong.Models.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Controllers
{
    public class ExternalTokenController : Core.Cosmos.Web.Controllers
        .ExternalTokenController<ApplicationUser,ApplicationRole, ExternalTokenController>
    {
        public static ApplicationUser OnCreateUser(Core.Web.Models.ExternalLogin vm)
        {
            string userName = vm.Info.Principal.Identity.Name;
            string email = vm.Info.Principal.FindFirst(ClaimTypes.Email)?.Value;
            if(email == null)
            {
                throw new Gomez.Core.Models.Exceptions.NotFoundException(nameof(email),"EMAIL_MOT_FOUND");
            }

            switch (vm.Info.LoginProvider.ToUpper())
            {
                case "GOOGLE":
                case "MICROSOFT":
                case "FACEBOOK":
                    userName = email;
                    break;
            }

            var user = new ApplicationUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = userName,
                NormalizedUserName = userName.ToUpperInvariant(),
                Email = email,
                NormalizedEmail = email?.ToUpperInvariant(),
                Nickname = $"Artist_{Gomez.Core.Utilities.NumberUtility.RandomBetween(1, 9999)}"
            };

            return user;
        }

        public ExternalTokenController(
            IRefreshTokenRepository<ApplicationUser, ApplicationRole> refreshTokenRepository,
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            IJwtTokenService<ApplicationRole,ApplicationUser> tokenService,
            RoleManager<ApplicationRole> rm,
            SignInManager<ApplicationUser> sim,
            ILogger<ExternalTokenController> logger,
            IConfiguration configuration) : base(refreshTokenRepository, um, localizerFactory, tokenService, rm, sim, logger)
        {
            var origins = configuration.GetValue<string>("AllowedOrigins").Split(";");
            this.SetRedirectionWhiteList(origins);

            if(CreateUser == null)
            {
                CreateUser = OnCreateUser;
            }
        }



        [HttpPost]
        public IActionResult Index(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            string url = Url.Action(new UrlActionContext
            {
                Protocol = Request.Scheme,
                Host = Request.Host.Value,
                Action = "Callback",
            });

            return  base.OnPost(provider, url, returnUrl);
        }

        [HttpGet]
        public async Task<IActionResult> Callback(string returnUrl = null, string remoteError = null)
        {
            return await base.OnGetCallbackAsync(true, returnUrl, remoteError);
        }
    }
}
