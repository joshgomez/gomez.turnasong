using Gomez.AspNetCore.Identity.Cosmos.Extensions;
using Gomez.Core.Cosmos.Repositories;
using Gomez.Core.Cosmos.Web.Services;
using Gomez.Core.DocumentDb;
using Gomez.Core.DocumentDb.Cosmos;
using Gomez.Core.Multimedia;
using Gomez.Core.Multimedia.YouTube;
using Gomez.Core.Utilities;
using Gomez.TurnASong.BusinessLogic.Seeds;
using Gomez.TurnASong.BusinessLogic.Services.Areas.Identity;
using Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia;
using Gomez.TurnASong.Configurations;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.Repositories.Areas.Identity;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Gomez.TurnASong.Services;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace Gomez.TurnASong
{
    public class Startup
    {
        private readonly string _allowSpecificOrigins = "AllowSpecificOrigins";
        private readonly string _allowAllOrigins = "AllowAllOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                var origins = Configuration.GetValue<string>("AllowedOrigins").Split(";");
                options.AddPolicy(_allowSpecificOrigins,
                builder => builder.WithOrigins(origins)
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials());

                options.AddPolicy(_allowAllOrigins,
                    builder => builder.AllowAnyOrigin()
                                .AllowAnyHeader()
                                .AllowAnyMethod());
            });

            services.AddApplicationInsightsTelemetry();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ITelemetryInitializer, AppInsightsInitializer>();

            services.AddHealthChecks();
            services.AddDistributedMemoryCache();
            services.AddDefaultDocumentClientForIdentity(Configuration.GetConnectionString(DatebaseConstant.DEFAULT_CONNECTION), 
                new CosmosOptions() 
                { 
                    AllowBulkExecution = true,
                    ContinuationTokenCollectionName = DatebaseConstant.COLLECTION_IDENTITIES,
                    UseContinuationTokenCollection = true
                });

            services.AddSingleton<IRefreshTokenRepository<ApplicationUser, ApplicationRole>>(x =>
                new RefreshTokenRepository<ApplicationUser, ApplicationRole>(
                    x.GetService<IDocumentDbContext>(),
                    DatebaseConstant.COLLECTION_IDENTITIES))
                .AddSingleton<IConnectionMapRepository>(x =>
                    new ConnectionMapRepository(
                        x.GetService<IDocumentDbContext>(),
                        DatebaseConstant.COLLECTION_IDENTITIES,
                        HubConstant.HUBNAME_ROOM))
                .AddSingleton<IRoomRepository, RoomRepository>()
                .AddSingleton<IRoomLookupRepository, RoomLookupRepository>()
                .AddSingleton<IRoomUserRepository, RoomUserRepository>()
                .AddSingleton<IRoomUserStateRepository, RoomUserStateRepository>()
                .AddSingleton<IRoomPlaylistItemRepository, RoomPlaylistItemRepository>()
                .AddSingleton<IMediaProvider>(x =>
                    new YouTubeMediaProvider(
                        Configuration.GetValue<string>("YouTube:Key"),
                        Configuration.GetValue<string>("YouTube:Name")))
                .AddScoped<IMediaService, MediaService>()
                .AddScoped<IRoomService, RoomService>()
                .AddScoped<IProfileService, ProfileService>()
                .AddScoped<ApplicationUserManager>()
                .AddSingleton<IRoomConnectionMapping>(
                    x => new RoomConnectionMapping(
                        x.GetService<IConnectionMapRepository>()
                    )
                );


            Socket.Startup.RegisterService(services);

            services.AddTransient<IJwtTokenService<ApplicationRole,ApplicationUser>, JwtTokenService<ApplicationRole,ApplicationUser>>();

            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en-US"),
                        new CultureInfo("sv-SE"),
                    };

                    options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;
                });

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddDocumentDbStores(x =>
                {
                    x.UserStoreDocumentCollection = DatebaseConstant.COLLECTION_IDENTITIES;
                });

            //Setting up Jwt Authentication
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration[JwtTokenService.JWT_ISSUER],
                        ValidAudience = Configuration[JwtTokenService.JWT_AUDIENCE],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration[JwtTokenService.JWT_KEY]))
                    };

#if DEBUG
                    options.TokenValidationParameters.ValidateAudience = false;
#endif

                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            Socket.Startup.RegisterJwtOnMessageReceived(context);
                            return Task.CompletedTask;
                        }
                    };

                }).AddMicrosoftAccount(option =>
                {
                    option.ClientId = Configuration["Authentication:Microsoft:ClientId"];
                    option.ClientSecret = Configuration["Authentication:Microsoft:ClientSecret"];
                }).AddGoogle(options =>
                {
                    options.ClientId = Configuration["Authentication:Google:ClientId"];
                    options.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
                }).AddFacebook(facebookOptions =>
                {
                    facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                    facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                });

            services.AddResponseCaching();
            services.AddResponseCompression();

            services.AddHostedService<ApplicationFeedService>();
            services.AddControllers()
                .AddHybridModelBinder()
                .AddDataAnnotationsLocalization()
                .AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app, 
            IWebHostEnvironment env, 
            IConfiguration configuration,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager, 
            IDocumentDbContext db, 
            CosmosClient cosmosClient,
            IRoomRepository roomRepository)
        {
            var dbConfig = new DatabaseConfiguration(cosmosClient, configuration);
            AsyncPump.Run(async () => 
            {
                await dbConfig.CreateAsync();
            });

            var dbInitializer = new ApplicationDbInitializer(db, userManager, roleManager);
            dbInitializer.SeedRoles();
            dbInitializer.SeedUsers();

            AsyncPump.Run(async () => {
                await RoomSeed.RunAsync(roomRepository, userManager);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseCors(_allowSpecificOrigins);

            app.UseWebSockets();
            app.UseHttpsRedirection();

            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".webmanifest"] = "application/manifest+json";
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                ContentTypeProvider = provider
            });


            app.UseRequestLocalization();
            app.UseRouting();

            Socket.Startup.UseSignalR(app);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseResponseCaching();
            app.UseResponseCompression();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                Socket.Startup.RegisterEndpoints(endpoints);
                endpoints.MapDefaultControllerRoute();
                endpoints.MapControllers();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "wwwroot";
            });
        }
    }
}
