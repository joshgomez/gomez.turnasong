﻿using Gomez.TurnASong.Configurations.Constants;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Configurations
{
    public class DatabaseConfiguration
    {
        private readonly CosmosClient _cosmosClient;
        private readonly IConfiguration _configuration;

        public DatabaseConfiguration(CosmosClient cosmosClient, IConfiguration configuration)
        {
            _cosmosClient = cosmosClient;
            _configuration = configuration;
        }

        public async Task CreateAsync()
        {
            var isServerless = _configuration.GetValue<bool>("Cosmos:Serverless");
            int? defaultThroughput = 400;
            if (isServerless)
            {
                defaultThroughput = null;
            }

            var dbDefaultThroughput = (defaultThroughput != null ? defaultThroughput * DatebaseConstant.TOTAL_COLLECTIONS : null);
            var dbResponse = await _cosmosClient.CreateDatabaseIfNotExistsAsync(DatebaseConstant.DATABASE, dbDefaultThroughput);
            await dbResponse.Database.CreateContainerIfNotExistsAsync(new ContainerProperties()
            {
                Id = DatebaseConstant.COLLECTION_IDENTITIES,
                PartitionKeyPath = "/partitionKey",
                DefaultTimeToLive = -1
            }, defaultThroughput);

            await dbResponse.Database.CreateContainerIfNotExistsAsync(new ContainerProperties()
            {
                Id = DatebaseConstant.COLLECTION_APPLICATION,
                PartitionKeyPath = "/partitionKey",
                DefaultTimeToLive = -1
            }, defaultThroughput);

            await dbResponse.Database.CreateContainerIfNotExistsAsync(new ContainerProperties()
            {
                Id = DatebaseConstant.COLLECTION_LEASES,
                PartitionKeyPath = "/id",
                DefaultTimeToLive = -1
            }, defaultThroughput);

            await UpsertApplicationIndexesAsync(dbResponse.Database);
        }

        private async Task UpsertApplicationIndexesAsync(Database db)
        {
            var appCollection = db.GetContainer(DatebaseConstant.COLLECTION_APPLICATION);
            var propResponse = await appCollection.ReadContainerAsync();

            var policy = propResponse.Resource.IndexingPolicy;
            policy.CompositeIndexes.Clear();

            policy.CompositeIndexes.Add(new Collection<CompositePath>
            {
                new CompositePath() { Path = "/userId", Order = CompositePathSortOrder.Ascending },
                new CompositePath() { Path = "/deletedAt" , Order = CompositePathSortOrder.Descending }
            });

            policy.CompositeIndexes.Add(new Collection<CompositePath>
            {
                new CompositePath() { Path = "/userId", Order = CompositePathSortOrder.Ascending },
                new CompositePath() { Path = "/createdAt" , Order = CompositePathSortOrder.Descending }
            });

            policy.CompositeIndexes.Add(new Collection<CompositePath>
            {
                new CompositePath() { Path = "/deletedAt", Order = CompositePathSortOrder.Descending },
                new CompositePath() { Path = "/createdAt" , Order = CompositePathSortOrder.Ascending }
            });

            policy.CompositeIndexes.Add(new Collection<CompositePath>
            {
                new CompositePath() { Path = "/deletedAt", Order = CompositePathSortOrder.Descending },
                new CompositePath() { Path = "/orderNumber", Order = CompositePathSortOrder.Ascending },
                new CompositePath() { Path = "/createdAt" , Order = CompositePathSortOrder.Ascending }
            });

            propResponse.Resource.DefaultTimeToLive = -1;
            propResponse.Resource.IndexingPolicy = policy;
            await appCollection.ReplaceContainerAsync(propResponse.Resource);
        }
    }
}
