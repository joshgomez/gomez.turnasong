﻿using Gomez.TurnASong.Models.Areas.Multimedia;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Services
{
    public interface IApplicationFeedService
    {
        Task HandleRoomFeedAsync(IReadOnlyCollection<Room> input, CancellationToken cancellationToken);
    }
}