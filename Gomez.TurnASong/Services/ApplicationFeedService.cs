﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Services
{
    public class ApplicationFeedService : BackgroundService, IApplicationFeedService
    {
        private readonly ILogger<ApplicationFeedService> _logger;
        private readonly IRoomLookupRepository _roomLookupRepository;
        private readonly ChangeFeedProcessor _roomProcessor;
        private readonly string _instanceName;

        protected ApplicationFeedService(
            IConfiguration configuration,
            string connectionStringName,
            CosmosClient cosmosClient,
            IRoomLookupRepository roomLookupRepository,
            ILogger<ApplicationFeedService> logger)
        {
            _roomLookupRepository = roomLookupRepository;
            _logger = logger;

            var connectionString = new DbConnectionString(connectionStringName, configuration);
            _instanceName = configuration.GetValue<string>("InstanceName");

            Container leaseContainer = cosmosClient.GetContainer(connectionString.Database, Configurations.Constants.DatebaseConstant.COLLECTION_LEASES);
            _roomProcessor = cosmosClient.GetContainer(connectionString.Database, Configurations.Constants.DatebaseConstant.COLLECTION_APPLICATION)
                .GetChangeFeedProcessorBuilder<Room>(processorName: configuration.GetValue<string>("Cosmos:ProcessorName"), HandleRoomFeedAsync)
                    .WithInstanceName(_instanceName)
                    .WithLeaseContainer(leaseContainer)
                    .Build();
        }

        public ApplicationFeedService(IConfiguration configuration,
            CosmosClient cosmosClient,
            IRoomLookupRepository roomLookupRepository,
            ILogger<ApplicationFeedService> logger) : this(configuration, DatebaseConstant.DEFAULT_CONNECTION,cosmosClient, roomLookupRepository, logger)
        { 
        }

        public async Task HandleRoomFeedAsync(IReadOnlyCollection<Room> input, CancellationToken cancellationToken)
        {
            if (input != null && input.Count > 0)
            {
                _logger.LogInformation("Documents modified {Count}",input.Count);
                foreach (var item in input)
                {
                    _logger.LogInformation("Sync modified {Room} document {Id}",nameof(Room), item.Id);
                    await _roomLookupRepository.SyncLookupAsync(item);
                }
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _roomProcessor.StartAsync();
            stoppingToken.WaitHandle.WaitOne();
            
            _logger.LogInformation("Stopping change feed processor. {InstanceName}", _instanceName);
            await _roomProcessor.StopAsync();
        }
    }
}
