﻿using Gomez.Core.Web.Extensions;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Http;
using System;
using System.Net.Mail;
using System.Security.Claims;

namespace Gomez.TurnASong.Services
{
    public class AppInsightsInitializer : ITelemetryInitializer
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AppInsightsInitializer(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException("httpContextAccessor");
        }

        public void Initialize(ITelemetry telemetry)
        {
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext != null && httpContext.User?.Identity.IsAuthenticated == true)
            {
                var userId = httpContext.User.Identity?.Name;
                if (!string.IsNullOrEmpty(userId))
                {
                    telemetry.Context.User.AuthenticatedUserId = httpContext.User.GetUserId<string>();
                    var email = httpContext.User.FindFirst(ClaimTypes.Email)?.Value;
                    if (!string.IsNullOrEmpty(email))
                    {
                        var address = new MailAddress(email);
                        telemetry.Context.User.AccountId = address.Host;
                    }
                }
            }
        }
    }
}
