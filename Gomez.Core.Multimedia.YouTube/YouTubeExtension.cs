﻿using Google.Apis.YouTube.v3.Data;

namespace Gomez.Core.Multimedia.YouTube
{
    public static class YouTubeExtension
    {
        public static double DurationAsSeconds(this VideoContentDetails details)
        {
            var timeSpan = System.Xml.XmlConvert.ToTimeSpan(details.Duration);
            return timeSpan.TotalSeconds;
        }
    }
}
