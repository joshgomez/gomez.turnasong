﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Multimedia.YouTube
{
    public class YouTubeMediaProvider : IMediaProvider, IDisposable
    {
        private YouTubeService _youTubeService;
        public YouTubeMediaProvider(string apiKey, string applicationName)
        {
            _youTubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = apiKey,
                ApplicationName = applicationName,
            });
        }

        public async Task<IVideo<IThumbnail>> GetVideoByIdAsync(string id)
        {
            var query = _youTubeService.Videos.List("snippet, contentDetails, id");
            query.Id = id;
            query.MaxResults = 1;

            VideoListResponse response = await query.ExecuteAsync();
            if(response.Items.Count == 1)
            {
                var video = response.Items[0];
                return new Video()
                {
                    Id = video.Id,
                    Title = video.Snippet.Title,
                    Description = video.Snippet.Description,
                    DurationAsSeconds = video.ContentDetails.DurationAsSeconds(),
                    Thumbnails = new Thumbnail()
                    {
                        StandardUrl = video.Snippet.Thumbnails.Standard?.Url ?? video.Snippet.Thumbnails.Default__?.Url,
                        MediumUrl = video.Snippet.Thumbnails.Medium?.Url ?? video.Snippet.Thumbnails.Default__?.Url,
                        HighUrl = video.Snippet.Thumbnails.High?.Url ?? video.Snippet.Thumbnails.Default__?.Url
                    }
                };
            }

            return null;
        }

        private SearchResource.ListRequest.VideoDurationEnum GetDurationFilter(MediaSearchQuery searchTerms)
        {
            return searchTerms.VideoDuration switch
            {
                DurationType.Short => SearchResource.ListRequest.VideoDurationEnum.Short__,
                DurationType.Medium => SearchResource.ListRequest.VideoDurationEnum.Medium,
                DurationType.Long => SearchResource.ListRequest.VideoDurationEnum.Long__,
                _=> SearchResource.ListRequest.VideoDurationEnum.Any
            };
        }

        public async Task<IEnumerable<IVideo<IThumbnail>>> SearchAsync(MediaSearchQuery searchTerms)
        {
            var query = _youTubeService.Search.List("snippet");
            query.Type = searchTerms.Type;
            query.MaxResults = Math.Min(searchTerms.MaxResults,50);
            query.Order = ((SearchResource.ListRequest.OrderEnum?) searchTerms.Order) ?? SearchResource.ListRequest.OrderEnum.Title;
            query.Q = searchTerms.Query;
            query.VideoDuration = GetDurationFilter(searchTerms);

            SearchListResponse response = await query.ExecuteAsync();
            var searchResponse = response.Items.Select(video => new Gomez.Core.Multimedia.Video()
            {
                Id = video.Id.VideoId,
                Title = video.Snippet.Title,
                Description = video.Snippet.Description,
                Thumbnails = new Thumbnail()
                {
                    StandardUrl = video.Snippet.Thumbnails.Standard?.Url ?? video.Snippet.Thumbnails.Default__?.Url,
                    MediumUrl = video.Snippet.Thumbnails.Medium?.Url ?? video.Snippet.Thumbnails.Default__?.Url,
                    HighUrl = video.Snippet.Thumbnails.High?.Url ?? video.Snippet.Thumbnails.Default__?.Url
                }
            }).ToList();

            var queryVideo = _youTubeService.Videos.List("contentDetails, id");
            queryVideo.MaxResults = searchResponse.Count;
            queryVideo.Id = String.Join(',', searchResponse.Select(x => x.Id));
            VideoListResponse videoResponse = await queryVideo.ExecuteAsync();
            foreach (var searchItem in searchResponse)
            {
                searchItem.DurationAsSeconds = videoResponse.Items.Where(x => x.Id == searchItem.Id)
                    .Select(x => x.ContentDetails.DurationAsSeconds()).FirstOrDefault();
            }

            return searchResponse;
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _youTubeService != null)
                {
                    _youTubeService.Dispose();
                    _youTubeService = null;
                }

                disposedValue = true;
            }
        }

        ~YouTubeMediaProvider()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
