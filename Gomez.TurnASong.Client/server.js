/* eslint-disable @typescript-eslint/no-var-requires */
'use strict';
var path = require('path');
var express = require('express');
var fs = require('fs');
var https = require('https');

var privateKey = fs.readFileSync('localhost.key', 'utf8');
var certificate = fs.readFileSync('localhost.crt', 'utf8');
var options = { key: privateKey, cert: certificate };

var staticPath = path.join(__dirname, 'wwwroot');
var indexPath = path.join(staticPath, 'index.html');

const app = express();
app.use(express.static(staticPath));

app.get('*', function (req, res) {
    res.sendFile(indexPath);
});

// Allows you to set port in the project properties.
app.set('port', process.env.PORT || 1337);

const server = https.createServer(options, app);
const port = app.get('port');
server.listen(port, function () {
    console.log(`listening on https://localhost:${port}`);
});
