﻿/* eslint-disable @typescript-eslint/no-var-requires */
const webpack = require('webpack');
const { CheckerPlugin } = require('awesome-typescript-loader');
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WorkboxWebpackPlugin = require('workbox-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const tsNameof = require('ts-nameof');
const path = require('path');

function buildConfig(env) {
    if (env === 'development' || env === 'production') {
        const optimization = {
            optimization: {
                chunkIds: 'named',
                usedExports: true,
                splitChunks: {
                    cacheGroups: {
                        commons: {
                            chunks: 'initial',
                            minChunks: 2,
                            maxInitialRequests: 5, // The default limit is too small to showcase the effect
                            minSize: 0, // This is example is too small to create commons chunks
                        },
                        vendor: {
                            test: /node_modules/,
                            chunks: 'initial',
                            name: 'vendor',
                            priority: 10,
                            enforce: true,
                        },
                    },
                },
            },
        };

        const resolve = {
            resolve: {
                extensions: ['.ts', '.tsx', '.js', '.jsx'],
                alias: { '@src': path.resolve(__dirname, 'src') },
            },
        };

        const module = {
            module: {
                rules: [
                    {
                        test: /\.(ts|tsx)$/,
                        loader: 'awesome-typescript-loader',
                        options: {
                            getCustomTransformers: () => ({ before: [tsNameof] }),
                        },
                    },
                    {
                        test: /\.scss$/,
                        use: [
                            'style-loader', // creates style nodes from JS strings
                            'css-loader', // translates CSS into CommonJS
                            'sass-loader', // compiles Sass to CSS, using Node Sass by default
                        ],
                    },
                    {
                        test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                        use: [
                            {
                                loader: 'file-loader',
                                options: {
                                    name: '[name].[ext]',
                                    outputPath: '../../wwwroot/fonts',
                                    publicPath: '/fonts',
                                },
                            },
                        ],
                    },
                ],
            },
        };

        const config = require('./webpack.' + env + '.config.js');
        const plugins = config.plugins || [];
        return {
            entry: ['./src/main.tsx'],
            ...config,
            ...optimization,
            ...resolve,
            ...module,
            devtool: false,
            plugins: [
                ...plugins,
                new CleanWebpackPlugin(),
                new CheckerPlugin(),
                new Dotenv({}),
                new webpack.HashedModuleIdsPlugin(),
                new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
                new HtmlWebpackPlugin({ filename: '../index.html', template: 'src/index.html' }),
                new CopyPlugin([
                    {
                        from: 'src/assets',
                        to: '../',
                    },
                ]),
                new WorkboxWebpackPlugin.InjectManifest({
                    swSrc: './src/sw.js',
                    swDest: '../sw.js',
                    include: [/\.html$/, /\.js$/, /\.css$/, /\.woff2$/, /\.jpg$/, /\.png$/],
                }),
                new CompressionPlugin(),
                new webpack.SourceMapDevToolPlugin({
                    filename: '[file].map',
                }),
            ],
        };
    } else {
        console.error("Wrong webpack build parameter. Possible choices: 'development' or 'production'.");
    }
}

module.exports = buildConfig;
