export class Config {
    public static apiEndPoint: string = process.env.API_ENDPOINT;
    public static wsEndPoint: string = process.env.WS_ENDPOINT;
    public static adsTest: boolean = process.env.ADS_TEST?.toLowerCase() === 'true';
    public static adsGoogleClient: string = process.env.ADS_GOOGLE_CLIENT;
    public static appInsKey: string = process.env.APPINSIGHTS_KEY;
}
