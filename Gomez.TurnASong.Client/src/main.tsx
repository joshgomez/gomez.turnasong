import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import store from './stores';

import './scss/main.scss';
import { Config } from './config';

const googleAdsScriptElm = document.getElementById('googleAdsScript');
if (googleAdsScriptElm) {
    googleAdsScriptElm.setAttribute('data-ad-client', Config.adsGoogleClient);
}

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'),
);
