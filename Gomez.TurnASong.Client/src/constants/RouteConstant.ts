﻿export class RouteConstant {
    static main = '/';
    static room = '/rooms/:id';
    static roomsSettings = '/rooms-settings/:id';
    static signIn = '/sign-in';
    static addRoom = '/add-room';
    static profile = '/profile';
    static profileSettings = '/profile/settings';
}
