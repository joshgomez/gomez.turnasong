export class ValidationConstant {
    static roomNameMinLength = 2;
    static roomNameMaxLength = 128;

    static chatMessageMinLength = 2;
    static chatMessageMaxLength = 256;

    static profileMinLength = 2;
    static profileMaxLength = 64;
}
