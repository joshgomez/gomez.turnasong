export interface IChatItem {
    nickname: string;
    message: string;
    userId: string;
    isPrivate: boolean;
}

export interface IUserItem {
    nickName: string;
    userId: string;
    isSelected: boolean;
}
