export interface IContinuedList<T> {
    continuationToken?: string | null;
    documents: T[];
}

export interface IReactWindowItem<T> {
    index: number;
    style: React.CSSProperties;
    data: T;
}
