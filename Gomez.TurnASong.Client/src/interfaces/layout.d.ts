export interface IToastItem {
    show: boolean;
    delay?: number;
    autoHide?: boolean;
    title: string | React.ReactNode;
    message?: string | React.ReactNode;
}
