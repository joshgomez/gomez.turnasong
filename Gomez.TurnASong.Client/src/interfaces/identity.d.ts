export interface IProfilePatchDto {
    nickname: string;
}

export interface IProfileDto {
    userName: string;
    nickname: string;
}

export interface IUserIdentity {
    id: string;
    name: string;
}
