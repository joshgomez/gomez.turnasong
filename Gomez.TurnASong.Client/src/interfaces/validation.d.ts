export interface IValidation<T> {
    model: T;
    isValid: boolean;
    errorMessages: Array<string>;
    errorMessage: string | null;
    status: string | null;
}
