import { IContinuedList } from './common';
import { IUserIdentity } from './identity';
import { DurationType } from '../models/DurationType';

export interface IRoomListItem {
    id: string;
    name: string;
    thumbnail: string | null;
    onlineCount: number;
}

export interface IRoomsResponse {
    search?: string;
    continuationToken?: string;
    rooms: IContinuedList<IRoomListItem>;
}

export interface IRoomDto {
    id: string;
    name: string;
    currentMedia: IMediaDto | null;
    owner: IUserIdentity;
    videoDuration: DurationType;
}

export interface IRoomPostDto {
    name: string;
}

export interface IRoomPatchDto {
    id: string;
    name: string;
    videoDuration: DurationType;
}

export interface IMediaDto {
    id: string;
    title: string;
    description: string;
    startTime: string | null;
    durationAsSeconds: number;
    elapsedTime: number;
    thumbnail: string;
    thumbnailHighRes: string;
    user: IUserIdentity;
}

export interface IPlaylistItemDto {
    id: string;
    videoId: string;
    title: string;
    durationAsSeconds: number;
    thumbnail: string;
    userId: string;
    nickname?: string;
}

export interface IMediaPlayerOption {
    volume: number;
}

export interface IMediaListItem {
    id: string;
    title: string;
    description: string;
    durationInSeconds: number;
    thumbnail: string;
}

export interface IMediaResponse {
    search?: string;
    media: IMediaListItem[];
}
