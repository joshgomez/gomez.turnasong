import React, { useEffect, useState } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from './stores';
import JwtService from './services/JwtService';
import UrlUtility from './utilities/UrlUtility';
import { authActions, AuthState, setIsAuthenticating, setTokenFromStorage } from './stores/auth.store';
import PageLoader from './pages/common/PageLoader';
import { setShowNavbar } from './stores/layout.store';

type IPrivateRouteProps = RouteProps;

const hashParams = UrlUtility.getHashParameters();

const PrivateRoute: React.FC<IPrivateRouteProps> = (props: IPrivateRouteProps) => {
    const authState = useSelector<AppState, AuthState>((state) => state.auth);
    const showNavbar = useSelector<AppState, boolean>((state) => state.layout.showNavbar);
    const dispatch = useDispatch();
    const { component, ...rest }: IPrivateRouteProps = props;

    useEffect(() => {
        const beginAuthAsync = async () => {
            if (!authState.isAuthenticating) {
                return;
            }

            if (hashParams['token']) {
                const token = decodeURIComponent(hashParams['token']);
                const refreshToken = decodeURIComponent(hashParams['refreshToken']);
                const expires = decodeURIComponent(hashParams['expires']);
                history.replaceState({}, document.title, window.location.href.split('#')[0]);
                dispatch(authActions.setTokenWithParm(token, refreshToken, expires));
                return;
            }

            if (window.localStorage.getItem('z')) {
                console.log('BeginAuth');
                dispatch(setTokenFromStorage());
                return;
            }

            dispatch(setIsAuthenticating(false));
        };

        beginAuthAsync();
    }, []);

    useEffect(() => {
        if (!showNavbar && authState.userId) {
            dispatch(setShowNavbar(true));
        }
    }, [authState.userId]);

    return (
        <Route
            {...rest}
            render={() => {
                if (authState.isAuthenticating) {
                    return <PageLoader />;
                }

                if (JwtService.isAuthenticated(authState.data)) {
                    return React.createElement(component, props);
                }

                return <Redirect to="/sign-in" />;
            }}
        />
    );
};

export default PrivateRoute;
