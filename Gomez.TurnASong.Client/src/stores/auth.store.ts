import { Dispatch } from 'react';
import { IAuthData, IJwtPayload } from '../interfaces/auth';
import JwtService from '../services/JwtService';
import SecureLS from 'secure-ls';
import { AppState } from '.';
import { AuthApiService } from '../services/api/AuthApiService';

const secureStorage = new SecureLS();
const accessInvalidationOffset = 10; //seconds before invalid access token.
let refreshTokenTimerId: number;

export const initialAuthData: IAuthData = {
    token: '',
    expireTime: 0,
    refreshToken: {
        token: '',
        expires: null,
    },
};

export type AuthState = {
    data: IAuthData;
    isAuthenticating: boolean;
    userId: string | null;
};

export const initialAuthState: AuthState = {
    data: initialAuthData,
    isAuthenticating: true,
    userId: null,
};

export type SetTokenAction = {
    readonly type: 'SetToken';
    readonly payload: IAuthData;
};

export type SetIsAuthenticatingAction = {
    readonly type: 'SetIsAuthenticating';
    readonly payload: boolean;
};

export type SetUserIdAction = {
    readonly type: 'SetUserId';
    readonly payload: string | null;
};

export const setToken = (data: IAuthData | null, payload?: IJwtPayload) => async (
    dispatch: Dispatch<SetTokenAction>,
): Promise<void> => {
    if (data === null) {
        secureStorage.remove('z');
        dispatch(setUserId(null) as any);
    } else {
        secureStorage.set('z', data);
        let userId = JwtService.getUserId(data);
        if (payload !== undefined) {
            userId = JwtService.getUserIdByJwt(payload);
        }

        dispatch(setUserId(userId) as any);
    }

    data = data ?? initialAuthData;
    dispatch({
        type: 'SetToken',
        payload: data,
    } as const);

    dispatch(setTokenFromRefreshToken() as any);
};

export const setTokenWithParm = (accessToken: string, refreshToken: string, expireDate: string) => async (
    dispatch: Dispatch<SetTokenAction>,
): Promise<void> => {
    const payload = JwtService.decodeToken(accessToken);
    dispatch(
        setToken({
            token: accessToken,
            expireTime: payload.exp,
            refreshToken: {
                token: refreshToken,
                expires: expireDate,
            },
        }) as any,
    );
};

export const setTokenFromStorage = () => async (dispatch: Dispatch<SetTokenAction>): Promise<void> => {
    const z: IAuthData | undefined = secureStorage.get('z');
    dispatch(setToken(z) as any);
};

export const setIsAuthenticating = (value: boolean) => async (
    dispatch: Dispatch<SetIsAuthenticatingAction>,
): Promise<void> => {
    dispatch({
        type: 'SetIsAuthenticating',
        payload: value,
    } as const);
};

export const setTokenFromRefreshToken = () => async (
    dispatch: Dispatch<SetTokenAction>,
    getState: () => AppState,
): Promise<void> => {
    let data: IAuthData | undefined = getState().auth.data;
    if (!JwtService.isAuthenticated(data) || !data.refreshToken.token) {
        return;
    }

    let remaining: number = JwtService.remainingSecondsToInvalidation(data, accessInvalidationOffset);
    if (remaining < 0) {
        //Access token have expired
        console.log('Authenticated');
        const api = new AuthApiService();
        data = await api.refreshAsync(data);
        remaining = JwtService.remainingSecondsToInvalidation(data, accessInvalidationOffset);
        dispatch(setToken(data) as any);
    }

    if (refreshTokenTimerId) {
        window.clearTimeout(refreshTokenTimerId);
    }

    console.log(`Authenticating in ${remaining} seconds`);
    refreshTokenTimerId = window.setTimeout(() => {
        if (JwtService.isAuthenticated(data)) {
            dispatch(setTokenFromRefreshToken() as any);
        }
    }, remaining * 1000);
};

export const setUserId = (value: string | null) => async (dispatch: Dispatch<SetUserIdAction>): Promise<void> => {
    dispatch({
        type: 'SetUserId',
        payload: value,
    } as const);
};

export const authActions = {
    setToken: setToken,
    setIsAuthenticating: setIsAuthenticating,
    setTokenWithParm: setTokenWithParm,
    setTokenFromRefreshToken: setTokenFromRefreshToken,
};
