import { Dispatch } from 'react';
import { IRoomDto, IMediaPlayerOption } from '../interfaces/multimedia';
import { DurationType } from '../models/DurationType';

export type RoomState = {
    data: IRoomDto;
    isLoading: boolean;
    mediaPlayerOption: IMediaPlayerOption;
};

export const initialRoomState: RoomState = {
    data: { id: '', name: '', currentMedia: null, owner: { id: null, name: null }, videoDuration: DurationType.Any },
    isLoading: false,
    mediaPlayerOption: { volume: 10 },
};

export type SetRoomAction = {
    readonly type: 'SetRoom';
    readonly payload: IRoomDto;
};

export type SetIsLoadingAction = {
    readonly type: 'SetIsLoading';
    readonly payload: boolean;
};

export type SetMediaPlayerOptionsAction = {
    readonly type: 'SetMediaPlayerOptions';
    readonly payload: IMediaPlayerOption;
};

export const setRoom = (value: IRoomDto) => async (dispatch: Dispatch<SetRoomAction>): Promise<void> => {
    dispatch({
        type: 'SetRoom',
        payload: value,
    } as const);
};

export const setIsLoading = (value: boolean) => async (dispatch: Dispatch<SetIsLoadingAction>): Promise<void> => {
    dispatch({
        type: 'SetIsLoading',
        payload: value,
    } as const);
};

export const setMediaPlayerOptions = (value: IMediaPlayerOption) => async (
    dispatch: Dispatch<SetMediaPlayerOptionsAction>,
): Promise<void> => {
    dispatch({
        type: 'SetMediaPlayerOptions',
        payload: value,
    } as const);
};

export const roomActions = {
    setRoom: setRoom,
    setIsLoading: setIsLoading,
    setMediaPlayerOptions: setMediaPlayerOptions,
};
