import { Dispatch } from 'react';
import { AppState } from '.';
import { RoomApiService } from '../services/api/RoomApiService';
import { MediaApiService } from '../services/api/MediaApiService';
import { ProfileApiService } from '../services/api/ProfileApiService';

export type ServiceState = {
    roomApiService: RoomApiService | null;
    mediaApiService: MediaApiService | null;
    profileApiService: ProfileApiService | null;
    isLoaded: boolean;
};

export const initialServiceState: ServiceState = {
    roomApiService: null,
    mediaApiService: null,
    profileApiService: null,
    isLoaded: false,
};

export type SetServiceStateAction = {
    readonly type: 'SetServiceState';
    readonly payload: ServiceState;
};

export type SetIsLoadedAction = {
    readonly type: 'SetIsLoaded';
    readonly payload: boolean;
};

export const setRoomApiService = () => async (
    dispatch: Dispatch<SetServiceStateAction>,
    getState: () => AppState,
): Promise<void> => {
    const currentState: ServiceState = getState().services;
    const token: string = getState().auth.data.token;
    dispatch({
        type: 'SetServiceState',
        payload: {
            ...currentState,
            roomApiService: new RoomApiService(token),
            mediaApiService: new MediaApiService(token),
            profileApiService: new ProfileApiService(token),
        },
    } as const);
};

export const setIsLoaded = (value: boolean) => async (dispatch: Dispatch<SetIsLoadedAction>): Promise<void> => {
    dispatch({
        type: 'SetIsLoaded',
        payload: value,
    } as const);
};

export const serviceActions = {
    setRoomApiService: setRoomApiService,
    setIsLoaded: setIsLoaded,
};
