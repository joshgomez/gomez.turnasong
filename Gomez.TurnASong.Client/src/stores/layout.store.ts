import { Dispatch } from 'react';
import { IToastItem } from '../interfaces/layout';
import { AppState } from '.';
import { createSelector } from 'reselect';

export type ToasterState = {
    entries: IToastItem[];
};

export type LayoutState = {
    isMenuOpen: Record<string, boolean>;
    toaster: ToasterState;
    showNavbar: boolean;
    title: string;
};

export const initialLayoutState: LayoutState = {
    showNavbar: false,
    isMenuOpen: {},
    toaster: { entries: [] },
    title: '',
};

export type SetIsMenuOpenAction = {
    readonly type: 'LayoutSetIsMenuOpen';
    readonly payload: { key: string; value: boolean | null };
};

export type SetShowNavbarAction = {
    readonly type: 'LayoutSetShowNavbar';
    readonly payload: boolean;
};

export type SetTitleAction = {
    readonly type: 'LayoutSetTitle';
    readonly payload: string;
};

export type SetToasterAction = {
    readonly type: 'LayoutSetToaster';
    readonly payload: ToasterState;
};

export type ClearIsMenuOpenAction = {
    readonly type: 'LayoutClearIsMenuOpen';
};

export const setIsMenuOpen = (key: string, value: boolean | null) => async (
    dispatch: Dispatch<SetIsMenuOpenAction>,
) => {
    dispatch({
        type: 'LayoutSetIsMenuOpen',
        payload: { key: key, value: value },
    } as const);
};

export const clearIsMenuOpen = () => async (dispatch: Dispatch<ClearIsMenuOpenAction>) => {
    dispatch({
        type: 'LayoutClearIsMenuOpen',
    } as const);
};

export const setShowNavbar = (value: boolean) => async (dispatch: Dispatch<SetShowNavbarAction>) => {
    dispatch({
        type: 'LayoutSetShowNavbar',
        payload: value,
    } as const);
};

export const setTitle = (value: string) => async (dispatch: Dispatch<SetTitleAction>) => {
    document.title = 'Turn a Song';
    if (value) {
        document.title = `${value} - Turn a Song`;
    }

    dispatch({
        type: 'LayoutSetTitle',
        payload: value,
    } as const);
};

export const stackToast = (value: IToastItem) => async (
    dispatch: Dispatch<SetToasterAction>,
    getState: () => AppState,
): Promise<void> => {
    const toaster = getState().layout.toaster;
    const newToaster = { ...toaster, entries: [value, ...toaster.entries] };
    if (newToaster.entries.length > 5) {
        newToaster.entries.pop();
    }

    dispatch({
        type: 'LayoutSetToaster',
        payload: newToaster,
    } as const);
};

export const editToast = (value: IToastItem, index: number) => async (
    dispatch: Dispatch<SetToasterAction>,
    getState: () => AppState,
): Promise<void> => {
    const toaster = getState().layout.toaster;
    const newToaster = { ...toaster, entries: [...toaster.entries] };
    newToaster.entries[index] = value;

    dispatch({
        type: 'LayoutSetToaster',
        payload: newToaster,
    } as const);
};

export const layoutActions = {
    setShowNavbar: setShowNavbar,
    setIsMenuOpen: setIsMenuOpen,
    clearIsMenuOpen: clearIsMenuOpen,
    setTitle: setTitle,
    stackToast: stackToast,
};

export const showingToastsCount = createSelector<AppState, IToastItem[], number>(
    (state) => state.layout.toaster.entries,
    (entries) => entries.filter((entry) => entry.show).length,
);
