import { Dispatch } from 'react';
import * as signalR from '@microsoft/signalr';
import { AppState } from '.';
import { Config } from '../config';

export type RoomHubState = {
    hub: signalR.HubConnection | null;
    isLoaded: boolean;
    pingTimer: number;
};

export const initialRoomHubState: RoomHubState = {
    hub: null,
    isLoaded: false,
    pingTimer: 0,
};

export type SetHubAction = {
    readonly type: 'SetHub';
    readonly payload: signalR.HubConnection | null;
};

export type SetIsLoadedAction = {
    readonly type: 'HubSetIsLoaded';
    readonly payload: boolean;
};

export type SetPingTimerAction = {
    readonly type: 'HubSetPingTimer';
    readonly payload: number;
};

export const startHub = () => async (dispatch: Dispatch<SetHubAction>, getState: () => AppState) => {
    let hub: signalR.HubConnection | null = null;
    if (getState().auth.data.token) {
        hub = new signalR.HubConnectionBuilder()
            .withUrl(Config.apiEndPoint + '/hubs/room', {
                accessTokenFactory: () => {
                    return getState().auth.data.token;
                },
                // transport: signalR.HttpTransportType.WebSockets,
                // skipNegotiation: true
            } as signalR.IHttpConnectionOptions)
            .build();

        const startAsync = async () => {
            try {
                await hub.start();
                const pingTimer = getState().roomHub.pingTimer;
                if (pingTimer) {
                    clearInterval(pingTimer);
                }

                hub.on('pinged', (dateStr: string) => {
                    console.log('ExpireAt', dateStr);
                });

                dispatch(
                    setPingTimer(
                        window.setInterval(() => {
                            if (hub != null) {
                                hub.send('ping');
                            }
                        }, 1000 * 60),
                    ) as any,
                );

                console.assert(hub.state === signalR.HubConnectionState.Connected);
                console.log('connected');
                dispatch({
                    type: 'SetHub',
                    payload: hub,
                } as const);
            } catch (err) {
                console.assert(hub.state === signalR.HubConnectionState.Disconnected);
                console.log(err);
                setTimeout(() => startAsync(), 5000);
            }
        };

        await startAsync();
    }
};

export const disconnectHub = () => async (
    dispatch: Dispatch<SetHubAction>,
    getState: () => AppState,
): Promise<void> => {
    const isLoaded = getState().roomHub.isLoaded;
    const hub = getState().roomHub.hub;
    if (!isLoaded || hub === null) {
        return;
    }

    const pingTimer = getState().roomHub.pingTimer;
    if (pingTimer) {
        clearInterval(pingTimer);
        dispatch(setPingTimer(0) as any);
    }

    hub.off('pinged');
    await hub.stop();
    console.log('disconnectHub');

    dispatch({
        type: 'SetHub',
        payload: null,
    } as const);
};

export const setIsLoaded = (value: boolean) => async (dispatch: Dispatch<SetIsLoadedAction>): Promise<void> => {
    dispatch({
        type: 'HubSetIsLoaded',
        payload: value,
    } as const);
};

const setPingTimer = (value: number) => async (dispatch: Dispatch<SetPingTimerAction>): Promise<void> => {
    dispatch({
        type: 'HubSetPingTimer',
        payload: value,
    } as const);
};

export const roomHubActions = {
    startHub: startHub,
    setIsLoaded: setIsLoaded,
    disconnectHub: disconnectHub,
};
