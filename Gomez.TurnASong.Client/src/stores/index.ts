import { createHashHistory } from 'history';
import { routerMiddleware, RouterState } from 'react-router-redux';
import { applyMiddleware, compose, createStore, Store } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import { AuthState } from './auth.store';
import { ServiceState } from './service.store';
import { RoomState } from './room.store';
import { RoomHubState } from './roomhub.store';
import { LayoutState } from './layout.store';

export type AppState = {
    routing: RouterState;
    auth: AuthState;
    services: ServiceState;
    room: RoomState;
    roomHub: RoomHubState;
    layout: LayoutState;
};

const reduxRouterMiddleware = routerMiddleware(createHashHistory());
const middleware = [thunk, reduxRouterMiddleware];
const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware))) as Store<AppState>;
