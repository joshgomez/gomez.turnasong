import { precacheAndRoute } from 'workbox-precaching';
precacheAndRoute(self.__WB_MANIFEST);

const version = '1.0';
self.addEventListener('install', (e) => {
    console.log(`[ServiceWorker] Installed ${version}`);
});

self.addEventListener('activate', (e) => {
    console.log('[ServiceWorker] Activated');
});

self.addEventListener('message', function (event) {
    if (event.data.action === 'skipWaiting') {
        self.skipWaiting();
    }
});
