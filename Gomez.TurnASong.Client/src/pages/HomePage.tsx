import React, { useEffect } from 'react';
import Rooms from './HomePage/Rooms';
import { Row, Col, Card } from 'react-bootstrap';
import CardHeader from './common/CardHeader';
import { useDispatch } from 'react-redux';
import { setTitle } from '../stores/layout.store';
import { useMenu } from '../models/hooks/useMenu';
import withAppInsTracking from '../models/AppInsights';

const HomePage: React.FC = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(setTitle(''));
    }, []);

    useMenu(['add'], [false]);

    return (
        <>
            <Row>
                <Col xs={12} md={3}></Col>
                <Col xs={12} md={6}>
                    <Card border="primary">
                        <CardHeader>Rooms</CardHeader>
                        <Card.Body>
                            <Rooms />
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3}></Col>
            </Row>
        </>
    );
};

export default withAppInsTracking(HomePage);
