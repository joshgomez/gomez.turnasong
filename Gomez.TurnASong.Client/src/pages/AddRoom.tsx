import React, { useState, useEffect } from 'react';
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../stores';
import { useHistory } from 'react-router-dom';
import CardHeader from './common/CardHeader';
import { setTitle } from '../stores/layout.store';
import { RoomApiService } from '../services/api/RoomApiService';
import { IRoomPostDto } from '../interfaces/multimedia';
import { useMenu } from '../models/hooks/useMenu';
import { ValidationConstant } from '../constants/ValidationConstant';
import withAppInsTracking from '../models/AppInsights';

const AddRoom: React.FC = () => {
    const [viewModel, setViewModel] = useState<IRoomPostDto | null>({ name: '' });
    const roomApiService = useSelector<AppState, RoomApiService>((state) => state.services.roomApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const history = useHistory();
    const dispatch = useDispatch();
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        dispatch(setTitle('Add room'));
    }, []);

    useMenu(['add'], [true]);

    const handleSubmit = async (evt: React.FormEvent<HTMLElement>) => {
        const form = evt.currentTarget as HTMLFormElement;
        evt.preventDefault();
        evt.stopPropagation();
        if (!isServicesLoaded || viewModel === undefined || form.checkValidity() === false) {
            setValidated(true);

            return;
        }

        setValidated(false);
        const ret = await roomApiService.postAsync(viewModel);
        if (ret !== null) {
            history.push(`/rooms-settings/${ret.id}`);
            return;
        }

        history.push('/');
    };

    const handleCancel = async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        history.push('/');
    };

    return (
        <>
            <Row>
                <Col xs={12} md={3}></Col>
                <Col xs={12} md={6}>
                    <Card border="primary">
                        <CardHeader>Create your community</CardHeader>
                        <Card.Body>
                            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                                <Form.Group controlId="name">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        required
                                        minLength={ValidationConstant.roomNameMinLength}
                                        maxLength={ValidationConstant.roomNameMaxLength}
                                        placeholder="Room name..."
                                        aria-label="Type a name..."
                                        value={viewModel?.name ?? ''}
                                        onChange={(e) => setViewModel({ ...viewModel, name: e.target.value })}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col md={6}>
                                            <Button
                                                type="button"
                                                onClick={handleCancel}
                                                block
                                                aria-label="Cancel..."
                                                className="mb-3"
                                                variant="secondary"
                                            >
                                                <FontAwesomeIcon icon={faTimes} />
                                            </Button>
                                        </Col>
                                        <Col md={6}>
                                            <Button type="submit" block aria-label="Add room..." variant="primary">
                                                <FontAwesomeIcon icon={faPlusSquare} />
                                            </Button>
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3}></Col>
            </Row>
        </>
    );
};

export default withAppInsTracking(AddRoom);
