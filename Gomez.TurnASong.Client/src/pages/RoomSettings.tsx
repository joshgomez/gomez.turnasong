import React, { useState, useEffect } from 'react';
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faTimes, faTrash } from '@fortawesome/free-solid-svg-icons';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../stores';
import { useHistory, useParams } from 'react-router-dom';
import CardHeader from './common/CardHeader';
import { setTitle } from '../stores/layout.store';
import { RoomApiService } from '../services/api/RoomApiService';
import { IRoomDto } from '../interfaces/multimedia';
import ConfirmModal from './common/ConfirmModal';
import { DurationType } from '../models/DurationType';
import PageLoader from './common/PageLoader';
import { useMenu } from '../models/hooks/useMenu';
import { ValidationConstant } from '../constants/ValidationConstant';
import withAppInsTracking from '../models/AppInsights';
import { useAppInsights } from '../models/AppInsightsContext';
import { formInvalidSubmitted } from '../models/AppInsightsEvents';

interface IParamTypes {
    id: string;
}

const RoomSettings: React.FC = () => {
    const [viewModel, setViewModel] = useState<IRoomDto | null>(null);
    const roomApiService = useSelector<AppState, RoomApiService>((state) => state.services.roomApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const history = useHistory();
    const { id } = useParams<IParamTypes>();
    const [confirmDelete, setConfirmDelete] = useState<boolean>(false);
    const [validated, setValidated] = useState(false);
    const ai = useAppInsights();

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(setTitle('Room settings'));
    }, []);
    useMenu(['add'], [false]);

    useEffect(() => {
        const beginAsync = async () => {
            const result = await roomApiService.ajaxGetAsync<IRoomDto | null>(`/${id}`);
            setViewModel(result);
        };

        if (isServicesLoaded) {
            beginAsync();
        }
    }, [isServicesLoaded]);

    const handleSubmit = async (evt: React.FormEvent<HTMLElement>) => {
        const form = evt.currentTarget as HTMLFormElement;
        evt.preventDefault();
        evt.stopPropagation();
        if (!isServicesLoaded || viewModel === undefined || form.checkValidity() === false) {
            setValidated(true);
            ai.trackEvent(formInvalidSubmitted, { name: 'RoomSettings' });
        }

        setValidated(false);
        const ret = await roomApiService.patchAsync({
            id: viewModel.id,
            name: viewModel.name,
            videoDuration: viewModel.videoDuration,
        });
        if (ret !== null) {
            history.push(`/rooms/${ret.id}`);
        }

        history.push('/');
    };

    const handleCancel = async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        history.push(`/rooms/${id}`);
    };

    const handleConfirmDelete = async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        setConfirmDelete(true);
    };

    const handleCancelDelete = async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        setConfirmDelete(false);
    };

    const handleDelete = async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        setConfirmDelete(false);
        await roomApiService.deleteAsync(id);
        history.push('/');
    };

    if (viewModel == null) {
        return <PageLoader />;
    }

    return (
        <>
            <Row>
                <Col xs={12} md={3}></Col>
                <Col xs={12} md={6}>
                    <Card border="primary">
                        <CardHeader>Manage room</CardHeader>
                        <Card.Body>
                            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                                <Form.Group controlId="name">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        required
                                        minLength={ValidationConstant.roomNameMinLength}
                                        maxLength={ValidationConstant.roomNameMaxLength}
                                        placeholder="Room name..."
                                        aria-label="Type a name..."
                                        value={viewModel?.name ?? ''}
                                        onChange={(e) => setViewModel({ ...viewModel, name: e.target.value })}
                                    />
                                </Form.Group>
                                <Form.Group controlId="exampleForm.ControlSelect1">
                                    <Form.Label>Video duration</Form.Label>
                                    <Form.Control
                                        as="select"
                                        defaultValue={viewModel?.videoDuration}
                                        onChange={(e) =>
                                            setViewModel({ ...viewModel, videoDuration: DurationType[e.target.value] })
                                        }
                                    >
                                        <option key={0} value={DurationType.Any}>
                                            Any
                                        </option>
                                        <option key={1} value={DurationType.Short}>
                                            Short
                                        </option>
                                        <option key={2} value={DurationType.Medium}>
                                            Medium
                                        </option>
                                        <option key={3} value={DurationType.Long}>
                                            Long
                                        </option>
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col md={6}>
                                            <Button
                                                type="button"
                                                onClick={handleCancel}
                                                block
                                                title="Cancel..."
                                                className="mb-3"
                                                variant="secondary"
                                            >
                                                <FontAwesomeIcon icon={faTimes} />
                                            </Button>
                                        </Col>
                                        <Col md={6}>
                                            <Button type="submit" block title="Save room..." variant="primary">
                                                <FontAwesomeIcon icon={faSave} />
                                            </Button>
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Form.Group>
                                    <Button variant="outline-danger" title="Delete room" onClick={handleConfirmDelete}>
                                        <FontAwesomeIcon icon={faTrash} />
                                    </Button>
                                </Form.Group>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3}></Col>
            </Row>
            <ConfirmModal show={confirmDelete} onClose={handleCancelDelete} onYesClick={handleDelete} />
        </>
    );
};

export default withAppInsTracking(RoomSettings);
