import React from 'react';
import { Card, CardProps } from 'react-bootstrap';

type ICardHeaderProps = CardProps;

const CardHeader: React.FC<ICardHeaderProps> = (props: ICardHeaderProps) => {
    const { children, ...rest }: ICardHeaderProps = props;
    return (
        <Card.Header {...rest} className="bg-primary text-white">
            {children}
        </Card.Header>
    );
};

export default CardHeader;
