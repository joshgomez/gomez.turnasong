import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

interface ITabProps extends React.HTMLProps<HTMLSpanElement> {
    icon: IconProp;
    srOnly?: boolean;
    title: string;
}

const TabIcon: React.FC<ITabProps> = (props: ITabProps) => {
    const { icon: icon, srOnly: srOnly, ...rest }: ITabProps = props;

    return (
        <span {...rest}>
            <FontAwesomeIcon icon={icon} />
            <span className={srOnly ? 'sr-only' : ''}>{rest.title}</span>
        </span>
    );
};

export default TabIcon;
