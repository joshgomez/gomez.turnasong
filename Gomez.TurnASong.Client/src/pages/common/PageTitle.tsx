import React from 'react';

type IPageTitleProps = React.HTMLProps<HTMLHeadingElement>;

const PageTitle: React.FC<IPageTitleProps> = (props: IPageTitleProps) => {
    return <h2 {...props}>{props.title}</h2>;
};

export default PageTitle;
