import React, { useEffect } from 'react';
import { Config } from '../../config';

const AdSenseToast: React.FC = () => {
    useEffect(() => {
        loadGoogleAd();
    }, []);

    const loadGoogleAd = () => {
        try {
            (globalThis.adsbygoogle = (window as any).adsbygoogle || []).push({});
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <ins
            className="adsbygoogle"
            style={{ display: 'block', minHeight: 60 }}
            data-adtest={Config.adsTest ? 'on' : 'off'}
            data-ad-format="fluid"
            data-ad-layout-key="-gw-3+1f-3d+2z"
            data-ad-client={Config.adsGoogleClient}
            data-ad-slot="1328902734"
        >
            A add will be showing here.
        </ins>
    );
};

export default AdSenseToast;
