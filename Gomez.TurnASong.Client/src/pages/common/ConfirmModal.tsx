import React from 'react';
import { Modal, Button, ModalProps } from 'react-bootstrap';

interface IConfirmModalProps extends ModalProps {
    show: boolean;
    onYesClick: (evt: React.MouseEvent<HTMLElement, MouseEvent>) => void | Promise<void>;
    onNoClick?: (evt: React.MouseEvent<HTMLElement, MouseEvent>) => void | Promise<void>;
    onClose: (evt: React.MouseEvent<HTMLElement, MouseEvent> | any) => void | Promise<void>;
    message?: string | React.ReactNode;
}

const ConfirmModal: React.FC<IConfirmModalProps> = (props: IConfirmModalProps) => {
    const { onYesClick: onYesClick, onNoClick: onNoClick, onClose: onClose, ...rest }: IConfirmModalProps = props;

    return (
        <Modal {...rest} className="confirm-modal" show={props.show} onHide={onClose}>
            <Modal.Body>
                <h5 className="confirm-message">
                    {props.message ? props.message : 'Are you sure you want to proceed?'}
                </h5>
            </Modal.Body>
            <Modal.Footer className="confirm-modal-footer">
                <Button variant="primary" onClick={onYesClick}>
                    Yes
                </Button>
                <Button variant="danger" onClick={onNoClick ?? onClose}>
                    No
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default ConfirmModal;
