import React, { HTMLAttributes } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

type ISectionLoaderProps = HTMLAttributes<HTMLDivElement>;

const SectionLoader: React.FC<ISectionLoaderProps> = (props: ISectionLoaderProps) => {
    return (
        <div {...props} className="text-center">
            <h2 className="mb-3" aria-busy="true" aria-live="polite">
                Loading...
            </h2>
            <FontAwesomeIcon icon={faSpinner} size="5x" pulse />
        </div>
    );
};

export default SectionLoader;
