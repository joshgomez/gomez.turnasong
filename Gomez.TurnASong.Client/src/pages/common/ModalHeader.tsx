import React from 'react';
import { Modal } from 'react-bootstrap';
import { ModalHeaderProps } from 'react-bootstrap/esm/ModalHeader';
type IModalHeaderProps = ModalHeaderProps;

const ModalHeader: React.FC<IModalHeaderProps> = (props: IModalHeaderProps) => {
    const { children, ref, ...rest }: IModalHeaderProps = props;
    return (
        <Modal.Header {...rest} className="bg-primary text-white">
            <Modal.Title>{children}</Modal.Title>
        </Modal.Header>
    );
};

export default ModalHeader;
