import React from 'react';
import { Row, Col, ColProps } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

type IPageLoaderProps = ColProps;

const PageLoader: React.FC<IPageLoaderProps> = (props: IPageLoaderProps) => {
    return (
        <Row>
            <Col {...props} className="text-center">
                <h2 className="mb-5" role="alert" aria-busy="true">
                    Loading...
                </h2>
                <FontAwesomeIcon icon={faSpinner} size="10x" pulse />
            </Col>
        </Row>
    );
};

export default PageLoader;
