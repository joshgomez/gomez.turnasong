import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserEdit, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../stores';
import { ProfileApiService } from '../services/api/ProfileApiService';
import { IProfileDto } from '../interfaces/identity';
import { useHistory } from 'react-router-dom';
import { setToken } from '../stores/auth.store';
import CardHeader from './common/CardHeader';
import { setTitle } from '../stores/layout.store';
import PageLoader from './common/PageLoader';
import { useMenu } from '../models/hooks/useMenu';
import withAppInsTracking from '../models/AppInsights';
import { useAppInsights } from '../models/AppInsightsContext';
import { signOutClicked } from '../models/AppInsightsEvents';

const Profile: React.FC = () => {
    const profileApiService = useSelector<AppState, ProfileApiService>((state) => state.services.profileApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const userId = useSelector<AppState, string>((state) => state.auth.userId);
    const [viewModel, setViewModel] = useState<IProfileDto | null>(null);
    const history = useHistory();
    const dispatch = useDispatch();
    const ai = useAppInsights();

    useEffect(() => {
        dispatch(setTitle('Profile'));
    }, []);

    useMenu(['add'], [false]);

    useEffect(() => {
        const beginAsync = async () => {
            const result = await profileApiService.getAsync(userId);
            setViewModel(result);
        };

        if (isServicesLoaded) {
            beginAsync();
        }
    }, [isServicesLoaded]);

    const handleEdit = () => {
        history.push('/profile/settings');
    };

    const handleLogout = () => {
        dispatch(setToken(null));
        ai.trackEvent(signOutClicked);
        window.location.href = window.location.origin;
    };

    if (viewModel == null) {
        return <PageLoader />;
    }

    return (
        <>
            <Row>
                <Col xs={12} md={3}></Col>
                <Col xs={12} md={6}>
                    <Card border="primary">
                        <CardHeader>{viewModel?.userName}</CardHeader>
                        <Card.Body>
                            <Card.Text>
                                Your nickname is <strong>{viewModel?.nickname}</strong>
                            </Card.Text>
                            <Button variant="primary" block onClick={handleEdit}>
                                <FontAwesomeIcon icon={faUserEdit} />
                            </Button>
                            <hr />
                            <Button className="mt-3" variant="outline-danger" block onClick={handleLogout}>
                                <FontAwesomeIcon icon={faSignOutAlt} />
                            </Button>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3}></Col>
            </Row>
        </>
    );
};

export default withAppInsTracking(Profile);
