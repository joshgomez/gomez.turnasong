import React, { useEffect } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeartBroken } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from 'react-redux';
import { setTitle } from '../stores/layout.store';
import PageTitle from './common/PageTitle';
import { useHistory } from 'react-router-dom';
import withAppInsTracking from '../models/AppInsights';

const NotFound: React.FC = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    useEffect(() => {
        dispatch(setTitle('Not found'));
    }, []);

    return (
        <>
            <Row>
                <Col></Col>
                <Col className="text-center">
                    <PageTitle title="Not found" />
                </Col>
                <Col></Col>
            </Row>
            <Row>
                <Col></Col>
                <Col className="text-center">
                    <span>
                        <FontAwesomeIcon icon={faHeartBroken} size="10x" />
                    </span>
                </Col>
                <Col></Col>
            </Row>
            <Row>
                <Col></Col>
                <Col className="text-center">
                    <Button
                        variant="link"
                        onClick={() => {
                            history.goBack();
                        }}
                    >
                        Go back
                    </Button>
                </Col>
                <Col></Col>
            </Row>
        </>
    );
};

export default withAppInsTracking(NotFound);
