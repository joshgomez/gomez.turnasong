import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Modal } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../stores';
import { RoomApiService } from '../services/api/RoomApiService';
import { IRoomDto } from '../interfaces/multimedia';
import { useParams, Link } from 'react-router-dom';
import { roomActions } from '../stores/room.store';
import { roomHubActions } from '../stores/roomhub.store';
import MediaSearch from './Room/MediaSearch';
import YouTubePlayer from './Room/YouTubePlayer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsers, faCog } from '@fortawesome/free-solid-svg-icons';
import RoomChat from './Room/RoomChat';
import Playlist from './Room/Playlist';
import { setIsMenuOpen, setTitle, showingToastsCount, stackToast } from '../stores/layout.store';
import CardHeader from './common/CardHeader';
import ModalHeader from './common/ModalHeader';
import WaitingList from './Room/WaitingList';
import { useMenu } from '../models/hooks/useMenu';
import PageLoader from './common/PageLoader';
import { useInterval } from '../models/hooks/useInterval';
import AdSenseToast from './common/AdSenseToast';
import withAppInsTracking from '../models/AppInsights';

interface IParamTypes {
    id: string;
}

const Room: React.FC = () => {
    const toastCount = useSelector(showingToastsCount);
    const roomApiService = useSelector<AppState, RoomApiService>((state) => state.services.roomApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const isHubLoaded = useSelector<AppState, boolean>((state) => state.roomHub.isLoaded);
    const hub = useSelector<AppState, signalR.HubConnection>((state) => state.roomHub.hub);
    const isMenuOpen = useSelector<AppState, Record<string, boolean>>((state) => state.layout.isMenuOpen);
    const data = useSelector<AppState, IRoomDto | null>((state) => state.room.data);
    const isLoading = useSelector<AppState, boolean>((state) => state.room.isLoading);
    const userId = useSelector<AppState, string>((state) => state.auth.userId);
    const { id } = useParams<IParamTypes>();
    const [onlineCount, setOnlineCount] = useState(0);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setTitle(data?.name));
    }, []);

    useMenu(['search', 'playlist', 'waitlist'], [false, false, false]);

    useEffect(() => {
        const beginAsync = async () => {
            if (!isLoading) {
                dispatch(roomActions.setIsLoading(true));
                const ret = await roomApiService.joinAsync(id);
                dispatch(roomActions.setRoom(ret));
                console.log('Joining a new room', ret.name);
                dispatch(setTitle(ret.name));
                console.log('Init hub');
                dispatch(roomHubActions.startHub());
                window.addEventListener('beforeunload', disconnect);
            }
        };

        if (isServicesLoaded && id && !isHubLoaded) {
            beginAsync();
        }

        return () => {
            if (isHubLoaded && !isLoading) {
                console.log('disconnecting at room');
                window.removeEventListener('beforeunload', disconnect);
                disconnect();
            }
        };
    }, [isServicesLoaded, id, isHubLoaded]);

    const disconnect = () => {
        dispatch(roomHubActions.disconnectHub());
    };

    useEffect(() => {
        if (isHubLoaded) {
            hub.on('roomUpdated', (room: IRoomDto) => {
                if (room !== null) {
                    dispatch(roomActions.setRoom(room));
                }
            });

            hub.on('onlineCountUpdated', (count: number) => {
                setOnlineCount(count);
            });

            hub.send('HasJoined');
        }

        return () => {
            if (isHubLoaded) {
                hub.off('roomUpdated');
                hub.off('onlineCountUpdated');
            }
        };
    }, [isHubLoaded]);

    useInterval(() => {
        if (toastCount < 3 && data?.currentMedia === null) {
            dispatch(stackToast({ title: 'Ad', autoHide: true, delay: 10000, message: <AdSenseToast />, show: true }));
        }
    }, 1000 * 60);

    if (!isHubLoaded || isLoading) {
        return <PageLoader />;
    }

    return (
        <>
            <Row>
                <Col xs={12} md={12} lg={8}>
                    <FontAwesomeIcon icon={faUsers} />
                    <span className="ml-2">{onlineCount}</span>
                </Col>
                <Col xs={12} md={12} lg={4} className="text-lg-right">
                    {userId === data?.owner.id && (
                        <Link to={`/rooms-settings/${data?.id}`}>
                            <FontAwesomeIcon icon={faCog} />
                            <span className="ml-2">Settings</span>
                        </Link>
                    )}
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={12} lg={8} className="mb-3">
                    <Card style={{ minHeight: 509 }} border="primary">
                        <CardHeader>{data?.currentMedia?.title ?? 'Currently no video is playing'}</CardHeader>
                        <Card.Body>
                            <YouTubePlayer videoId={data?.currentMedia?.id} />
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={12} lg={4} className="mb-3">
                    <Card style={{ minHeight: 509 }} border="primary">
                        <CardHeader>Chat</CardHeader>
                        <Card.Body>
                            <RoomChat />
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Modal
                show={isMenuOpen['search']}
                onHide={() => {
                    dispatch(setIsMenuOpen('search', false));
                }}
            >
                <ModalHeader closeButton>Pick a video</ModalHeader>
                <Modal.Body>
                    <MediaSearch />
                </Modal.Body>
                <Modal.Footer></Modal.Footer>
            </Modal>
            <Modal
                animation
                container={document.body}
                show={isMenuOpen['playlist']}
                onHide={() => {
                    dispatch(setIsMenuOpen('playlist', false));
                }}
            >
                <ModalHeader closeButton>Playlist</ModalHeader>
                <Modal.Body>
                    <Playlist />
                </Modal.Body>
                <Modal.Footer></Modal.Footer>
            </Modal>
            <Modal
                show={isMenuOpen['waitlist']}
                onHide={() => {
                    dispatch(setIsMenuOpen('waitlist', false));
                }}
            >
                <ModalHeader closeButton>Waiting list</ModalHeader>
                <Modal.Body>
                    <WaitingList />
                </Modal.Body>
                <Modal.Footer></Modal.Footer>
            </Modal>
        </>
    );
};

export default withAppInsTracking(Room);
