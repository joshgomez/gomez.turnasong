import React, { useEffect } from 'react';
import { Button, Form, Row, Col, Card } from 'react-bootstrap';
import { Config } from '../config';
import { useDispatch } from 'react-redux';
import { setShowNavbar } from '../stores/layout.store';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faGoogle, faMicrosoft } from '@fortawesome/free-brands-svg-icons';
import withAppInsTracking from '../models/AppInsights';
import { useAppInsights } from '../models/AppInsightsContext';
import { signInButtonClicked } from '../models/AppInsightsEvents';

const SignIn: React.FC = () => {
    const dispatch = useDispatch();
    const ai = useAppInsights();
    useEffect(() => {
        dispatch(setShowNavbar(false));
    }, []);

    const handleSubmit = (evt: React.FormEvent<HTMLElement>) => {
        const form = evt.currentTarget as HTMLFormElement;
        const inputElm = form.elements['provider'] as HTMLInputElement;
        ai.trackEvent(signInButtonClicked, { provider: inputElm.value });
    };

    return (
        <>
            <Row className="d-flex text-center">
                <Col sm={6} md={5} lg={4} className="mx-auto pt-5">
                    <h1 className="logo-front">T</h1>
                    <Card className="bg-transparent border-0">
                        <Card.Body>
                            <Card.Title className="sr-only">Sign in</Card.Title>
                            <Form action={`${Config.apiEndPoint}/externaltoken`} method="post" onSubmit={handleSubmit}>
                                <Form.Control type="hidden" name="provider" value="Microsoft" />
                                <Form.Control type="hidden" name="returnUrl" value={window.location.origin} />
                                <Form.Group>
                                    <Button variant="primary" type="submit" block className="text-left">
                                        <FontAwesomeIcon icon={faMicrosoft} className="ml-5" />
                                        <span className="ml-2">Sign in with Microsoft</span>
                                    </Button>
                                </Form.Group>
                            </Form>
                            <Form action={`${Config.apiEndPoint}/externaltoken`} method="post" onSubmit={handleSubmit}>
                                <Form.Control type="hidden" name="provider" value="Google" />
                                <Form.Control type="hidden" name="returnUrl" value={window.location.origin} />
                                <Form.Group>
                                    <Button variant="primary" type="submit" block className="text-left">
                                        <FontAwesomeIcon icon={faGoogle} className="ml-5" />
                                        <span className="ml-2 w-100">Sign in with Google</span>
                                    </Button>
                                </Form.Group>
                            </Form>
                            <Form action={`${Config.apiEndPoint}/externaltoken`} method="post" onSubmit={handleSubmit}>
                                <Form.Control type="hidden" name="provider" value="Facebook" />
                                <Form.Control type="hidden" name="returnUrl" value={window.location.origin} />
                                <Form.Group>
                                    <Button variant="primary" type="submit" block className="text-left">
                                        <FontAwesomeIcon icon={faFacebook} className="ml-5" />
                                        <span className="ml-2">Sign in with Facebook</span>
                                    </Button>
                                </Form.Group>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </>
    );
};

export default withAppInsTracking(SignIn);
