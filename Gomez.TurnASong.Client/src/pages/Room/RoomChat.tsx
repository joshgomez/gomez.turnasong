import React, { useEffect, useState, forwardRef, HtmlHTMLAttributes } from 'react';
import { Row, Col, Form, InputGroup, Button, Tabs, Tab, ListGroup, Fade } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { AppState } from '../../stores';
import { faPaperPlane, faComments, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TabIcon from '../common/TabIcon';
import { FixedSizeList } from 'react-window';
import { IChatItem, IUserItem } from '../../interfaces/chat';
import { IReactWindowItem } from '../../interfaces/common';
import ChatItem from './ChatItem';
import { ValidationConstant } from '../../constants/ValidationConstant';

const innerElementType = forwardRef<HTMLUListElement, HtmlHTMLAttributes<HTMLUListElement>>(
    ({ style, ...rest }: HtmlHTMLAttributes<HTMLUListElement>, ref) => (
        <ul
            ref={ref}
            className={`list-unstyled p-2`}
            style={{
                ...style,
            }}
            {...rest}
        />
    ),
);
innerElementType.displayName = 'RoomChatListElement';

const Item: React.FC<IReactWindowItem<IChatItem[]>> = ({ index, style, data }: IReactWindowItem<IChatItem[]>) => (
    <ChatItem index={index} style={style} messages={data} />
);
Item.displayName = 'ChatItem';

interface IListProps {
    data: IChatItem[];
}

const listPropsAreEqual = (prevProps: IListProps, nextProps: IListProps) => {
    // Check if the arrays are the same length
    if (prevProps.data.length !== nextProps.data.length) return false;

    // Check if all items exist and are in the same order
    for (let i = 0; i < prevProps.data.length; i++) {
        if (prevProps.data[i].message !== nextProps.data[i].message) return false;
    }

    return true;
};

const ChatList = React.memo(
    ({ data }: IListProps) => (
        <FixedSizeList
            itemCount={data.length}
            innerElementType={innerElementType}
            itemSize={20}
            height={300}
            width="100%"
        >
            {({ index, style }) => <Item index={index} style={style} data={data} />}
        </FixedSizeList>
    ),
    listPropsAreEqual,
);
ChatList.displayName = 'ChatList';

const RoomChat: React.FC = () => {
    const isHubLoaded = useSelector<AppState, boolean>((state) => state.roomHub.isLoaded);
    const hub = useSelector<AppState, signalR.HubConnection>((state) => state.roomHub.hub);
    const [messages, setMessages] = useState<IChatItem[]>([]);
    const [users, setUsers] = useState<IUserItem[]>([]);
    const [message, setMessage] = useState<string>('');
    const [tabKey, setTabKey] = useState('messages');
    const [validated, setValidated] = useState(false);

    useEffect(() => {
        if (isHubLoaded) {
            hub.on('messageReceived', (characterName: string, message: string, userId: string, isPrivate: boolean) => {
                setMessages((old) => [
                    ...old,
                    {
                        nickname: characterName,
                        message: message,
                        userId: userId,
                        isPrivate: isPrivate,
                    },
                ]);
            });

            hub.on('userListRefreshed', (collection: IUserItem[]) => {
                setUsers(collection);
            });
        }

        return () => {
            if (isHubLoaded) {
                hub.off('messageReceived');
                hub.off('userListRefreshed');
            }
        };
    }, [isHubLoaded]);

    const handleSubmit = async (evt: React.FormEvent<HTMLElement>) => {
        const form = evt.currentTarget as HTMLFormElement;
        evt.preventDefault();
        evt.stopPropagation();
        if (form.checkValidity() !== false && isHubLoaded) {
            hub.send('NewMessage', message);
            setMessage('');
            setValidated(false);
            return;
        }

        setValidated(true);
    };

    // Render an item or a loading indicator.

    return (
        <Row>
            <Col className="bg-white">
                <Tabs
                    id="chatTabs"
                    variant="pills"
                    activeKey={tabKey}
                    onSelect={(k) => {
                        setTabKey(k);
                    }}
                >
                    <Tab eventKey="messages" title={<TabIcon icon={faComments} srOnly title={'Messages'} />}>
                        <Row className="mt-3">
                            <Col style={{ height: 310 }}>
                                <Fade in={messages.length > 0}>
                                    <ChatList data={messages} />
                                </Fade>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                                    <Form.Group controlId="chatMessage">
                                        <InputGroup className="mb-3">
                                            <Form.Label srOnly>Message</Form.Label>
                                            <Form.Control
                                                minLength={ValidationConstant.chatMessageMinLength}
                                                maxLength={ValidationConstant.chatMessageMaxLength}
                                                required
                                                placeholder="message..."
                                                value={message ?? ''}
                                                onChange={(e) => setMessage(e.target.value)}
                                            />
                                            <InputGroup.Append>
                                                <Button type="submit" aria-label="Send message..." variant="primary">
                                                    <FontAwesomeIcon icon={faPaperPlane} />
                                                </Button>
                                            </InputGroup.Append>
                                        </InputGroup>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                    </Tab>
                    <Tab eventKey="users" title={<TabIcon icon={faUsers} srOnly title={'Users'} />}>
                        <Row className="mt-3">
                            <Col style={{ height: 310 }}>
                                <ListGroup variant="flush" style={{ height: 300 }} className="overflow-auto">
                                    {users.map((value, index) => {
                                        const isEven = index % 2 == 0;
                                        return (
                                            <ListGroup.Item
                                                key={index}
                                                className="p-1"
                                                variant={isEven ? 'secondary' : 'primary'}
                                            >
                                                {value.nickName}
                                            </ListGroup.Item>
                                        );
                                    })}
                                </ListGroup>
                            </Col>
                        </Row>
                    </Tab>
                </Tabs>
            </Col>
        </Row>
    );
};

export default RoomChat;
