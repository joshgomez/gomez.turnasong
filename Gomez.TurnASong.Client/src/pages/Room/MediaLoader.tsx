import React, { HTMLAttributes } from 'react';

type IMediaLoaderProps = HTMLAttributes<HTMLDivElement>;

const MediaLoader: React.FC<IMediaLoaderProps> = (props: IMediaLoaderProps) => {
    return (
        <section className="loader-container">
            <div className="loader loader-2" {...props}>
                <svg xmlns="http://www.w3.org/2000/svg" className="loader-star">
                    <path d="M29.8 0.3L22.8 21.8 0 21.8 18.5 35.2 11.5 56.7 29.8 43.4 48.2 56.7 41.2 35.1 59.6 21.8 36.8 21.8z"></path>
                </svg>
                <div className="loader-circles"></div>
            </div>
            <p aria-busy="true" aria-live="polite" className="mt-1 text-shadow-white text-white">
                <strong>Loading...</strong>
            </p>
        </section>
    );
};

export default MediaLoader;
