import React, { useState } from 'react';
import { ListGroup, Popover, Media, OverlayTrigger, Badge, Button } from 'react-bootstrap';
import { faClock, faCompactDisc, faUserAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DateTimeUtility } from '../../utilities/DateTimeUtility';
import { IContinuedList } from '../../interfaces/common';
import { IPlaylistItemDto } from '../../interfaces/multimedia';
import { useSelector } from 'react-redux';
import { AppState } from '../../stores';

const GUTTER_SIZE = 5;

interface IMediaItemProps {
    style: React.CSSProperties;
    index: number;
    data: IContinuedList<IPlaylistItemDto>;
    deletable: boolean;
}

const PlaylistItem: React.FC<IMediaItemProps> = (props: IMediaItemProps) => {
    const currentPlayingVideoId = useSelector<AppState, string>((state) => state.room.data?.currentMedia?.id);
    if (props.data?.documents && props.index < props.data.documents.length) {
        const item = props.data.documents[props.index];
        const popover = (
            <Popover id={`playlist-item-popover-${item.id}`}>
                <Popover.Content>{item.title}</Popover.Content>
            </Popover>
        );

        return (
            <ListGroup.Item
                action
                className="d-inline-block text-truncate"
                as="a"
                data-media-id={item.videoId}
                href={`#${props.index}${item.title}`}
                style={{
                    ...props.style,
                    borderTopWidth: 1,
                    top: (props.style.top as number) + GUTTER_SIZE,
                    height: (props.style.height as number) - GUTTER_SIZE,
                }}
            >
                <OverlayTrigger
                    trigger={['hover', 'focus']}
                    container={document.body}
                    overlay={popover}
                    placement="top"
                >
                    <Media>
                        <img
                            style={{ height: 'auto', maxHeight: 64, width: 64 }}
                            className="mr-3"
                            src={item.thumbnail}
                            alt={item.title}
                        />
                        <Media.Body className="overflow-hidden">
                            <h6>
                                <FontAwesomeIcon icon={faCompactDisc} />
                                <span className="ml-2 text-truncate" style={{ width: 280 }}>
                                    {item.title}
                                </span>
                            </h6>
                            <p>
                                <FontAwesomeIcon icon={faClock} />
                                <span className="ml-2">{DateTimeUtility.hhmmss(item.durationAsSeconds)}</span>
                                <FontAwesomeIcon icon={faUserAlt} className="ml-2" />
                                <span className="ml-2">{item.nickname}</span>
                                {props.index == 0 && currentPlayingVideoId == item.videoId && !props.deletable && (
                                    <span className="ml-2 text-success">Playing</span>
                                )}
                                {props.deletable && (
                                    <Button
                                        variant="btn-link"
                                        className="border-0 ml-2 d-inline"
                                        size="sm"
                                        data-media-item-id={item.id}
                                    >
                                        <FontAwesomeIcon icon={faTimes} />
                                    </Button>
                                )}
                            </p>
                        </Media.Body>
                    </Media>
                </OverlayTrigger>
            </ListGroup.Item>
        );
    }

    return (
        <ListGroup.Item
            action
            as="a"
            href={`#${props.index}`}
            style={{
                ...props.style,
                borderTopWidth: 1,
                top: (props.style.top as number) + GUTTER_SIZE,
                height: (props.style.height as number) - GUTTER_SIZE,
            }}
        >
            Loading...
        </ListGroup.Item>
    );
};

export default PlaylistItem;
