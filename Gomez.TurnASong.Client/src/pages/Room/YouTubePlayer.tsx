import React, { useEffect, useRef, MutableRefObject, useState } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../../stores';
import YouTube from 'react-youtube';
import { IRoomDto, IMediaPlayerOption } from '../../interfaces/multimedia';
import { roomActions } from '../../stores/room.store';
import { MediaPlayerStateType } from '../../models/MediaPlayerStateType';
import {
    faClock,
    faVolumeUp,
    faVolumeOff,
    faVolumeDown,
    faUser,
    faPlayCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DateTimeUtility } from '../../utilities/DateTimeUtility';
import { useInterval } from '../../models/hooks/useInterval';
import { useTimeout } from '../../models/hooks/useTimeout';
import RangeSlider from 'react-bootstrap-range-slider';
import MediaLoader from './MediaLoader';
import { isMobile } from '../../utilities/PlatformUtility';

interface IYouTubePlayerProps {
    videoId: string;
}

const getSpeakerIcon = (volume: number) => {
    if (volume > 50) {
        return faVolumeUp;
    }

    if (volume > 10) {
        return faVolumeDown;
    }

    return faVolumeOff;
};

const YouTubePlayer: React.FC<IYouTubePlayerProps> = (props: IYouTubePlayerProps) => {
    const options = useSelector<AppState, IMediaPlayerOption>((state) => state.room.mediaPlayerOption);
    const room = useSelector<AppState, IRoomDto>((state) => state.room.data);
    const isHubLoaded = useSelector<AppState, boolean>((state) => state.roomHub.isLoaded);
    const hub = useSelector<AppState, signalR.HubConnection>((state) => state.roomHub.hub);
    const [player, setPlayer] = useState<YT.Player | null>(null);
    const dispatch = useDispatch();
    const [currentTime, setCurrentTime] = useState(0);
    const [playerState, setPlayerState] = useState(YT.PlayerState.UNSTARTED);
    const [mediaState, setMediaState] = useState(MediaPlayerStateType.NotReady);

    const { start: retryReadyStart, stop: retryReadyStop } = useTimeout(() => {
        if (playerState !== YT.PlayerState.PLAYING) {
            console.log('retryReady', 'Waiting time is over, force ready.');
            onReady({ target: player });
        }
    }, 35 * 1000);

    const { start: retryEndStart, stop: retryEndStop } = useTimeout(() => {
        if (playerState === YT.PlayerState.ENDED) {
            console.log('retryEnd', 'Forced to play next video.');
            onEnd({ target: player });
        }
    }, 35 * 1000);

    const onReady = (evt: { target: YT.Player }) => {
        if (evt.target === undefined) {
            console.info('UserMediaIsReady', 'Player is not ready');
            return;
        }
        setPlayer(evt.target);

        if (room.currentMedia?.durationAsSeconds === undefined) {
            return;
        }

        console.log('UserMediaIsReady', room.currentMedia?.elapsedTime, room.currentMedia?.durationAsSeconds);

        retryEndStop();
        retryReadyStart();

        evt.target.setVolume(options.volume);
        if (isHubLoaded && room.currentMedia && room.currentMedia.elapsedTime === 0) {
            hub.send('UserMediaIsReady');
            return;
        }

        if (room.currentMedia?.elapsedTime > 0) {
            evt.target.seekTo(room.currentMedia.elapsedTime, true);
            evt.target.playVideo();
        }

        if (room.currentMedia?.elapsedTime > room.currentMedia?.durationAsSeconds) {
            console.log('UserMediaIsReady->UserMediaHasEnded');
            hub.send('UserMediaHasEnded');
        }
    };

    const onEnd = (evt: { target: YT.Player }) => {
        console.log('UserMediaHasEnded', room.currentMedia?.durationAsSeconds, evt.target.getVideoUrl());
        retryReadyStop();
        retryEndStart();
        hub.send('UserMediaHasEnded');
    };

    const onStateChange = (evt: { target: YT.Player; data: YT.PlayerState }) => {
        console.log('onStateChange', evt.data);
        setPlayerState(evt.data);
        if (evt.data === YT.PlayerState.CUED) {
            onReady(evt);
        }
    };

    useEffect(() => {
        if (isHubLoaded) {
            hub.on('mediaUpdated', (room: IRoomDto, state: MediaPlayerStateType) => {
                setMediaState(state);
                if (room !== null) {
                    dispatch(roomActions.setRoom(room));
                    console.log('mediaUpdated', state);
                    if (state === MediaPlayerStateType.Ready || room.currentMedia?.startTime !== null) {
                        if (player) {
                            console.log('playing next video', room.currentMedia.title);
                            player.seekTo(room.currentMedia.elapsedTime, true);
                            player.playVideo();
                        }
                    }
                }
            });
        }

        return () => {
            if (isHubLoaded) {
                hub.off('mediaUpdated');
            }
        };
    }, [isHubLoaded]);

    useEffect(() => {
        if (!player || room.currentMedia === null) {
            return;
        }

        if (room.currentMedia.startTime === null && mediaState === MediaPlayerStateType.Ended) {
            onReady({ target: player });
            return;
        }

        if (room.currentMedia.startTime !== null && playerState === YT.PlayerState.CUED) {
            console.log('Cued->StartVideo');
            onReady({ target: player });
        }
    }, [room.currentMedia?.startTime, mediaState]);

    useInterval(
        () => {
            if (player) {
                const pTime =
                    playerState === YT.PlayerState.PLAYING
                        ? player.getCurrentTime()
                        : DateTimeUtility.getElapsedTimeInSeconds(room.currentMedia.startTime);

                const durationAsSeconds = room?.currentMedia?.durationAsSeconds ?? 0;
                if (durationAsSeconds > 0 && pTime <= durationAsSeconds) {
                    setCurrentTime(pTime);
                    if (pTime >= durationAsSeconds) {
                        onEnd({ target: player });
                    }
                }
            }
        },
        playerState === YT.PlayerState.PLAYING || !!room?.currentMedia?.startTime ? 500 : null,
    );

    useEffect(() => {
        if (player) {
            player.setVolume(options.volume);
        }
    }, [options.volume]);

    const isLoading = () => {
        if (room?.currentMedia != null && room?.currentMedia.startTime === null) {
            return true;
        }

        if (room?.currentMedia != null && playerState !== YT.PlayerState.PLAYING) {
            return true;
        }

        return false;
    };

    const canBePlayed = () => {
        if (room?.currentMedia?.startTime && playerState !== YT.PlayerState.PLAYING) {
            return true;
        }

        return false;
    };

    const playVideo = () => {
        if (canBePlayed()) {
            const elapsedTime = DateTimeUtility.getElapsedTimeInSeconds(room.currentMedia.startTime);
            console.log('playVideo', elapsedTime);
            player.seekTo(elapsedTime, true);
            player.playVideo();
        }
    };

    const showOverlay = () => {
        if (canBePlayed() || isLoading() || playerState !== YT.PlayerState.PLAYING || room?.currentMedia === null) {
            return true;
        }

        return false;
    };

    return (
        <>
            <Row>
                <Col style={{ height: 340 }} className="bg-dark shadow-sm text-center">
                    <div className={`player-overlay ${showOverlay() ? 'bg-dark' : ''}`}>
                        {canBePlayed() ? (
                            <Row className="align-items-center h-100">
                                <Col>
                                    <Button variant="link" onClick={playVideo} className="my-auto">
                                        <FontAwesomeIcon size="5x" icon={faPlayCircle} />
                                    </Button>
                                    <p className="text-shadow-white text-white">
                                        <strong>A video is being played, press play to watch</strong>
                                    </p>
                                </Col>
                            </Row>
                        ) : (
                            isLoading() && <MediaLoader />
                        )}
                    </div>
                    <YouTube
                        videoId={props.videoId}
                        opts={{
                            height: '340',
                            width: '100%',
                            playerVars: {
                                autoplay: 0,
                                controls: 0,
                                disablekb: 1,
                                modestbranding: 1,
                            },
                        }}
                        onReady={onReady}
                        onEnd={onEnd}
                        onStateChange={onStateChange}
                    />
                </Col>
            </Row>
            <Row>
                <Col className="text-left">
                    <Row>
                        <Col xs="2">
                            <p className="mb-0" id="mediaTimeRemaning">
                                <FontAwesomeIcon icon={faClock} />
                                <span className="sr-only">Time remaning of the video.</span>
                            </p>
                            <p id="mediaPlayedBy">
                                <FontAwesomeIcon icon={faUser} />
                                <span className="sr-only">The user who is playing the video.</span>
                            </p>
                        </Col>
                        <Col xs="10">
                            <p className="mb-0" aria-describedby="mediaTimeRemaning">
                                {DateTimeUtility.hhmmss(currentTime)} /
                                {DateTimeUtility.hhmmss(room?.currentMedia?.durationAsSeconds)}
                            </p>
                            <p aria-describedby="mediaPlayedBy">
                                {room?.currentMedia?.user?.name ? (
                                    <span>
                                        Played by <strong>{room?.currentMedia?.user?.name}</strong>
                                    </span>
                                ) : (
                                    <span>No one is playing</span>
                                )}
                            </p>
                        </Col>
                    </Row>
                </Col>
                <Col className="text-right">
                    <Form>
                        {!isMobile() && (
                            <Form.Group as={Row} controlId="volumeRange">
                                <Form.Label column xs="12" srOnly>
                                    Volume
                                </Form.Label>
                                <Col xs="2" className="align-items-center" style={{ paddingBottom: 4, paddingTop: 4 }}>
                                    <FontAwesomeIcon icon={getSpeakerIcon(options.volume)} className="mr-2" />
                                </Col>
                                <Col xs="10">
                                    <RangeSlider
                                        inputProps={{ id: 'volumeRange' }}
                                        value={options.volume}
                                        onChange={(e) => {
                                            dispatch(
                                                roomActions.setMediaPlayerOptions({
                                                    ...options,
                                                    volume: parseInt(e.target.value, 10),
                                                }),
                                            );
                                        }}
                                        min={0}
                                        step={1}
                                        max={100}
                                        size="sm"
                                    />
                                </Col>
                            </Form.Group>
                        )}
                    </Form>
                </Col>
            </Row>
        </>
    );
};

export default YouTubePlayer;
