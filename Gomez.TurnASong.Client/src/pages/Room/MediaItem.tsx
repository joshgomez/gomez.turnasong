import React from 'react';
import { ListGroup, Popover, Media, OverlayTrigger } from 'react-bootstrap';
import { faClock, faCompactDisc } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DateTimeUtility } from '../../utilities/DateTimeUtility';
import { IMediaResponse } from '../../interfaces/multimedia';

const GUTTER_SIZE = 5;

interface IMediaItemProps {
    style: React.CSSProperties;
    index: number;
    data: IMediaResponse;
}

const MediaItem: React.FC<IMediaItemProps> = (props: IMediaItemProps) => {
    if (props.index < props.data.media.length) {
        const media = props.data.media[props.index];
        const popover = (
            <Popover id={`media-item-popover-${media.id}`}>
                <Popover.Title as="h3">{media.title}</Popover.Title>
                <Popover.Content>{media.description}</Popover.Content>
            </Popover>
        );

        return (
            <ListGroup.Item
                action
                className="d-inline-block text-truncate"
                as="a"
                data-media-id={media.id}
                href={`#${props.index}${media.title}`}
                style={{
                    ...props.style,
                    borderTopWidth: 1,
                    top: (props.style.top as number) + GUTTER_SIZE,
                    height: (props.style.height as number) - GUTTER_SIZE,
                }}
            >
                <OverlayTrigger
                    trigger={['hover', 'focus']}
                    container={document.body}
                    overlay={popover}
                    placement="top"
                >
                    <Media>
                        <img
                            style={{ height: 'auto', maxHeight: 64, width: 64 }}
                            className="mr-3"
                            src={media.thumbnail}
                            alt={media.title}
                        />
                        <Media.Body>
                            <h6>
                                <FontAwesomeIcon icon={faCompactDisc} />
                                <span className="ml-2">{media.title}</span>
                            </h6>
                            <p>
                                <FontAwesomeIcon icon={faClock} />
                                <span className="ml-2">{DateTimeUtility.hhmmss(media.durationInSeconds)}</span>
                            </p>
                        </Media.Body>
                    </Media>
                </OverlayTrigger>
            </ListGroup.Item>
        );
    }

    return (
        <ListGroup.Item
            action
            as="a"
            href={`#${props.index}`}
            style={{
                ...props.style,
                borderTopWidth: 1,
                top: (props.style.top as number) + GUTTER_SIZE,
                height: (props.style.height as number) - GUTTER_SIZE,
            }}
        >
            Loading...
        </ListGroup.Item>
    );
};

export default MediaItem;
