import React, { useEffect, useState, forwardRef, HtmlHTMLAttributes } from 'react';
import { Row, Col, ListGroup, ListGroupProps } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { AppState } from '../../stores';
import { MediaApiService } from '../../services/api/MediaApiService';
import { FixedSizeList } from 'react-window';
import { IReactWindowItem } from '../../interfaces/common';
import WaitListItem from './WaitListItem';
import { IUserIdentity } from '../../interfaces/identity';
import { CSSTransition } from 'react-transition-group';

const innerElementType = forwardRef<HTMLDivElement, HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps>(
    ({ style, ...rest }: HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps, ref) => (
        <ListGroup
            ref={ref}
            style={{
                ...style,
            }}
            {...rest}
        />
    ),
);
innerElementType.displayName = 'WaitingListListGroup';

const GUTTER_SIZE = 5;

const WaitingList: React.FC = () => {
    const mediaApiService = useSelector<AppState, MediaApiService>((state) => state.services.mediaApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const isRoomLoading = useSelector<AppState, boolean>((state) => state.room.isLoading);
    const isRoomHubLoaded = useSelector<AppState, boolean>((state) => state.roomHub.isLoaded);
    const [data, setData] = useState<IUserIdentity[]>([]);
    const [isLoading, setIsLoading] = useState(true);

    const setDataAsync = async () => {
        setIsLoading(true);
        setData(await mediaApiService.getWaitingUsersAsync());
        setIsLoading(false);
    };

    useEffect(() => {
        if (!isRoomLoading && isServicesLoaded && isRoomHubLoaded) {
            setDataAsync();
        }
    }, [isRoomLoading, isServicesLoaded, isRoomHubLoaded]);

    // Render an item or a loading indicator.
    const Item = ({ index, style }: IReactWindowItem<IUserIdentity[]>) => (
        <WaitListItem index={index} style={style} data={data} />
    );

    return (
        <>
            <Row className="mt-3">
                <Col>
                    <CSSTransition in={!isLoading} timeout={200} classNames="fade-in">
                        <FixedSizeList
                            itemCount={data.length}
                            innerElementType={innerElementType}
                            itemSize={40 + GUTTER_SIZE}
                            height={400}
                            width="100%"
                        >
                            {Item}
                        </FixedSizeList>
                    </CSSTransition>
                </Col>
            </Row>
        </>
    );
};

export default WaitingList;
