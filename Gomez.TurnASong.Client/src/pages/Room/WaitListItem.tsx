import React from 'react';
import { ListGroup, Media } from 'react-bootstrap';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IUserIdentity } from '../../interfaces/identity';

const GUTTER_SIZE = 5;

interface IWaitListItemProps {
    style: React.CSSProperties;
    index: number;
    data: IUserIdentity[];
}

const WaitListItem: React.FC<IWaitListItemProps> = (props: IWaitListItemProps) => {
    if (props.index < props.data.length) {
        const item = props.data[props.index];
        return (
            <ListGroup.Item
                action
                className="d-inline-block text-truncate"
                as="a"
                data-user-id={item.id}
                href={`#${item.name}`}
                style={{
                    ...props.style,
                    borderTopWidth: 1,
                    top: (props.style.top as number) + GUTTER_SIZE,
                    height: (props.style.height as number) - GUTTER_SIZE,
                }}
            >
                <Media>
                    <Media.Body>
                        <h6>
                            <FontAwesomeIcon icon={faUser} />
                            <span className="ml-2">{item.name}</span>
                        </h6>
                    </Media.Body>
                </Media>
            </ListGroup.Item>
        );
    }

    return (
        <ListGroup.Item
            action
            as="a"
            href={`#${props.index}`}
            style={{
                ...props.style,
                borderTopWidth: 1,
                top: (props.style.top as number) + GUTTER_SIZE,
                height: (props.style.height as number) - GUTTER_SIZE,
            }}
        >
            Loading...
        </ListGroup.Item>
    );
};

export default WaitListItem;
