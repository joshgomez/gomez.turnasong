import React, { useState, forwardRef, HtmlHTMLAttributes } from 'react';
import { Row, Col, InputGroup, Button, Form, ListGroup, ListGroupProps } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../../stores';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { IMediaResponse } from '../../interfaces/multimedia';
import { MediaApiService } from '../../services/api/MediaApiService';
import { FixedSizeList } from 'react-window';
import { EventUtility } from '../../utilities/EventUtility';
import { RoomHubState } from '../../stores/roomhub.store';
import { setIsMenuOpen } from '../../stores/layout.store';
import { useDebouncedCallback } from 'use-debounce';
import { DurationType } from '../../models/DurationType';
import MediaItem from './MediaItem';
import { CSSTransition } from 'react-transition-group';
import MediaService from '../../services/MediaService';
import { IReactWindowItem } from '../../interfaces/common';

const innerElementType = forwardRef<HTMLDivElement, HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps>(
    ({ style, ...rest }: HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps, ref) => (
        <ListGroup
            ref={ref}
            style={{
                ...style,
            }}
            {...rest}
        />
    ),
);

innerElementType.displayName = 'MediaSearchListGroup';
const GUTTER_SIZE = 5;

const Item: React.FC<IReactWindowItem<IMediaResponse>> = ({ index, style, data }: IReactWindowItem<IMediaResponse>) => (
    <MediaItem index={index} style={style} data={data} />
);
Item.displayName = 'MediaItem';

const listPropsAreEqual = (prevProps: IListProps, nextProps: IListProps) => {
    // Check if the arrays are the same length
    if (prevProps.data.media.length !== nextProps.data.media.length) return false;

    // Check if all items exist and are in the same order
    for (let i = 0; i < prevProps.data.media.length; i++) {
        if (prevProps.data.media[i].id !== nextProps.data.media[i].id) return false;
    }

    return true;
};

interface IListProps {
    data: IMediaResponse;
}

const List = React.memo(
    ({ data }: IListProps) => (
        <FixedSizeList
            itemCount={data.media.length}
            innerElementType={innerElementType}
            itemSize={65 + GUTTER_SIZE}
            height={400}
            width="100%"
        >
            {({ index, style }) => <Item index={index} style={style} data={data} />}
        </FixedSizeList>
    ),
    listPropsAreEqual,
);
List.displayName = 'MediaList';

const MediaSearch: React.FC = () => {
    const mediaApiService = useSelector<AppState, MediaApiService>((state) => state.services.mediaApiService);
    const videoDuration = useSelector<AppState, DurationType>((state) => state.room.data.videoDuration);
    const [data, setData] = useState<IMediaResponse>({ media: [] });
    const [isLoading, setIsLoading] = useState(false);
    const hubState = useSelector<AppState, RoomHubState>((state) => state.roomHub);
    const dispatch = useDispatch();

    const search = async () => {
        setIsLoading(true);
        setData({ ...data, media: [] });
        console.log(data.search);
        const result = await mediaApiService.getAllAsync(data.search, videoDuration);
        setData(result);
        setIsLoading(false);
    };

    const [debouncedSearch] = useDebouncedCallback(search, 1000);

    const handleSubmit = async (evt: React.FormEvent<HTMLElement>) => {
        evt.preventDefault();
        await search();
    };

    const handleClick = async (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        const target = EventUtility.getTargetMouseEvent(evt, 'A') as HTMLAnchorElement;
        addMedia(target);
    };

    const handleKeyPress = async (evt: React.KeyboardEvent<HTMLDivElement>) => {
        const target = EventUtility.getTargetKeyboardEvent(evt, 'A', ['Enter']) as HTMLAnchorElement | null;
        addMedia(target);
    };

    const handleChange = async (evt: React.ChangeEvent<HTMLInputElement>) => {
        setData({ ...data, search: evt.target.value });
    };

    const handleSearch = async () => {
        await debouncedSearch();
    };

    const addMedia = async (target: HTMLAnchorElement | null) => {
        if (await MediaService.AddMediaAsync(target, hubState.hub, dispatch)) {
            dispatch(setIsMenuOpen('search', false));
        }
    };

    return (
        <>
            <Row className="mt-3">
                <Col>
                    <Form onSubmit={handleSubmit}>
                        <InputGroup className="mb-3">
                            <Form.Control
                                placeholder="Search..."
                                aria-label="Search rooms..."
                                aria-describedby="rooms-view-information"
                                defaultValue={data?.search ?? ''}
                                onChange={handleChange}
                                onKeyUp={handleSearch}
                            />
                            <InputGroup.Append>
                                <Button type="submit" className="d-none" aria-label="Search rooms..." variant="primary">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col onClick={handleClick} onKeyPress={handleKeyPress}>
                    <CSSTransition in={!isLoading} timeout={200} classNames="fade-in">
                        <List data={data} />
                    </CSSTransition>
                </Col>
            </Row>
        </>
    );
};

export default MediaSearch;
