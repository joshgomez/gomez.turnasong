import React from 'react';
import { Popover, OverlayTrigger } from 'react-bootstrap';
import { IChatItem } from '../../interfaces/chat';

interface IChatItemProps {
    style: React.CSSProperties;
    index: number;
    messages: IChatItem[];
}

const ChatItem: React.FC<IChatItemProps> = (props: IChatItemProps) => {
    if (props.index < props.messages.length) {
        const item = props.messages[props.index];
        const popover = (
            <Popover id={`chat-m-popover-${props.index}`}>
                <Popover.Title as="h3">{item.nickname}</Popover.Title>
                <Popover.Content>{item.message}</Popover.Content>
            </Popover>
        );

        return (
            <li style={props.style}>
                <OverlayTrigger
                    trigger={['hover', 'focus']}
                    container={document.body}
                    overlay={popover}
                    placement="top"
                >
                    <p className={`chat-item text-truncate small bg-light`}>
                        <strong className="chat-item-user">{item.nickname}</strong>:
                        <span className="chat-item-message" dangerouslySetInnerHTML={{ __html: item.message }}></span>
                    </p>
                </OverlayTrigger>
            </li>
        );
    }

    return (
        <li className={`chat-item text-truncate small bg-light`} style={props.style}>
            Loading...
        </li>
    );
};

export default ChatItem;
