import React, { useEffect, useState, forwardRef, HtmlHTMLAttributes } from 'react';
import { Row, Col, ListGroup, ListGroupProps, Tab, Tabs } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../../stores';
import { faUserClock, faHistory, faForward } from '@fortawesome/free-solid-svg-icons';
import { IPlaylistItemDto } from '../../interfaces/multimedia';
import { MediaApiService } from '../../services/api/MediaApiService';
import { FixedSizeList } from 'react-window';
import { IContinuedList, IReactWindowItem } from '../../interfaces/common';
import TabIcon from '../common/TabIcon';
import PlaylistItem from './PlaylistItem';
import { EventUtility } from '../../utilities/EventUtility';
import { HubConnection } from '@microsoft/signalr';
import { setIsMenuOpen, stackToast } from '../../stores/layout.store';
import MediaService from '../../services/MediaService';
import { CSSTransition } from 'react-transition-group';

const innerElementType = forwardRef<HTMLDivElement, HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps>(
    ({ style, ...rest }: HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps, ref) => (
        <ListGroup
            ref={ref}
            style={{
                ...style,
            }}
            {...rest}
        />
    ),
);
innerElementType.displayName = 'PlaylistListGroup';

const GUTTER_SIZE = 5;
const TAB_KEY_HISTORY = 'history';
const TAB_KEY_MYHISTORY = 'my-history';
const TAB_KEY_MYQUEUE = 'my-queue';

const Playlist: React.FC = () => {
    const mediaApiService = useSelector<AppState, MediaApiService>((state) => state.services.mediaApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const isRoomLoading = useSelector<AppState, boolean>((state) => state.room.isLoading);
    const isRoomHubLoaded = useSelector<AppState, boolean>((state) => state.roomHub.isLoaded);
    const hub = useSelector<AppState, HubConnection>((state) => state.roomHub.hub);
    const [data, setData] = useState<IContinuedList<IPlaylistItemDto>>({ documents: [] });
    const [isLoading, setIsLoading] = useState(true);
    const [tabKey, setTabKey] = useState(TAB_KEY_HISTORY);
    const dispatch = useDispatch();

    const handleClick = async (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        const target = EventUtility.getTargetMouseEvent(evt, 'A') as HTMLAnchorElement;
        const btntarget = EventUtility.getTargetMouseEvent(evt, 'Button') as HTMLButtonElement;
        if (btntarget) {
            evt.stopPropagation();
            evt.preventDefault();
            const itemId = btntarget.getAttribute('data-media-item-id');
            if (itemId) {
                const media = data.documents.find((media) => media.id == itemId);
                if (media !== null) {
                    await mediaApiService.deleteQueueItemAsync(itemId);
                    dispatch(
                        stackToast({
                            title: 'Deleted media',
                            autoHide: true,
                            delay: 6000,
                            message: media.title,
                            show: true,
                        }),
                    );

                    hub.send('NewMessage', 'Have deleted a video from the queue.');
                    await setDataAsync();
                }
            }
            return;
        }

        addMedia(target);
    };

    const handleKeyPress = async (evt: React.KeyboardEvent<HTMLDivElement>) => {
        const target = EventUtility.getTargetKeyboardEvent(evt, 'A', ['Enter']) as HTMLAnchorElement | null;
        addMedia(target);
    };

    const setHistoryAsync = async () => {
        setData(await mediaApiService.getHistoryAsync());
    };

    const setMyHistoryAsync = async () => {
        setData(await mediaApiService.getMyHistoryAsync());
    };

    const setMyQueueAsync = async () => {
        setData(await mediaApiService.getMyQueueAsync());
    };

    const addMedia = async (target: HTMLAnchorElement | null) => {
        if (await MediaService.AddMediaAsync(target, hub, dispatch)) {
            dispatch(setIsMenuOpen('playlist', false));
        }
    };

    const setDataAsync = async () => {
        setIsLoading(true);
        switch (tabKey) {
            case TAB_KEY_HISTORY:
                await setHistoryAsync();
                break;
            case TAB_KEY_MYHISTORY:
                await setMyHistoryAsync();
                break;
            case TAB_KEY_MYQUEUE:
                await setMyQueueAsync();
                break;
        }
        setIsLoading(false);
    };

    useEffect(() => {
        if (!isRoomLoading && isServicesLoaded && isRoomHubLoaded) {
            setDataAsync();
        }
    }, [isRoomLoading, isServicesLoaded, isRoomHubLoaded]);

    useEffect(() => {
        if (!isRoomLoading && isServicesLoaded && !isLoading && isRoomHubLoaded) {
            setDataAsync();
        }
    }, [tabKey]);

    // Render an item or a loading indicator.
    const Item = ({ index, style }: IReactWindowItem<IContinuedList<IPlaylistItemDto>>) => (
        <PlaylistItem index={index} style={style} deletable={tabKey === TAB_KEY_MYQUEUE} data={data} />
    );

    return (
        <>
            <Row className="mt-3">
                <Col>
                    <Tabs
                        id="paylistTabs"
                        activeKey={tabKey}
                        onSelect={(k) => {
                            if (isLoading) {
                                return;
                            }

                            setTabKey(k);
                        }}
                    >
                        <Tab
                            eventKey={TAB_KEY_HISTORY}
                            title={<TabIcon icon={faHistory} srOnly title={'Playlist history'} />}
                        ></Tab>
                        <Tab
                            eventKey={TAB_KEY_MYHISTORY}
                            title={<TabIcon icon={faUserClock} srOnly title={'Played by you'} />}
                        ></Tab>
                        <Tab
                            eventKey={TAB_KEY_MYQUEUE}
                            title={<TabIcon icon={faForward} srOnly title={'Played next by you'} />}
                        ></Tab>
                    </Tabs>
                </Col>
            </Row>
            <Row>
                <Col onClick={handleClick} onKeyPress={handleKeyPress}>
                    <CSSTransition in={!isLoading} timeout={200} classNames="fade-in">
                        <FixedSizeList
                            itemCount={data?.documents?.length ?? 0}
                            innerElementType={innerElementType}
                            itemSize={65 + GUTTER_SIZE}
                            height={400}
                            width="100%"
                        >
                            {Item}
                        </FixedSizeList>
                    </CSSTransition>
                </Col>
            </Row>
        </>
    );
};

export default Playlist;
