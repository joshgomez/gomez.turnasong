import React, { useState, useEffect } from 'react';
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faTimes } from '@fortawesome/free-solid-svg-icons';
import { IProfileDto } from '../interfaces/identity';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../stores';
import { ProfileApiService } from '../services/api/ProfileApiService';
import { useHistory } from 'react-router-dom';
import CardHeader from './common/CardHeader';
import { setTitle } from '../stores/layout.store';
import { useMenu } from '../models/hooks/useMenu';
import PageLoader from './common/PageLoader';
import { ValidationConstant } from '../constants/ValidationConstant';
import withAppInsTracking from '../models/AppInsights';
import { useAppInsights } from '../models/AppInsightsContext';
import { formInvalidSubmitted } from '../models/AppInsightsEvents';

const ProfileSettings: React.FC = () => {
    const [viewModel, setViewModel] = useState<IProfileDto | null>(null);
    const profileApiService = useSelector<AppState, ProfileApiService>((state) => state.services.profileApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const userId = useSelector<AppState, string>((state) => state.auth.userId);
    const history = useHistory();
    const [validated, setValidated] = useState(false);
    const ai = useAppInsights();

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(setTitle('Profile settings'));
    }, []);

    useMenu(['add'], [false]);

    useEffect(() => {
        const beginAsync = async () => {
            const result = await profileApiService.getAsync(userId);
            setViewModel(result);
        };

        if (isServicesLoaded) {
            beginAsync();
        }
    }, [isServicesLoaded]);

    const handleSubmit = async (evt: React.FormEvent<HTMLElement>) => {
        const form = evt.currentTarget as HTMLFormElement;
        evt.preventDefault();
        evt.stopPropagation();

        if (!isServicesLoaded || viewModel === undefined || form.checkValidity() === false) {
            setValidated(true);
            ai.trackEvent(formInvalidSubmitted, { name: 'ProfileSettings' });
            return;
        }

        setValidated(false);
        await profileApiService.patchAsync({ nickname: viewModel.nickname });
        history.push('/profile');
    };

    const handleCancel = async (evt: React.MouseEvent<HTMLButtonElement>) => {
        evt.preventDefault();
        history.push('/profile');
    };

    if (viewModel == null) {
        return <PageLoader />;
    }

    return (
        <>
            <Row>
                <Col xs={12} md={3}></Col>
                <Col xs={12} md={6}>
                    <Card border="primary">
                        <CardHeader>{viewModel?.userName}</CardHeader>
                        <Card.Body>
                            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                                <Form.Group controlId="nickname">
                                    <Form.Label>Nickname</Form.Label>
                                    <Form.Control
                                        placeholder="Nickname..."
                                        aria-label="Type nickname..."
                                        required
                                        minLength={ValidationConstant.profileMinLength}
                                        maxLength={ValidationConstant.profileMaxLength}
                                        value={viewModel?.nickname ?? ''}
                                        onChange={(e) => setViewModel({ ...viewModel, nickname: e.target.value })}
                                    />
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col md={6}>
                                            <Button
                                                type="button"
                                                onClick={handleCancel}
                                                block
                                                title="Cancel..."
                                                className="mb-3"
                                                variant="secondary"
                                            >
                                                <FontAwesomeIcon icon={faTimes} />
                                            </Button>
                                        </Col>
                                        <Col md={6}>
                                            <Button type="submit" block title="Save profile..." variant="primary">
                                                <FontAwesomeIcon icon={faSave} />
                                            </Button>
                                        </Col>
                                    </Row>
                                </Form.Group>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={3}></Col>
            </Row>
        </>
    );
};

export default withAppInsTracking(ProfileSettings);
