import React from 'react';
import { ListGroup } from 'react-bootstrap';
import { faDoorOpen, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRoomsResponse } from '../../interfaces/multimedia';

const GUTTER_SIZE = 5;

interface IRoomItemProps {
    style: React.CSSProperties;
    index: number;
    data: IRoomsResponse;
}

const RoomItem: React.FC<IRoomItemProps> = (props: IRoomItemProps) => {
    if (props.index < props.data.rooms.documents.length) {
        const room = props.data.rooms.documents[props.index];
        return (
            <ListGroup.Item
                action
                as="a"
                data-room-id={room.id}
                href={`#${props.index}${room.name}`}
                style={{
                    ...props.style,
                    borderTopWidth: 1,
                    top: (props.style.top as number) + GUTTER_SIZE,
                    height: (props.style.height as number) - GUTTER_SIZE,
                }}
            >
                <FontAwesomeIcon icon={faDoorOpen} />
                <span className="ml-2">{room.name}</span>
                {room.thumbnail && (
                    <img
                        src={room.thumbnail}
                        style={{ maxHeight: 48 }}
                        className="float-right"
                        alt={`thumbnail of ${room.name}`}
                    />
                )}
                <p>
                    <FontAwesomeIcon icon={faUsers} /> {room.onlineCount}
                    <span className="sr-only">Online users</span>
                </p>
            </ListGroup.Item>
        );
    }

    return (
        <ListGroup.Item
            action
            as="a"
            href={`#${props.index}`}
            style={{
                ...props.style,
                borderTopWidth: 1,
                top: (props.style.top as number) + GUTTER_SIZE,
                height: (props.style.height as number) - GUTTER_SIZE,
            }}
        >
            Loading...
        </ListGroup.Item>
    );
};

export default RoomItem;
