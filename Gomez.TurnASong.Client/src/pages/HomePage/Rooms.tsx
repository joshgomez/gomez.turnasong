import React, { useEffect, useState, forwardRef, HtmlHTMLAttributes } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../../stores';
import { RoomApiService } from '../../services/api/RoomApiService';
import { IRoomsResponse } from '../../interfaces/multimedia';
import InfiniteLoader from 'react-window-infinite-loader';
import { FixedSizeList, ListOnItemsRenderedProps } from 'react-window';
import { IReactWindowItem } from '../../interfaces/common';
import { ListGroup, ListGroupProps, Row, Col, InputGroup, Form, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { EventUtility } from '../../utilities/EventUtility';
import { useHistory } from 'react-router-dom';
import { useDebouncedCallback } from 'use-debounce/lib';
import RoomItem from './RoomItem';
import { CSSTransition } from 'react-transition-group';

const innerElementType = forwardRef<HTMLDivElement, HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps>(
    ({ style, ...rest }: HtmlHTMLAttributes<HTMLDivElement> & ListGroupProps, ref) => (
        <ListGroup
            ref={ref}
            style={{
                ...style,
            }}
            {...rest}
        />
    ),
);
innerElementType.displayName = 'RoomsListGroup';
const GUTTER_SIZE = 5;

const Item: React.FC<IReactWindowItem<IRoomsResponse>> = ({ index, style, data }: IReactWindowItem<IRoomsResponse>) => (
    <RoomItem index={index} style={style} data={data} />
);
Item.displayName = 'RoomItem';

interface IListProps {
    data: IRoomsResponse;
    itemCount: number;
    onItemsRendered: (props: ListOnItemsRenderedProps) => any;
}

const listPropsAreEqual = (prevProps: IListProps, nextProps: IListProps) => {
    // Check if the arrays are the same length
    if (prevProps.data.rooms.documents.length !== nextProps.data.rooms.documents.length) return false;

    // Check if all items exist and are in the same order
    for (let i = 0; i < prevProps.data.rooms.documents.length; i++) {
        if (prevProps.data.rooms.documents[i].id !== nextProps.data.rooms.documents[i].id) return false;
    }

    return true;
};
// { data, itemCount, onItemsRendered, forwardedRef }: IListProps)
const List = React.memo(
    React.forwardRef<FixedSizeList, IListProps>(({ data, itemCount, onItemsRendered }: IListProps, ref) => (
        <FixedSizeList
            itemCount={itemCount}
            onItemsRendered={onItemsRendered}
            innerElementType={innerElementType}
            ref={ref}
            itemSize={45 + 28 + GUTTER_SIZE}
            height={400}
            width="100%"
        >
            {({ index, style }) => <Item index={index} style={style} data={data} />}
        </FixedSizeList>
    )),
    listPropsAreEqual,
);
List.displayName = 'RoomList';

const Rooms: React.FC = () => {
    const roomApiService = useSelector<AppState, RoomApiService>((state) => state.services.roomApiService);
    const isServicesLoaded = useSelector<AppState, boolean>((state) => state.services.isLoaded);
    const [isLoaded, setIsLoaded] = useState(false);
    const [data, setData] = useState<IRoomsResponse>({ continuationToken: null, search: '', rooms: { documents: [] } });
    const history = useHistory();

    useEffect(() => {
        const beginAsync = async () => {
            const result = await roomApiService.getAllAsync();
            setData(result);
            setIsLoaded(true);
        };

        if (isServicesLoaded) {
            beginAsync();
        }
    }, [isServicesLoaded]);

    const loadNextPage = async () => {
        if (data.rooms.continuationToken === null) {
            return;
        }

        setIsLoaded(false);
        const result = await roomApiService.getAllAsync(data.search, data.continuationToken);
        const documents = [...data.rooms.documents, ...result.rooms.documents];
        result.rooms.documents = documents;
        setData(result);
        setIsLoaded(true);
    };

    const search = async () => {
        setIsLoaded(false);
        setData({ ...data, rooms: { ...data.rooms, documents: [] } });
        console.log(data.search);
        const result = await roomApiService.getAllAsync(data.search);
        setData(result);
        setIsLoaded(true);
    };

    const [debouncedSearch] = useDebouncedCallback(search, 1000);

    const handleSearch = async () => {
        debouncedSearch();
    };

    const handleSubmit = async (evt: React.FormEvent<HTMLElement>) => {
        evt.preventDefault();
        await search();
    };

    const handleClick = async (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        const target = EventUtility.getTargetMouseEvent(evt, 'A') as HTMLAnchorElement;
        const roomId = target.getAttribute('data-room-id');
        history.push(`/rooms/${roomId}`);
    };

    const handleKeyPress = async (evt: React.KeyboardEvent<HTMLDivElement>) => {
        const target = EventUtility.getTargetKeyboardEvent(evt, 'A', ['Enter']) as HTMLAnchorElement | null;
        const roomId = target.getAttribute('data-room-id');
        history.push(`/rooms/${roomId}`);
    };

    // If there are more items to be loaded then add an extra row to hold a loading indicator.
    const itemCount = () => {
        if (data.rooms.documents.length == 0) {
            return 0;
        }
        return hasNextPage() ? data.rooms.documents.length + 1 : data.rooms.documents.length;
    };

    const hasNextPage = () => !!data.rooms.continuationToken;

    // Only load 1 page of items at a time.
    // Pass an empty callback to InfiniteLoader in case it asks us to load more than once.
    const loadMoreItems = !isLoaded
        ? async () => {
              // Empty
          }
        : loadNextPage;

    // Every row is loaded except for our loading indicator row.
    const isItemLoaded = (index: number) => !hasNextPage() || index < data.rooms.documents.length;

    return (
        <>
            <Row>
                <Col>
                    <h2 className="sr-only">Rooms</h2>
                    <p id="rooms-view-information" className="sr-only">
                        List of rooms you can join to listen or watch music and videos together. Search by room names.
                    </p>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form onSubmit={handleSubmit}>
                        <InputGroup className="mb-3">
                            <Form.Control
                                placeholder="Search..."
                                aria-label="Search rooms..."
                                aria-describedby="rooms-view-information"
                                defaultValue={data?.search ?? ''}
                                onChange={(e) => setData({ ...data, search: e.target.value })}
                                onKeyUp={handleSearch}
                            />
                            <InputGroup.Append>
                                <Button type="submit" aria-label="Search rooms..." variant="primary">
                                    <FontAwesomeIcon icon={faSearch} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col onClick={handleClick} onKeyPress={handleKeyPress} style={{ minHeight: 400 }}>
                    <CSSTransition in={isLoaded} timeout={200} classNames="fade-in">
                        <InfiniteLoader
                            isItemLoaded={isItemLoaded}
                            itemCount={itemCount()}
                            loadMoreItems={loadMoreItems}
                        >
                            {({ onItemsRendered, ref }) => (
                                <List onItemsRendered={onItemsRendered} ref={ref} itemCount={itemCount()} data={data} />
                            )}
                        </InfiniteLoader>
                    </CSSTransition>
                </Col>
            </Row>
        </>
    );
};

export default Rooms;
