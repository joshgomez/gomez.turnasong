import React from 'react';
import { Navbar } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../stores';
import { EventUtility } from '../utilities/EventUtility';
import { setIsMenuOpen } from '../stores/layout.store';
import { Link } from 'react-router-dom';
import { useAppInsights } from '../models/AppInsightsContext';
import { brandLogoClicked } from '../models/AppInsightsEvents';

const HeaderNavbar: React.FC = () => {
    const showNavbar = useSelector<AppState, boolean>((state) => state.layout.showNavbar);
    const isMenuOpen = useSelector<AppState, Record<string, boolean>>((state) => state.layout.isMenuOpen);
    const title = useSelector<AppState, string>((state) => state.layout.title);
    const ai = useAppInsights();
    const dispatch = useDispatch();
    if (!showNavbar) {
        return null;
    }

    const handleClick = async (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        const target = EventUtility.getTargetMouseEvent(evt, 'A') as HTMLAnchorElement;
        toggleMenu(target);
    };

    const handleKeyPress = async (evt: React.KeyboardEvent<HTMLDivElement>) => {
        const target = EventUtility.getTargetKeyboardEvent(evt, 'A', ['Enter']) as HTMLAnchorElement | null;
        toggleMenu(target);
    };

    const toggleMenu = async (target: HTMLAnchorElement | null) => {
        if (target === null) {
            return;
        }

        const menu = target.getAttribute('data-menu');
        if (!menu) {
            return;
        }

        if (menu in isMenuOpen) {
            dispatch(setIsMenuOpen(menu, !isMenuOpen[menu]));
        }
    };

    return (
        <Navbar bg="primary" fixed="top" variant="dark" onClick={handleClick} onKeyPress={handleKeyPress}>
            <Navbar.Brand as={Link} to="/" onClick={() => ai.trackEvent(brandLogoClicked)}>
                <img
                    src="/mstile-144x144.png"
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt="Turn A Song"
                />
            </Navbar.Brand>
            <header className="text-white navbar-header-title w-100">
                <h1 className={`mb-0${title ? '' : ' sr-only'}`}>{title ? title : 'Home'}</h1>
            </header>
        </Navbar>
    );
};

export default HeaderNavbar;
