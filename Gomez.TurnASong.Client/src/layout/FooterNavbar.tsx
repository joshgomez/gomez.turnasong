import React from 'react';
import { Row, Container, Col, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faUserCircle, faList, faSearch, faPlusSquare, faUserClock } from '@fortawesome/free-solid-svg-icons';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../stores';
import { EventUtility } from '../utilities/EventUtility';
import { setIsMenuOpen } from '../stores/layout.store';
import IconLink from './common/IconLink';
import IconNavLink from './common/IconNavLink';

const FooterNavbar: React.FC = (props) => {
    const showNavbar = useSelector<AppState, boolean>((state) => state.layout.showNavbar);
    const isMenuOpen = useSelector<AppState, Record<string, boolean>>((state) => state.layout.isMenuOpen);
    const dispatch = useDispatch();
    if (!showNavbar) {
        return null;
    }

    const handleClick = async (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        const target = EventUtility.getTargetMouseEvent(evt, 'A') as HTMLAnchorElement;
        toggleMenu(target);
    };

    const handleKeyPress = async (evt: React.KeyboardEvent<HTMLDivElement>) => {
        const target = EventUtility.getTargetKeyboardEvent(evt, 'A', ['Enter']) as HTMLAnchorElement | null;
        toggleMenu(target);
    };

    const toggleMenu = async (target: HTMLAnchorElement | null) => {
        if (target === null) {
            return;
        }

        const menu = target.getAttribute('data-menu');
        if (!menu) {
            return;
        }

        if (menu in isMenuOpen) {
            dispatch(setIsMenuOpen(menu, !isMenuOpen[menu]));
        }
    };

    return (
        <Navbar bg="primary" fixed="bottom" variant="dark" onClick={handleClick} onKeyPress={handleKeyPress}>
            <Nav className="mx-auto">
                <IconNavLink to="/" icon={faHome} title="home" srOnly />
                <IconLink menu="playlist" icon={faList} title="Playlist" srOnly />
                <IconLink menu="waitlist" icon={faUserClock} title="Waiting list" srOnly />
                <IconLink menu="search" icon={faSearch} title="Search" srOnly />
                <IconNavLink to="/add-room" icon={faPlusSquare} menu="add" title="Add room" srOnly />
                <IconNavLink to="/profile" icon={faUserCircle} title="Profile" srOnly />
            </Nav>
        </Navbar>
    );
};

export default FooterNavbar;
