import { useSelector } from 'react-redux';
import { AppState } from '../../stores';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Nav } from 'react-bootstrap';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import React from 'react';

interface IIconLinkProps extends React.HTMLProps<HTMLSpanElement> {
    icon: IconProp;
    srOnly?: boolean;
    title: string;
    menu: string;
}

const IconLink: React.FC<IIconLinkProps> = (props: IIconLinkProps) => {
    const isMenuOpen = useSelector<AppState, Record<string, boolean>>((state) => state.layout.isMenuOpen);

    if (props.menu in isMenuOpen) {
        return (
            <Nav.Link href={`#${props.menu}`} data-menu={props.menu} active={isMenuOpen[props.menu]}>
                <FontAwesomeIcon icon={props.icon} />
                <span id={`menu_item_${props.menu}`} className={props.srOnly ? 'sr-only' : ''}>
                    {props.title}
                </span>
            </Nav.Link>
        );
    }

    return null;
};

export default IconLink;
