import { useSelector } from 'react-redux';
import { AppState } from '../../stores';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Nav } from 'react-bootstrap';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import * as H from 'history';

interface IIconNavLinkProps<S = H.LocationState> extends React.HTMLProps<HTMLSpanElement> {
    to: H.LocationDescriptor<S> | ((location: H.Location<S>) => H.LocationDescriptor<S>);
    icon: IconProp;
    srOnly?: boolean;
    title: string;
    menu?: string;
}

const IconNavLink: React.FC<IIconNavLinkProps> = (props: IIconNavLinkProps) => {
    const isMenuOpen = useSelector<AppState, Record<string, boolean>>((state) => state.layout.isMenuOpen);
    const location = useLocation();

    if (props.menu === undefined || props.menu in isMenuOpen) {
        return (
            <Nav.Link as={Link} to={props.to} active={location.pathname == props.to}>
                <FontAwesomeIcon icon={props.icon} />
                <span className={props.srOnly ? 'sr-only' : ''}>{props.title}</span>
            </Nav.Link>
        );
    }

    return null;
};

export default IconNavLink;
