import { useSelector, useDispatch } from 'react-redux';
import { AppState } from '../../stores';
import { Toast } from 'react-bootstrap';
import React from 'react';
import { ToasterState, editToast } from '../../stores/layout.store';
import { IToastItem } from '../../interfaces/layout';

const Toaster: React.FC = () => {
    const toaster = useSelector<AppState, ToasterState>((state) => state.layout.toaster);
    const dispatch = useDispatch();
    const handleClose = (item: IToastItem, index: number) => {
        dispatch(editToast({ ...item, show: false }, index));
    };

    return (
        <div className="toast-container" aria-live="polite" aria-atomic="true">
            <div className="toast-wrapper">
                {toaster.entries.map((toast, index) => (
                    <Toast
                        style={{ minWidth: 300 }}
                        key={`toast_${index}`}
                        onClose={() => handleClose(toast, index)}
                        show={toast.show}
                        delay={toast.delay}
                        autohide={toast.autoHide}
                    >
                        <Toast.Header className="bg-primary text-white">
                            <strong className="mr-auto">{toast.title}</strong>
                            <small></small>
                        </Toast.Header>
                        <Toast.Body>{toast.message}</Toast.Body>
                    </Toast>
                ))}
            </div>
        </div>
    );
};

export default Toaster;
