import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useState } from 'react';
import { Alert, Button } from 'react-bootstrap';
import { useAppInsights } from '../../models/AppInsightsContext';
import { updateAppClicked } from '../../models/AppInsightsEvents';

const CE_NEW_WORKER = 'ct_new_servicer_worker';

let refreshing = false;
const register = () => {
    if ('serviceWorker' in navigator) {
        // The event listener that is fired when the service worker updates
        // Here we reload the page
        navigator.serviceWorker.addEventListener('controllerchange', function () {
            if (refreshing) return;
            refreshing = true;
            window.location.reload();
        });

        window.addEventListener('load', async () => {
            try {
                const reg = await navigator.serviceWorker.register('/sw.js', { scope: './' });
                console.log('ServiceWorker registration successful with scope: ', reg.scope);
                afterRegistration(reg);
            } catch (err) {
                console.log('ServiceWorker registration failed: ', err);
            }
        });
    } else {
        console.log('ServiceWorker is not supported.');
    }
};

const afterRegistration = (reg: ServiceWorkerRegistration) => {
    if (reg !== null) {
        if (reg.waiting && reg.active) {
            // The page has been loaded when there's already a waiting and active SW.
            // This would happen if skipWaiting() isn't being called, and there are
            // still old tabs open.
            console.log('ServiceWorker a new update is waiting to be activated.');
            dispatchEvent(reg.waiting);
        }

        reg.addEventListener('updatefound', () => {
            waitTillInstalled(reg.installing);
            console.log('ServiceWorker a new update is being installed in the background.');
        });
    }
};

const dispatchEvent = (newServiceWorker: ServiceWorker) => {
    const c_event = new CustomEvent<ServiceWorker>(CE_NEW_WORKER, {
        detail: newServiceWorker,
    });

    console.log('ServiceWorker a new update is waiting to be activated.');
    window.dispatchEvent(c_event);
};

const waitTillInstalled = (newServiceWorker: ServiceWorker) => {
    if (newServiceWorker !== null) {
        newServiceWorker.addEventListener('statechange', () => {
            // Has service worker state changed?
            switch (newServiceWorker.state) {
                case 'installed':
                    // There is a new service worker available, show the notification
                    if (navigator.serviceWorker.controller) {
                        dispatchEvent(newServiceWorker);
                    }
                    break;
            }
        });
    }
};

const ServiceWorker: React.FC = () => {
    const [serviceWorker, setServiceWorker] = useState<ServiceWorker | null>(null);
    const [isInstalling, setIsInstalling] = useState(false);
    const ai = useAppInsights();

    useEffect(() => {
        window.addEventListener(CE_NEW_WORKER, (e: CustomEvent<ServiceWorker>) => {
            setServiceWorker(e.detail);
        });
        register();
    }, []);

    const update = () => {
        if (serviceWorker === null) {
            return;
        }

        setIsInstalling(true);
        ai.trackEvent(updateAppClicked);
        serviceWorker.postMessage({ action: 'skipWaiting' });
    };

    if (serviceWorker !== null) {
        return (
            <Alert variant="info">
                There is a update available, please update.
                <Button size="sm" variant="secondary" onClick={update} className="float-right">
                    <FontAwesomeIcon icon={faSpinner} size="sm" style={{ opacity: 0 }} />
                    <span className="mx-3">Install</span>
                    <FontAwesomeIcon icon={faSpinner} spin size="sm" style={{ opacity: isInstalling ? 1 : 0 }} />
                </Button>
            </Alert>
        );
    }

    return null;
};

export default ServiceWorker;
