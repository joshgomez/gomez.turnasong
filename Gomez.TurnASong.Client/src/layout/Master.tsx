import React from 'react';
import { Row, Container, Col } from 'react-bootstrap';
import FooterNavbar from './FooterNavbar';
import HeaderNavbar from './HeaderNavbar';
import Toaster from './common/Toaster';
import ServiceWorker from './common/ServiceWorker';

interface IMasterProps {
    children?: React.ReactNode;
}

const Master: React.FC<IMasterProps> = (props: IMasterProps) => {
    return (
        <>
            <Container fluid className="px-0">
                <HeaderNavbar></HeaderNavbar>
                <ServiceWorker />
            </Container>
            <Container fluid>
                <Toaster></Toaster>
            </Container>
            <Container className="pt-3">
                <Row>
                    <Col>
                        <main>{props.children}</main>
                    </Col>
                </Row>
            </Container>

            <FooterNavbar></FooterNavbar>
        </>
    );
};

export default Master;
