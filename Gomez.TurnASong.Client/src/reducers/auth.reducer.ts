import {
    SetTokenAction,
    SetIsAuthenticatingAction,
    AuthState,
    initialAuthState,
    SetUserIdAction,
} from '../stores/auth.store';

export type AuthActions = SetTokenAction | SetIsAuthenticatingAction | SetUserIdAction;

export function authReducer(state: AuthState = initialAuthState, action: AuthActions): AuthState {
    switch (action.type) {
        case 'SetToken':
            return { ...state, data: action.payload, isAuthenticating: false };
        case 'SetIsAuthenticating':
            return { ...state, isAuthenticating: action.payload };
        case 'SetUserId':
            return { ...state, userId: action.payload };
        default:
            neverReached(action);
    }

    return state;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const neverReached = (_never: never) => {
    // neverReached
};
