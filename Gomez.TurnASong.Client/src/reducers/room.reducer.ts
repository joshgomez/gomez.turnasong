import {
    SetRoomAction,
    SetIsLoadingAction,
    initialRoomState,
    RoomState,
    SetMediaPlayerOptionsAction,
} from '../stores/room.store';

export type RoomActions = SetRoomAction | SetIsLoadingAction | SetMediaPlayerOptionsAction;

export function roomReducer(state: RoomState = initialRoomState, action: RoomActions): RoomState {
    switch (action.type) {
        case 'SetRoom':
            return { ...state, data: action.payload, isLoading: false };
        case 'SetIsLoading':
            return { ...state, isLoading: action.payload };
        case 'SetMediaPlayerOptions':
            return { ...state, mediaPlayerOption: action.payload };
        default:
            neverReached(action);
    }

    return state;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const neverReached = (_never: never) => {
    // neverReached
};
