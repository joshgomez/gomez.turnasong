import { SetServiceStateAction, ServiceState, initialServiceState, SetIsLoadedAction } from '../stores/service.store';

export type ServiceActions = SetIsLoadedAction | SetServiceStateAction;

export function serviceReducer(state: ServiceState = initialServiceState, action: ServiceActions): ServiceState {
    switch (action.type) {
        case 'SetServiceState':
            return action.payload;
        case 'SetIsLoaded':
            return { ...state, isLoaded: action.payload };
        default:
            neverReached(action);
    }

    return state;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const neverReached = (_never: never) => {
    // neverReached
};
