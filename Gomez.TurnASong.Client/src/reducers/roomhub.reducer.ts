import {
    SetHubAction,
    SetIsLoadedAction,
    initialRoomHubState,
    RoomHubState,
    SetPingTimerAction,
} from '../stores/roomhub.store';

export type RoomHubActions = SetHubAction | SetIsLoadedAction | SetPingTimerAction;

export function roomHubReducer(state: RoomHubState = initialRoomHubState, action: RoomHubActions): RoomHubState {
    switch (action.type) {
        case 'SetHub':
            return { ...state, hub: action.payload, isLoaded: !!action.payload };
        case 'HubSetIsLoaded':
            return { ...state, isLoaded: action.payload };
        case 'HubSetPingTimer':
            return { ...state, pingTimer: action.payload };
        default:
            neverReached(action);
    }

    return state;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const neverReached = (_never: never) => {
    // neverReached
};
