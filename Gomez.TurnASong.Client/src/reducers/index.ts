import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { AppState } from '../stores';
import { authReducer } from './auth.reducer';
import { serviceReducer } from './service.reducer';
import { roomReducer } from './room.reducer';
import { roomHubReducer } from './roomhub.reducer';
import { layoutReducer } from './layout.reducer';

const rootReducer = combineReducers<AppState>({
    routing: routerReducer,
    auth: authReducer,
    services: serviceReducer,
    room: roomReducer,
    roomHub: roomHubReducer,
    layout: layoutReducer,
});

export default rootReducer;
