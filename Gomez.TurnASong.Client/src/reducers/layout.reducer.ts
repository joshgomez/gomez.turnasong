import {
    SetShowNavbarAction,
    SetIsMenuOpenAction,
    initialLayoutState,
    LayoutState,
    SetTitleAction,
    SetToasterAction,
    ClearIsMenuOpenAction,
} from '../stores/layout.store';

export type LayoutActions =
    | SetShowNavbarAction
    | SetIsMenuOpenAction
    | SetTitleAction
    | ClearIsMenuOpenAction
    | SetToasterAction;

export function layoutReducer(state: LayoutState = initialLayoutState, action: LayoutActions): LayoutState {
    switch (action.type) {
        case 'LayoutSetIsMenuOpen':
            const isMenuOpen = Object.assign({}, state.isMenuOpen);
            for (const key in isMenuOpen) {
                isMenuOpen[key] = false;
            }

            isMenuOpen[action.payload.key] = action.payload.value;
            if (action.payload.value === null) {
                delete isMenuOpen[action.payload.key];
            }

            return { ...state, isMenuOpen: isMenuOpen };
        case 'LayoutSetShowNavbar':
            return { ...state, showNavbar: action.payload };
        case 'LayoutSetTitle':
            return { ...state, title: action.payload };
        case 'LayoutClearIsMenuOpen':
            return { ...state, isMenuOpen: {} };
        case 'LayoutSetToaster':
            return { ...state, toaster: action.payload };
        default:
            neverReached(action);
    }

    return state;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const neverReached = (_never: never) => {
    // neverReached
};
