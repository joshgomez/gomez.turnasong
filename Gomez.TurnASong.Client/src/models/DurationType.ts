export enum DurationType {
    Any = 0,
    Short,
    Medium,
    Long,
}
