export enum MediaPlayerStateType {
    NotReady = 0,
    Ready,
    Ended,
}
