import { useState, useRef, useEffect } from 'react';

interface UseTimeoutHandler {
    start: () => void;
    clear: () => void;
    stop: () => void;
    isActive: boolean;
}

function useTimeout(cb: () => void, timeoutDelayMs = 0): UseTimeoutHandler {
    const [isTimeoutActive, setIsTimeoutActive] = useState(false);
    const savedRefCallback = useRef<() => any>();

    useEffect(() => {
        savedRefCallback.current = cb;
    }, [cb]);

    function callback() {
        savedRefCallback.current && savedRefCallback.current();
        clear();
    }

    function clear() {
        setIsTimeoutActive(false);
    }
    function start() {
        setIsTimeoutActive(true);
    }

    useEffect(() => {
        if (isTimeoutActive) {
            const timeout = window.setTimeout(callback, timeoutDelayMs);
            return () => {
                window.clearTimeout(timeout);
            };
        }
    }, [isTimeoutActive]);
    return {
        clear,
        start,
        stop: clear,
        isActive: isTimeoutActive,
    };
}

export { useTimeout };
