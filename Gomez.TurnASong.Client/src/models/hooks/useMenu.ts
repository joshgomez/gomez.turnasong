import * as React from 'react';
import { useDispatch } from 'react-redux';
import { setIsMenuOpen, clearIsMenuOpen } from '../../stores/layout.store';

const { useEffect } = React;
export function useMenu(menu: string[], enabled: boolean[]): void {
    const dispatch = useDispatch();
    if (menu.length != enabled.length) {
        throw 'missmatch';
    }

    useEffect(() => {
        dispatch(clearIsMenuOpen());
        for (let index = 0; index < menu.length; index++) {
            dispatch(setIsMenuOpen(menu[index], enabled[index]));
        }
    }, []);
}
