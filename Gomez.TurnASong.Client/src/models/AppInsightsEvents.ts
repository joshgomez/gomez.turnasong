import { IEventTelemetry } from '@microsoft/applicationinsights-web';

export const signInButtonClicked: IEventTelemetry = { name: 'As a user I clicked on sign-in, {provider}' };
export const formInvalidSubmitted: IEventTelemetry = { name: 'As a user I filled the form wrong, {name}' };
export const brandLogoClicked: IEventTelemetry = { name: 'As a user I clicked on brand logo' };
export const signOutClicked: IEventTelemetry = { name: 'As a user I clicked sign-out' };
export const updateAppClicked: IEventTelemetry = { name: 'As a user I clicked on install to update the app' };
