import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import { ReactPlugin, withAITracking } from '@microsoft/applicationinsights-react-js';
import * as H from 'history';
import { Config } from '../config';

const reactPlugin = new ReactPlugin();
const initialize = (history: H.History<unknown>): ApplicationInsights => {
    const ai = new ApplicationInsights({
        config: {
            instrumentationKey: Config.appInsKey,
            extensions: [reactPlugin],
            extensionConfig: {
                [reactPlugin.identifier]: { history: history },
            },
        },
    });

    ai.loadAppInsights();
    return ai;
};

function withAppInsTracking<T>(Component: React.ComponentType<T>): React.ComponentClass<T, any> {
    return withAITracking(reactPlugin, Component);
}

export default withAppInsTracking;

export { reactPlugin, initialize };
