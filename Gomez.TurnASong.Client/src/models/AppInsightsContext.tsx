import { ReactPlugin } from '@microsoft/applicationinsights-react-js';
import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import React, { useEffect, useState } from 'react';
import { createContext } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { AppState } from '../stores';
import { reactPlugin, initialize } from './AppInsights';

const AppInsightsContext = createContext(reactPlugin);
interface IAppInsightsContextProps {
    children: React.ReactNode;
}

const AppInsightsContextProvider = ({ children }: IAppInsightsContextProps): JSX.Element => {
    const history = useHistory();
    const userId = useSelector<AppState, string>((state) => state.auth.userId);
    const [ai, setAi] = useState<ApplicationInsights | null>(null);

    useEffect(() => {
        setAi(initialize(history));
    }, []);

    useEffect(() => {
        if (ai !== null && userId) {
            ai.setAuthenticatedUserContext(userId);
        }
    }, [ai, userId]);

    if (ai == null) {
        return null;
    }

    return <AppInsightsContext.Provider value={reactPlugin}>{children}</AppInsightsContext.Provider>;
};

export { AppInsightsContext, AppInsightsContextProvider };
export const useAppInsights = (): ReactPlugin => React.useContext(AppInsightsContext);
