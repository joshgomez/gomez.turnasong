import { HubConnection } from '@microsoft/signalr';
import { Dispatch } from 'redux';
import { stackToast } from '../stores/layout.store';

export default class MediaService {
    public static async AddMediaAsync(
        target: HTMLAnchorElement | null,
        hub: HubConnection,
        dispatch: Dispatch<any>,
    ): Promise<boolean> {
        if (target === null) {
            return false;
        }

        const mediaId = target.getAttribute('data-media-id');
        if (!mediaId) {
            return false;
        }

        const songTitle = decodeURIComponent(target.hash.substr(1));

        try {
            await hub.send('AddMediaToQueueAsync', mediaId);
            dispatch(stackToast({ title: 'Added media', autoHide: true, delay: 6000, message: songTitle, show: true }));
        } catch (err) {
            return false;
        }

        return true;
    }
}
