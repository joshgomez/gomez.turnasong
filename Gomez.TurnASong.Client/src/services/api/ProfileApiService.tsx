﻿import { BaseApiService } from './BaseApiService';
import { IRoomsResponse, IRoomDto } from '../../interfaces/multimedia';
import { IProfileDto, IProfilePatchDto } from '../../interfaces/identity';

export class ProfileApiService extends BaseApiService {
    constructor(token: string) {
        super('api/identity/profiles', token);
    }

    async getAsync(id: string): Promise<IProfileDto> {
        try {
            const responseData: IProfileDto = await this.ajaxGetAsync<IProfileDto>('/' + encodeURIComponent(id));
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async patchAsync(postData: IProfilePatchDto): Promise<IProfileDto> {
        try {
            const responseData: IProfileDto = await this.ajaxPatchAsync<IProfileDto>('', postData);
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }
}
