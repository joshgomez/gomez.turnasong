﻿import { BaseApiService } from './BaseApiService';
import { IAuthData } from '../../interfaces/auth';

export class AuthApiService extends BaseApiService {
    constructor() {
        super('api/token', false);
    }

    async refreshAsync(data?: IAuthData): Promise<IAuthData | null | undefined> {
        try {
            const postData: any = { token: data.token, refreshToken: data.refreshToken.token };
            const responseData: IAuthData = await this.ajaxPostAsync<IAuthData>('/refresh', postData);
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }
}
