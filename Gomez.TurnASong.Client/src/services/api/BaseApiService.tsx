﻿import { Config } from '../../config';

export class BaseApiService {
    private endpoint: string;
    private token: string | false;

    constructor(endpoint: string, token: string | false) {
        this.endpoint = endpoint;
        this.token = token;
    }

    protected getEndPoint(): string {
        return `${Config.apiEndPoint}/${this.endpoint}`;
    }

    public ajaxGetAsync<T>(urlSegments: string): Promise<T> {
        return fetch(this.getEndPoint() + urlSegments, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: this.getHeader(),
            referrer: 'no-referrer', // no-referrer, *client
        }).then((response) => response.json());
    }

    public ajaxPutAsync<T>(urlSegments: string, postData: any): Promise<T> {
        return this.ajaxFetchAsync('PUT', urlSegments, postData);
    }

    public ajaxPostAsync<T>(urlSegments: string, postData: any): Promise<T> {
        return this.ajaxFetchAsync('POST', urlSegments, postData);
    }

    public ajaxPatchAsync<T>(urlSegments: string, postData: any): Promise<T> {
        return this.ajaxFetchAsync('PATCH', urlSegments, postData);
    }

    public ajaxDeleteAsync(urlSegments: string, postData?: any): Promise<void> {
        return this.ajaxFetchAsync('DELETE', urlSegments, postData);
    }

    private getHeader(): HeadersInit {
        const header: HeadersInit = {};
        header['Content-Type'] = 'application/json';
        if (this.token) {
            const bearer = 'Bearer ' + this.token;
            header['Authorization'] = bearer;
        }

        return header;
    }

    private ajaxFetchAsync(method: string, urlSegments: string, postData?: any): Promise<any> {
        return fetch(this.getEndPoint() + urlSegments, {
            method: method, // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'include', // include, *same-origin, omit
            headers: this.getHeader(),
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: postData !== undefined ? JSON.stringify(postData) : '', // body data type must match "Content-Type" header
        }).then((response) => (response.status === 200 ? response.json() : null)); // parses response to JSON
    }
}
