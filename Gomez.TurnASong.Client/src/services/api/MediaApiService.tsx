﻿import { BaseApiService } from './BaseApiService';
import { IMediaResponse, IPlaylistItemDto } from '../../interfaces/multimedia';
import { IContinuedList } from '../../interfaces/common';
import { DurationType } from '../../models/DurationType';
import { IUserIdentity } from '../../interfaces/identity';

export class MediaApiService extends BaseApiService {
    constructor(token: string) {
        super('api/multimedia/media', token);
    }

    async getAllAsync(search?: string, videoDuration?: DurationType): Promise<IMediaResponse> {
        const params = new URLSearchParams({
            search: search ?? '',
            videoDuration: videoDuration.toString() ?? DurationType.Any.toString(),
        });

        try {
            const responseData: IMediaResponse = await this.ajaxGetAsync<IMediaResponse>('?' + params.toString());
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async getMyHistoryAsync(continuationToken?: string): Promise<IContinuedList<IPlaylistItemDto>> {
        const params = new URLSearchParams({
            continuationToken: continuationToken ?? '',
        });

        try {
            const responseData: IContinuedList<IPlaylistItemDto> = await this.ajaxGetAsync<
                IContinuedList<IPlaylistItemDto>
            >('/my-history?' + params.toString());
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async getMyQueueAsync(continuationToken?: string): Promise<IContinuedList<IPlaylistItemDto>> {
        const params = new URLSearchParams({
            continuationToken: continuationToken ?? '',
        });

        try {
            const responseData: IContinuedList<IPlaylistItemDto> = await this.ajaxGetAsync<
                IContinuedList<IPlaylistItemDto>
            >('/my-queue?' + params.toString());
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async getHistoryAsync(continuationToken?: string): Promise<IContinuedList<IPlaylistItemDto>> {
        const params = new URLSearchParams({
            continuationToken: continuationToken ?? '',
        });

        try {
            const responseData: IContinuedList<IPlaylistItemDto> = await this.ajaxGetAsync<
                IContinuedList<IPlaylistItemDto>
            >('/history?' + params.toString());
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async getWaitingUsersAsync(): Promise<IUserIdentity[]> {
        try {
            const responseData: IUserIdentity[] = await this.ajaxGetAsync<IUserIdentity[]>('/waiting');
            return responseData;
        } catch (err) {
            console.log('err', err);
            return [];
        }
    }

    async deleteQueueItemAsync(id: string): Promise<void> {
        try {
            await this.ajaxDeleteAsync(`/${encodeURIComponent(id)}`);
        } catch (err) {
            console.log('err', err);
        }
    }
}
