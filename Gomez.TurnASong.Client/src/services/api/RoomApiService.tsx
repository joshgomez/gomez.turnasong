﻿import { BaseApiService } from './BaseApiService';
import { IRoomsResponse, IRoomDto, IRoomPostDto, IRoomPatchDto } from '../../interfaces/multimedia';

export class RoomApiService extends BaseApiService {
    constructor(token: string) {
        super('api/multimedia/rooms', token);
    }

    async getAllAsync(search?: string, continuationToken?: string): Promise<IRoomsResponse> {
        const params = new URLSearchParams({
            search: search ?? '',
            continuationToken: continuationToken ?? '',
        });

        try {
            const responseData: IRoomsResponse = await this.ajaxGetAsync<IRoomsResponse>('?' + params.toString());
            return responseData;
        } catch (err) {
            console.log('err', err);
            return { continuationToken: null, search: '', rooms: { documents: [] } };
        }
    }

    async joinAsync(roomId: string): Promise<IRoomDto | null> {
        try {
            const responseData: IRoomDto = await this.ajaxGetAsync<IRoomDto>(`/${roomId}/join`);
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async postAsync(data: IRoomPostDto): Promise<IRoomDto | null> {
        try {
            const responseData: IRoomDto = await this.ajaxPostAsync<IRoomDto>(``, data);
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async patchAsync(postData: IRoomPatchDto): Promise<IRoomDto | null> {
        try {
            const responseData: IRoomDto = await this.ajaxPatchAsync<IRoomDto>(``, postData);
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async deleteAsync(id: string): Promise<void> {
        try {
            await this.ajaxDeleteAsync(`/${id}`);
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async leaveAsync(): Promise<IRoomDto | null> {
        try {
            const responseData: IRoomDto = await this.ajaxGetAsync<IRoomDto>(`/leave`);
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }

    async byUserAsync(): Promise<IRoomDto | null> {
        try {
            const responseData: IRoomDto = await this.ajaxGetAsync<IRoomDto>(`/by-user`);
            return responseData;
        } catch (err) {
            console.log('err', err);
            return null;
        }
    }
}
