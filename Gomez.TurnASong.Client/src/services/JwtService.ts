import jwtDecode from 'jwt-decode';
import { IJwtPayload, IAuthData } from '../interfaces/auth';

export default class JwtService {
    static decodeToken(value: string): IJwtPayload {
        return jwtDecode<IJwtPayload>(value);
    }

    static remainingSecondsToInvalidation(data: IAuthData | null, accessInvalidationOffset: number): number {
        if (data === null) {
            return 0;
        }

        const sec: number = new Date().getTime() / 1000;
        const decodedToken: IJwtPayload = JwtService.decodeToken(data.token);
        return decodedToken.exp - (sec + accessInvalidationOffset);
    }

    static isAuthenticated(data: IAuthData): boolean {
        if (data?.refreshToken?.expires != null) {
            const milliseconds: number = new Date().getTime();
            const expires: Date = new Date(data.refreshToken.expires);
            return expires.getTime() > milliseconds;
        }

        return false;
    }

    static getUserId(data: IAuthData): string {
        const payload = JwtService.decodeToken(data.token);
        return JwtService.getUserIdByJwt(payload);
    }

    static getUserIdByJwt(payload: IJwtPayload): string | null {
        if (payload) {
            return payload.sub;
        }

        return null;
    }
}
