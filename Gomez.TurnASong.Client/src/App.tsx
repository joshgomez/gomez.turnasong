import React, { FunctionComponent, Suspense, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch, useLocation } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import { useSelector, useDispatch } from 'react-redux';
import { AppState } from './stores';
import { serviceActions } from './stores/service.store';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Master from './layout/Master';
import { RouteConstant } from './constants/RouteConstant';
import PageLoader from './pages/common/PageLoader';
import { AppInsightsContextProvider } from './models/AppInsightsContext';

const LazyRoom = React.lazy(() => import(/* webpackChunkName: "Room" */ './pages/Room'));
const LazyHomePage = React.lazy(() => import(/* webpackChunkName: "HomePage" */ './pages/HomePage'));
const LazyRoomSettings = React.lazy(() => import(/* webpackChunkName: "RoomSettings" */ './pages/RoomSettings'));
const LazyProfileSettings = React.lazy(
    () => import(/* webpackChunkName: "ProfileSettings" */ './pages/ProfileSettings'),
);
const LazyProfile = React.lazy(() => import(/* webpackChunkName: "Profile" */ './pages/Profile'));
const LazyAddRoom = React.lazy(() => import(/* webpackChunkName: "AddRoom" */ './pages/AddRoom'));
const LazyNotFound = React.lazy(() => import(/* webpackChunkName: "NotFound" */ './pages/NotFound'));
const LazySignIn = React.lazy(() => import(/* webpackChunkName: "SignIn" */ './pages/SignIn'));

const AnimatedSwitch: FunctionComponent = () => {
    const location = useLocation();
    return (
        <TransitionGroup className="transition-group">
            <CSSTransition key={location.key} classNames="fade" timeout={500}>
                <section className="transition-route-section">
                    <Suspense fallback={<PageLoader />}>
                        <Switch location={location}>
                            <PrivateRoute exact path={RouteConstant.main} component={LazyHomePage} />
                            <PrivateRoute exact path={RouteConstant.room} component={LazyRoom} />
                            <PrivateRoute exact path={RouteConstant.roomsSettings} component={LazyRoomSettings} />
                            <PrivateRoute exact path={RouteConstant.profileSettings} component={LazyProfileSettings} />
                            <PrivateRoute exact path={RouteConstant.profile} component={LazyProfile} />
                            <PrivateRoute exact path={RouteConstant.addRoom} component={LazyAddRoom} />
                            <Route exact path={RouteConstant.signIn} component={LazySignIn} />
                            <Route component={LazyNotFound} />
                        </Switch>
                    </Suspense>
                </section>
            </CSSTransition>
        </TransitionGroup>
    );
};

const App: FunctionComponent = () => {
    const token = useSelector<AppState, string>((state) => state.auth.data.token);
    const dispatch = useDispatch();

    //We should init all services here
    useEffect(() => {
        if (token) {
            dispatch(serviceActions.setRoomApiService());
            dispatch(serviceActions.setIsLoaded(true));
        }
    }, [token]);

    return (
        <Router>
            <Master>
                <AppInsightsContextProvider>
                    <AnimatedSwitch />
                </AppInsightsContextProvider>
            </Master>
        </Router>
    );
};

export default App;
