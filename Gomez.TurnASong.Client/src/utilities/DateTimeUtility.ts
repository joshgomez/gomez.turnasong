﻿import Moment from 'moment';

export class DateTimeUtility {
    public static getRemainingPercentage(strStartDate: string, strEndDate: string): number {
        const now = Moment().unix();
        const startDate = Moment(strStartDate).unix();
        const endDate = Moment(strEndDate).unix();
        let percentage_complete = ((now - startDate) / (endDate - startDate)) * 100;
        percentage_complete = Math.round(percentage_complete * 100) / 100;
        percentage_complete = Math.min(percentage_complete, 100);
        percentage_complete = Math.max(0, percentage_complete);
        return percentage_complete;
    }

    public static getElapsedTimeInSeconds(strStartDate: string): number {
        return Moment().utc().unix() - Moment(strStartDate).unix();
    }

    private static pad(num: number) {
        return ('0' + num).slice(-2);
    }

    public static hhmmss(secs?: number): string {
        let sec_num = secs ?? 0;
        sec_num = Math.round(sec_num);
        const hours = Math.floor(sec_num / 3600);
        const minutes = Math.floor((sec_num - hours * 3600) / 60);
        const seconds = sec_num - hours * 3600 - minutes * 60;

        if (hours > 0) {
            return `${DateTimeUtility.pad(hours)}:${DateTimeUtility.pad(minutes)}:${DateTimeUtility.pad(seconds)}`;
        }

        return `${DateTimeUtility.pad(minutes)}:${DateTimeUtility.pad(seconds)}`;
    }
}
