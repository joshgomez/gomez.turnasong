export class EventUtility {
    public static getTargetMouseEvent<T = Element>(evt: React.MouseEvent<T, MouseEvent>, tagName: string): HTMLElement {
        const unknownTarget: HTMLElement = evt.target as HTMLElement;
        tagName = tagName.toUpperCase();
        if (unknownTarget.tagName === tagName) {
            return unknownTarget;
        }

        return unknownTarget.closest(tagName);
    }

    public static getTargetKeyboardEvent<T = Element>(
        evt: React.KeyboardEvent<T>,
        tagName: string,
        keyCodes: string[],
    ): HTMLElement | null {
        if (keyCodes.indexOf(evt.key) === -1) {
            return null;
        }

        const unknownTarget: HTMLElement = evt.target as HTMLElement;
        tagName = tagName.toUpperCase();
        if (unknownTarget.tagName === tagName) {
            return unknownTarget;
        }

        return unknownTarget.closest(tagName);
    }
}
