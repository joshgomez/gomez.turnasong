import * as url from 'url';

export default class UrlUtility {
    public static getHashParameters(): { [s: string]: string } {
        const ret: { [s: string]: string } = {};
        const currentUrl: url.UrlWithStringQuery = url.parse(window.location.href);
        if (currentUrl.hash && currentUrl.hash.length > 1) {
            currentUrl.hash
                .substring(1)
                .split('&')
                .forEach(function (x) {
                    const arr = x.split('=');
                    arr[1] && (ret[arr[0]] = arr[1]);
                });
        }

        return ret;
    }
}
