﻿export interface IProperty {
    key: string;
    typeNum: number;
    value: any;
}

export interface IPropertyCollection {
    properties: Array<IProperty>;
}

export class PropertyUtility {
    properties: Array<IProperty>;

    constructor(properties: Array<IProperty>) {
        this.properties = properties;
    }

    public Find(key: string): IProperty {
        return PropertyUtility.Find(this.properties, key);
    }

    public static Find(properties: Array<IProperty>, key: string): IProperty {
        return properties.find(function (element) {
            return element.key == key;
        });
    }

    public static FindFromCollection(collection: IPropertyCollection, key: string): IProperty {
        return collection.properties.find(function (element) {
            return element.key == key;
        });
    }
}
