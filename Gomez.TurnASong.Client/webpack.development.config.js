﻿/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
module.exports = {
    watch: false,
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'wwwroot/js'),
        publicPath: '/js/',
        filename: '[name].js',
        chunkFilename: '[name].js',
        sourceMapFilename: '[name].map',
    },
    plugins: [],
};
