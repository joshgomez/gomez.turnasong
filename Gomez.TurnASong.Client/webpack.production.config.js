﻿/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
module.exports = {
    watch: false,
    mode: 'production',
    output: {
        path: path.resolve(__dirname, 'wwwroot/js'),
        publicPath: '/js/',
        filename: '[name].[contenthash].js',
        chunkFilename: '[name].[contenthash].js',
        sourceMapFilename: '[name].[contenthash].map',
    },
};
