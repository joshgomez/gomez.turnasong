﻿using Gomez.TurnASong.Models.Areas.Identity;

namespace Gomez.TurnASong.ViewModels.Areas.Identity.Profiles
{
    public class ProfileDto
    {
        public ProfileDto()
        {

        }

        public ProfileDto(ApplicationUser user)
        {
            Id = user.Id;
            Nickname = user.Nickname;
            UserName = user.UserName;
        }

        public string Id { get; set; }
        public string Nickname { get; set; }
        public string UserName { get; set; }
    }
}
