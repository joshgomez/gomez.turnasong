﻿using Gomez.TurnASong.Models.Areas.Identity;
using System.ComponentModel.DataAnnotations;

namespace Gomez.TurnASong.ViewModels.Areas.Identity.Profiles
{
    public class ProfilePatchDto
    {
        public ProfilePatchDto()
        {

        }

        public ProfilePatchDto(ApplicationUser user)
        {
            Id = user.Id;
            Nickname = user.Nickname;
        }

        public string Id { get; set; }

        [MinLength(2)]
        [MaxLength(64)]
        public string Nickname { get; set; }
    }
}
