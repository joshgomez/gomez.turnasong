﻿using Gomez.Core.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.ViewModels.Areas.Common;
using Gomez.TurnASong.ViewModels.Areas.Multimedia.Media;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms
{
    public class RoomDto
    {
        public RoomDto()
        {

        }

        public RoomDto(Room room)
        {
            Id = room.Id;
            Name = room.Name;
            VideoDuration = room.VideoDuration;
            if(room.CurrentMedia != null)
            {
                CurrentMedia = new MediaDto(room);
            }

            Owner = new UserIdentityDto(room.Owner);
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public DurationType VideoDuration { get; set; }

        public MediaDto CurrentMedia { get; set; }
        public UserIdentityDto Owner { get; set; }
    }
}
