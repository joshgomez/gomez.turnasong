﻿using Gomez.Core.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia;
using System.ComponentModel.DataAnnotations;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms
{
    public class RoomPatchDto : IRoomUpdateDto
    {
        public RoomPatchDto()
        {

        }

        public RoomPatchDto(Room room)
        {
            Id = room.Id;
            Name = room.Name;
            VideoDuration = room.VideoDuration;
        }

        public string Id { get; set; }

        [MinLength(2)]
        [MaxLength(64)]
        public string Name { get; set; }

        public DurationType VideoDuration { get; set; }
    }
}
