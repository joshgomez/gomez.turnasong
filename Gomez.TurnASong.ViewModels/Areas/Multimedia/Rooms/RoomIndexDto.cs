﻿using AutoMapper;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Room;
using System.ComponentModel.DataAnnotations;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms
{
    public class RoomIndexDto
    {
        private static readonly MapperConfiguration _mapperConfig =
            new MapperConfiguration(cfg => cfg.CreateMap<RoomListItem, RoomIndexDto>());

        public RoomIndexDto()
        {

        }

        public RoomIndexDto(RoomListItem item)
        {
            _mapperConfig.CreateMapper().Map(item, this);
        }

        public string Id { get; set; }

        [Display(Name = "Name")]
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public int OnlineCount { get; set; }
    }
}
