﻿using Gomez.TurnASong.Models.Areas.Multimedia;
using System.ComponentModel.DataAnnotations;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms
{
    public class RoomPostDto : IRoomUpdateDto
    {
        public RoomPostDto()
        {

        }

        public RoomPostDto(Room room)
        {
            Name = room.Name;
        }

        [MinLength(2)]
        [MaxLength(64)]
        public string Name { get; set; }
    }
}
