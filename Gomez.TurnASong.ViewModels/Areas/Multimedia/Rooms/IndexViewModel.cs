﻿using Gomez.Core.DocumentDb;
using Gomez.Core.ViewModels.Shared;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms
{
    public class QueryIndexViewModel : QueryBaseModel<QueryIndexViewModel, IndexViewModel>
    {
        public string ContinuesToken { get; set; }
    }

    public class IndexViewModel : QueryIndexViewModel
    {
        public IndexViewModel(QueryIndexViewModel query) : base()
        {
            this.Update(query);
        }

        public ContinuedList<RoomIndexDto> Rooms { get; set; }
    }
}
