﻿namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms
{
    public class RoomUserDto
    {
        public string Id { get; set; }
        public string NickName { get; set; }
    }
}
