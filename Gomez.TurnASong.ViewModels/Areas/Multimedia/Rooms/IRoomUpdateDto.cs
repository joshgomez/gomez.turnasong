﻿namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms
{
    public interface IRoomUpdateDto
    {
        string Name { get; set; }
    }
}