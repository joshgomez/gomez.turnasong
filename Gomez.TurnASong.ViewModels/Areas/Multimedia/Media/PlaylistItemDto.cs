﻿using AutoMapper;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Media;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Media
{
    public class PlaylistItemDto
    {
        private static readonly MapperConfiguration _mapperConfig = 
            new MapperConfiguration(cfg => cfg.CreateMap<PlaylistItem, PlaylistItemDto>());

        public PlaylistItemDto()
        {

        }

        public PlaylistItemDto(PlaylistItem item)
        {
            _mapperConfig.CreateMapper().Map(item,this);
        }

        public string Id { get; set; }
        public string VideoId { get; set; }
        public string Title { get; set; }
        public double DurationAsSeconds { get; set; }
        public string Thumbnail { get; set; }

        public string UserId { get; set; }
        public string Nickname { get; set; }
    }
}
