﻿using Gomez.Core.Multimedia;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Media
{
    public class MediaIndexDto
    {
        public MediaIndexDto(IVideo<IThumbnail> media)
        {
            Id = media.Id;
            Title = media.Title;
            Description = media.Description;
            DurationInSeconds = media.DurationAsSeconds;
            Thumbnail = media.Thumbnails.MediumUrl;
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double DurationInSeconds { get; set; }
        public string Thumbnail { get; set; }
    }
}
