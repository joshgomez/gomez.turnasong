﻿using Gomez.Core.Multimedia;
using Gomez.Core.ViewModels.Shared;
using System.Collections.Generic;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Media
{
    public class QueryIndexViewModel : QueryBaseModel<QueryIndexViewModel, IndexViewModel>
    {
        public DurationType VideoDuration { get; set; }
    }

    public class IndexViewModel : QueryIndexViewModel
    {
        public IndexViewModel(QueryIndexViewModel query) : base()
        {
            this.Update(query);
        }

        public List<MediaIndexDto> Media { get; set; }
    }
}
