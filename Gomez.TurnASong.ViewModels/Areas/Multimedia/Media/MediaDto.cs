﻿using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.ViewModels.Areas.Common;
using System;

namespace Gomez.TurnASong.ViewModels.Areas.Multimedia.Media
{
    public class MediaDto
    {
        public MediaDto()
        {

        }

        public MediaDto(Room room)
        {
            Id = room.CurrentMedia.Id;
            Title = room.CurrentMedia.Title;
            StartTime = room.CurrentMedia.StartTime;
            DurationAsSeconds = room.CurrentMedia.DurationAsSeconds;

            ThumbnailHighRes = room.CurrentMedia.Thumbnails.HighUrl;
            Thumbnail = room.CurrentMedia.Thumbnails.StandardUrl;

            User = new UserIdentityDto(room.CurrentMedia.User);
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public double DurationAsSeconds { get; set; }

        public string Thumbnail { get; set; }
        public string ThumbnailHighRes { get; set; }

        public DateTime? StartTime { get; set; }
        public UserIdentityDto User { get; set; }

        public double ElapsedTime
        {
            get
            {
                var currentTime = DateTime.UtcNow;
                double diff = (currentTime - (StartTime ?? currentTime)).TotalSeconds;
                if (diff > 0d)
                {
                    return System.Math.Abs(diff);
                }

                return 0d;
            }
        }
    }
}
