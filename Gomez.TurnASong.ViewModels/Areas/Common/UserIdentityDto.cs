﻿using Gomez.TurnASong.Models.Areas.Common;

namespace Gomez.TurnASong.ViewModels.Areas.Common
{
    public class UserIdentityDto
    {
        public UserIdentityDto()
        {

        }

        public UserIdentityDto(UserIdentity user)
        {
            Id = user.Id;
            Name = user.Name;
        }

        public UserIdentityDto(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public string Id { get; set; }
        public string Name { get; set; }
    }
}
