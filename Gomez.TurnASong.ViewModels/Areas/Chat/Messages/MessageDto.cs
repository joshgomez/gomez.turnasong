﻿using System;

namespace Gomez.TurnASong.ViewModels.Areas.Chat.Messages
{
    public class MessageDto
    {
        public long Id { get; set; }
        public Guid UserId { get; set; }
        public string NickName { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
