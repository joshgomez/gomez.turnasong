﻿using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms;
using System.Threading.Tasks;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia
{
    public interface IRoomService
    {
        Task<(RoomDto room, ApplicationUser user)> AddMediaToQueueAsync(string userId, string videoId);
        Task DeleteAsync(string userId, string roomId, bool isAdmin);
        Task<RoomDto> GetAsync(string roomId);
        Task<(RoomDto room, ApplicationUser user)> GetByUserAsync(string userId);
        Task<IndexViewModel> IndexAsync(QueryIndexViewModel requestQuery);
        Task<(RoomDto room, ApplicationUser user)> JoinAsync(string userId, string roomId);
        Task<(RoomDto room, ApplicationUser user)> JoinAsync(string userId);
        Task<(RoomDto room, ApplicationUser user)> LeaveAsync(string userId);
        Task<RoomDto> PatchAsync(string userId, RoomPatchDto request, bool isAdmin);
        Task<RoomDto> PostAsync(string userId, RoomPostDto request);
        Task<(RoomDto room, ApplicationUser user)> SetEndedAsync(int activeUsers, string userId);
        Task<(RoomDto room, ApplicationUser user)> SetReadyAsync(int activeUsers, string userId);
    }
}