﻿using Gomez.Core.DocumentDb;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.Multimedia;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Media;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Gomez.TurnASong.ViewModels.Areas.Common;
using Gomez.TurnASong.ViewModels.Areas.Multimedia.Media;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia
{
    public class MediaService : IMediaService
    {
        private readonly IMediaProvider _mediaProvider;
        private readonly ApplicationUserManager _userManager;
        private readonly IRoomPlaylistItemRepository _roomPlaylistRepo;
        private readonly IRoomUserRepository _roomUserRepository;

        public MediaService(
            IMediaProvider mediaProvider,
            ApplicationUserManager userManager,
            IRoomPlaylistItemRepository roomPlaylistRepo,
            IRoomUserRepository roomUserRepository)
        {
            _mediaProvider = mediaProvider;
            _userManager = userManager;
            _roomPlaylistRepo = roomPlaylistRepo;
            _roomUserRepository = roomUserRepository;
        }

        public async Task<IndexViewModel> IndexAsync(QueryIndexViewModel request)
        {
            var vm = new IndexViewModel(request)
            {
                Media = (await _mediaProvider
                .SearchAsync(new MediaSearchQuery() 
                { 
                    MaxResults = 25, 
                    Query = request.Search,
                    VideoDuration = request.VideoDuration
                }))
                .Select(x => new MediaIndexDto(x))
                .Where(x => x.DurationInSeconds > 0)
                .ToList()
            };

            return vm;
        }

        public async Task<IContinuedList<PlaylistItemDto>> GetMyItemQueueAsync(string userId, string continuationToken)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if(user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            var myPlaylistQueueData = await _roomPlaylistRepo.GetMyItemsAsync(user.Id, user.RoomId, false, continuationToken);
            return SetMyPlaylistItems(user, myPlaylistQueueData);
        }

        public async Task<IContinuedList<PlaylistItemDto>> GetMyPlayedItemsAsync(string userId, string continuationToken)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            var myPlayedItemsData = await _roomPlaylistRepo.GetMyItemsAsync(user.Id, user.RoomId, true, continuationToken);
            return SetMyPlaylistItems(user, myPlayedItemsData);
        }

        public async Task<IContinuedList<PlaylistItemDto>> GetAllPlayedItemsByUserRoomAsync(string userId, string continuationToken)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            if (string.IsNullOrEmpty(user.RoomId))
            {
                throw new NotFoundException(nameof(user.RoomId));
            }

            return await GetAllPlayedItemsAsync(user.RoomId, continuationToken);
        }

        public async Task<IContinuedList<PlaylistItemDto>> GetAllPlayedItemsAsync(string roomId, string continuationToken)
        {
            var playedItemsData = await _roomPlaylistRepo.GetAllPlayedItemsAsync(roomId, continuationToken);
            var userIds = playedItemsData.Documents.Select(x => x.UserId).Distinct().ToArray();
            var nicknamesData = await _roomUserRepository.GetAllNicknamesByUserIdsAsync(userIds, roomId);

            var results = new ContinuedList<PlaylistItemDto>();
            results.Documents = new List<PlaylistItemDto>();
            results.ContinuesToken = playedItemsData.ContinuesToken;
            foreach (var item in playedItemsData.Documents)
            {
                results.Documents.Add(new PlaylistItemDto(item) 
                { 
                    Nickname = nicknamesData[item.UserId] 
                });
   
            }

            return results;
        }

        public async Task<IEnumerable<UserIdentityDto>> GetAllWaitingUsersByUserIdAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            if (string.IsNullOrEmpty(user.RoomId))
            {
                throw new NotFoundException(nameof(user.RoomId));
            }

            return await GetAllWaitingUsersAsync(user.RoomId);
        }

        public async Task<IEnumerable<UserIdentityDto>> GetAllWaitingUsersAsync(string roomId)
        {
            var userIds = (await _roomPlaylistRepo.GetAllWaitingUserIdsAsync(roomId, 51)).ToArray();
            var data = await _roomUserRepository.GetAllNicknamesByUserIdsAsync(userIds, roomId);
            var result = new UserIdentityDto[data.Count];

            for (int i = 0; i < userIds.Length; i++)
            {
                var userId = userIds[i];
                result[i] = new UserIdentityDto()
                {
                    Id = userId,
                    Name = data.ContainsKey(userId) ? data[userId] : string.Empty
                };
            }

            return result;
        }

        public async Task DeleteQueuedItemAsync(string id, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException(userId, nameof(user));
            }

            if (string.IsNullOrEmpty(user.RoomId))
            {
                throw new NotFoundException(nameof(user.RoomId));
            }

            await _roomPlaylistRepo.DeleteQueuedItemAsync(id, user.RoomId, user.Id);
        }

        private IContinuedList<PlaylistItemDto> SetMyPlaylistItems(ApplicationUser user, IContinuedList<PlaylistItem> myPlaylistData)
        {
            var results = new ContinuedList<PlaylistItemDto>();
            results.Documents = new List<PlaylistItemDto>();
            results.ContinuesToken = myPlaylistData.ContinuesToken;
            foreach (var item in myPlaylistData.Documents)
            {
                results.Documents.Add(new PlaylistItemDto(item)
                {
                    UserId = user.Id,
                    Nickname = user.Nickname
                });

            }

            return results;
        }
    }
}
