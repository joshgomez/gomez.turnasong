﻿using Gomez.Core.DocumentDb;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.Multimedia;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.RoomModels;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Gomez.TurnASong.ViewModels.Areas.Multimedia.Rooms;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia
{
    public class RoomService : IRoomService
    {
        private readonly IMediaProvider _mediaProvider;
        private readonly IRoomRepository _roomRepository;
        private readonly IRoomPlaylistItemRepository _roomPlaylistItemRepository;
        private readonly IRoomUserRepository _roomUserRepository;
        private readonly IRoomUserStateRepository _roomUserStateRepository;
        private readonly ApplicationUserManager _userManager;

        public RoomService(
            IMediaProvider mediaProvider,
            IRoomRepository roomRepository,
            IRoomPlaylistItemRepository roomPlaylistItemRepository,
            IRoomUserRepository roomUserRepository,
            IRoomUserStateRepository roomUserStateRepository,
            ApplicationUserManager userManager)
        {
            _mediaProvider = mediaProvider;
            _roomRepository = roomRepository;
            _userManager = userManager;
            _roomPlaylistItemRepository = roomPlaylistItemRepository;
            _roomUserRepository = roomUserRepository;
            _roomUserStateRepository = roomUserStateRepository;
        }

        public async Task<IndexViewModel> IndexAsync(QueryIndexViewModel requestQuery)
        {
            var vm = new IndexViewModel(requestQuery);
            var data = await _roomRepository.IndexAsync(requestQuery.Search,requestQuery.ContinuesToken);
            var results = new ContinuedList<RoomIndexDto>();
            results.Documents = data.Documents.Select(room => new RoomIndexDto(room)).ToList();
            results.ContinuesToken = data.ContinuesToken;
            vm.Rooms = results;
            return vm;
        }

        public async Task<RoomDto> GetAsync(string roomId)
        {
            var room = await _roomRepository.ReadAsync(roomId, roomId);
            return new RoomDto(room);
        }

        private bool ValidateRoomVideoDuration(Room room, IVideo<IThumbnail> video)
        {
            if(room.VideoDuration == DurationType.Short && video.DurationAsSeconds > 4 * 60)
            {
                return false;
            }

            if (room.VideoDuration == DurationType.Medium && video.DurationAsSeconds > 20 * 60)
            {
                return false;
            }

            return true;
        }

        public async Task<(RoomDto room, ApplicationUser user)> AddMediaToQueueAsync(string userId, string videoId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null && user.RoomId != null)
            {
                var video = await _mediaProvider.GetVideoByIdAsync(videoId);
                if (video != null && video.DurationAsSeconds > 0)
                {
                    var room = await _roomRepository.ReadAsync(user.RoomId, user.RoomId);
                    if (!ValidateRoomVideoDuration(room, video))
                    {
                        return (null,user);
                    }

                    var media = new RoomMedia()
                    {
                        Id = video.Id,
                        Description = video.Description,
                        DurationAsSeconds = video.DurationAsSeconds,
                        Title = video.Title,
                        Thumbnails = new RoomMediaThumbnail()
                        {
                            HighUrl = video.Thumbnails.HighUrl,
                            MediumUrl = video.Thumbnails.MediumUrl,
                            StandardUrl = video.Thumbnails.StandardUrl
                        }
                    };

                    bool addedToCurrent = false;
                    room = await _roomRepository.UpdateAsync(room, r => {
                        if(r.CurrentMedia == null)
                        {
                            var currentMedia = new RoomCurrentMedia(media);
                            currentMedia.User = new Models.Areas.Common.UserIdentity(user);
                            r.CurrentMedia = currentMedia;
                            addedToCurrent = true;
                            return r;
                        }

                        return r;
                    });

                    //We don't want same user to stack items in the queue as all users should have a change to play
                    var totalQueuedItems = await _roomPlaylistItemRepository.CountMyQueuedItemsAsync(user.Id,room.Id);
                    await _roomPlaylistItemRepository
                        .CreateAsync(new RoomPlaylistItem()
                        {
                            RoomId = room.Id,
                            Media = media,
                            UserId = user.Id,
                            DeletedAt = addedToCurrent ? (DateTime?)DateTime.UtcNow : null,
                            OrderNumber = totalQueuedItems
                        });

                    return (new RoomDto(room),user);
                }
            }

            return (null,user);
        }

        public async Task<(RoomDto room, ApplicationUser user)> GetByUserAsync(string userId)
        {
            var (room, user) = await GetByUserInternalAsync(userId);
            return (new RoomDto(room), user);
        }

        private async Task<(Room room, ApplicationUser user)> GetByUserInternalAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null && user.RoomId != null)
            {
                var room = await _roomRepository.ReadAsync(user.RoomId, user.RoomId);
                if (room == null)
                {
                    return (null, user);
                }

                return (room, user);
            }

            return (null, user);
        }

        public async Task<(RoomDto room, ApplicationUser user)> SetReadyAsync(int activeUsers, string userId)
        {
            var (room, user) = await GetByUserInternalAsync(userId);
            if (room != null && room.CurrentMedia != null && room.CurrentMedia.StartTime == null)
            {
                await _roomUserStateRepository.SetUserStateAsync(
                    user.Id,
                    room.Id,
                    room.CurrentMedia.DurationAsSeconds,
                    MediaPlayerStateType.Ready);

                var waitExpireAt = room.UpdatedAt.AddSeconds(30);
                if (DateTime.UtcNow > waitExpireAt || activeUsers == await _roomUserStateRepository
                    .GetUsersReadyCountAsync(room.Id))
                {
                    room = await _roomRepository.UpdateAsync(room, r =>
                    {
                        r.CurrentMedia.User = new Models.Areas.Common.UserIdentity(user);
                        r.CurrentMedia.StartTime = DateTime.UtcNow;
                        return r;
                    });

                    return (new RoomDto(room),user);
                }
            }

            return (null,user);
        }

        private bool HasMediaEnded(Room room)
        {
            if(room != null && room.CurrentMedia?.StartTime != null)
            {
                var expireAt = room.CurrentMedia.StartTime.Value
                    .AddSeconds(room.CurrentMedia.DurationAsSeconds);
                if(DateTime.UtcNow >= expireAt)
                {
                    return true;
                }
            }

            return false;
        }

        public async Task DeleteAsync(string userId, string roomId, bool isAdmin)
        {
            var room = await _roomRepository.ReadAsync(roomId, roomId);
            if (room == null)
            {
                throw new NotFoundException(nameof(room));
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            if (!isAdmin && room.Owner.Id != user.Id)
            {
                throw new AccessDeniedException(user.Id);
            }

            if(await _roomUserRepository.GetIsUsersActiveAsync(room.Id))
            {
                throw new ExistsException(room.Id, "Users_Exists");
            }

            await _roomRepository.DeleteAsync(room);
        }

        public async Task<RoomDto> PostAsync(string userId, RoomPostDto request)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            ValidateRoomName(request);

            var room = new Room()
            {
                Name = request.Name,
                Owner = new Models.Areas.Common.UserIdentity()
                {
                    Id = user.Id,
                    Name = user.Nickname
                }
            };

            room = await _roomRepository.CreateAsync(room);
            return new RoomDto(room);
        }

        public async Task<RoomDto> PatchAsync(string userId, RoomPatchDto request, bool isAdmin)
        {
            ValidateRoomName(request);

            var room = await _roomRepository.ReadAsync(request.Id, request.Id);
            if (room == null)
            {
                throw new NotFoundException(nameof(room));
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            if (!isAdmin && room.Owner.Id != user.Id)
            {
                throw new AccessDeniedException(user.Id);
            }

            room.Name = request.Name;
            room.VideoDuration = request.VideoDuration;
            room = await _roomRepository.ReplaceAsync(_roomRepository.CreateOptions(room, new ItemOptions { IfMatchEtag = room.ETag }), room);
            return new RoomDto(room);
        }

        private void ValidateRoomName(IRoomUpdateDto request)
        {
            if (string.IsNullOrEmpty(request.Name) || request.Name.Length < 2 || request.Name.Length > 128)
            {
                throw new InvalidException(nameof(request.Name));
            }
        }

        public async Task<(RoomDto room, ApplicationUser user)> SetEndedAsync(int activeUsers, string userId)
        {
            var (room, user) = await GetByUserInternalAsync(userId);
            if (HasMediaEnded(room))
            {
                await _roomUserStateRepository.SetUserStateAsync(
                    user.Id,
                    room.Id,
                    35,
                    MediaPlayerStateType.Ended);

                var roomInfo = new RoomDto(room);
                if (roomInfo.CurrentMedia.ElapsedTime > (roomInfo.CurrentMedia.DurationAsSeconds + 30)
                    || activeUsers == await _roomUserStateRepository.GetUsersEndedCountAsync(room.Id))
                {
                    var nextItem = await _roomPlaylistItemRepository
                        .PopItemAsync(room.Id);

                    string nickName = "";
                    if(nextItem != null)
                    {
                        var nextItemOwner = await _userManager.FindByIdAsync(nextItem.UserId);
                        if(nextItemOwner != null)
                        {
                            nickName = nextItemOwner.Nickname;
                        }
                    }

                    room = await _roomRepository.UpdateAsync(room, r =>
                    {
                        r.CurrentMedia = nextItem != null ? 
                            new RoomCurrentMedia(nextItem.Media) : 
                            null;
                        if(r.CurrentMedia != null)
                        {
                            r.CurrentMedia.User = new Models.Areas.Common.UserIdentity()
                            {
                                Id = nextItem.UserId,
                                Name = nickName
                            };
                        }

                        return r;
                    });

                    return (new RoomDto(room),user);
                }
            }

            return (null,user);
        }

        /// <summary>
        /// Used before joining the room hub
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public async Task<(RoomDto room, ApplicationUser user)> JoinAsync(string userId, string roomId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                if(user.RoomId  != null && user.RoomId != roomId){
                    var oldRoom = await _roomRepository.ReadAsync(user.RoomId, user.RoomId);
                    if(oldRoom != null)
                    {
                        await LeaveAsync(oldRoom, user);
                    }
                }

                var room = await _roomRepository.ReadAsync(roomId, roomId);
                if(room == null)
                {
                    return (null, user);
                }

                user.RoomId = room.Id;
                await _userManager.UpdateAsync(user);

                return (new RoomDto(room), user);
            }

            return (null, user);
        }

        /// <summary>
        /// Used when connecting to the hub
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<(RoomDto room, ApplicationUser user)> JoinAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null && !string.IsNullOrEmpty(user.RoomId))
            {
                var room = await _roomRepository.ReadAsync(user.RoomId, user.RoomId);
                if (room == null)
                {
                    return (null, user);
                }

                string id = $"{userId}_{room.Id}";
                var roomUser = await _roomUserRepository.ReadAsync(id, room.Id);
                roomUser ??= new RoomUser(user.Id, room.Id);
                await _roomUserRepository.UpsertAsync(roomUser, ru => {
                    ru.Nickname = user.Nickname;
                    ru.IsActive = true;
                    return ru;
                });

                return (new RoomDto(room), user);
            }

            return (null, user);
        }

        public async Task<(RoomDto room, ApplicationUser user)> LeaveAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null && !string.IsNullOrEmpty(user.RoomId))
            {
                var room = await _roomRepository.ReadAsync(user.RoomId, user.RoomId);
                if(room == null)
                {
                    return (null, user);
                }

                await LeaveAsync(room, user);
                return (new RoomDto(room), user);
            }

            return (null,user);
        }

        private async Task LeaveAsync(Room room, ApplicationUser user)
        {
            string id = $"{user.Id}_{room.Id}";
            var roomUser = await _roomUserRepository.ReadAsync(id, room.Id);
            if(roomUser == null || !roomUser.IsActive)
            {
                await LeaveAsync(user);
                return;
            }

            await _roomUserRepository.UpdateAsync(roomUser, ru => {
                ru.IsActive = false;
                return ru;
            });

            await LeaveAsync(user);
        }

        private async Task LeaveAsync(ApplicationUser user)
        {
            if (user.RoomId != null)
            {
                user.RoomId = null;
                await _userManager.UpdateAsync(user);
            }
        }
    }
}
