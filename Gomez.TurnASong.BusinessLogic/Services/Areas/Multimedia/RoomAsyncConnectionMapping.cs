﻿using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Repositories.Areas.Identity;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Identity
{
    public class RoomConnectionMapping : CosmosConnectionMapping, IRoomConnectionMapping
    {
        public RoomConnectionMapping(IConnectionMapRepository store) : base(store, HubConstant.HUBNAME_ROOM)
        {

        }
    }
}
