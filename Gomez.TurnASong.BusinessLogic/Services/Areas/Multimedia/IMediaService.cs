﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.ViewModels.Areas.Common;
using Gomez.TurnASong.ViewModels.Areas.Multimedia.Media;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Multimedia
{
    public interface IMediaService
    {
        Task DeleteQueuedItemAsync(string id, string userId);
        Task<IContinuedList<PlaylistItemDto>> GetAllPlayedItemsAsync(string roomId, string continuationToken);
        Task<IContinuedList<PlaylistItemDto>> GetAllPlayedItemsByUserRoomAsync(string userId, string continuationToken);
        Task<IEnumerable<UserIdentityDto>> GetAllWaitingUsersAsync(string roomId);
        Task<IEnumerable<UserIdentityDto>> GetAllWaitingUsersByUserIdAsync(string userId);
        Task<IContinuedList<PlaylistItemDto>> GetMyItemQueueAsync(string userId, string continuationToken);
        Task<IContinuedList<PlaylistItemDto>> GetMyPlayedItemsAsync(string userId, string continuationToken);
        Task<IndexViewModel> IndexAsync(QueryIndexViewModel request);
    }
}