﻿using Gomez.TurnASong.ViewModels.Areas.Identity.Profiles;
using System.Threading.Tasks;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Identity
{
    public interface IProfileService
    {
        Task<ProfileDto> GetAsync(string id);
        Task<ProfileDto> PatchAsync(ProfilePatchDto request);
    }
}