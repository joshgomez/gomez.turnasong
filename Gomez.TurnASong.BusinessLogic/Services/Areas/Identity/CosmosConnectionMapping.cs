﻿using Gomez.Core.Cosmos.Web.Sockets;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.Repositories.Areas.Identity;
using Gomez.TurnASong.ViewModels.Areas.Identity.Users;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Identity
{
    public class CosmosConnectionMapping : 
        CosmosConnectionMapping<ConnectionData, ApplicationRole, ApplicationUser>, 
        IAsyncConnectionMapping
    {
        public CosmosConnectionMapping(IConnectionMapRepository store, string hubName) : base(store, hubName, 5)
        {

        }
    }
}
