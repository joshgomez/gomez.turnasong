﻿using Gomez.Core.Web.Sockets;
using Gomez.TurnASong.ViewModels.Areas.Identity.Users;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Identity
{
    public interface IAsyncConnectionMapping : IAsyncConnectionMapping<string, ConnectionData>
    {
    }
}
