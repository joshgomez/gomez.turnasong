﻿using Gomez.Core.Models.Exceptions;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.ViewModels.Areas.Identity.Profiles;
using System.Threading.Tasks;

namespace Gomez.TurnASong.BusinessLogic.Services.Areas.Identity
{
    public class ProfileService : IProfileService
    {
        private readonly ApplicationUserManager _userManager;

        public ProfileService(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task<ProfileDto> GetAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            return new ProfileDto(user);
        }

        public async Task<ProfileDto> PatchAsync(ProfilePatchDto request)
        {
            if (request == null || string.IsNullOrEmpty(request.Id))
            {
                throw new BadRequestException(nameof(request.Id));
            }

            var user = await _userManager.FindByIdAsync(request.Id);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            if (string.IsNullOrEmpty(request.Nickname) || request.Nickname.Length < 2 || request.Nickname.Length > 64)
            {
                throw new InvalidException(nameof(request.Nickname));
            }

            user.Nickname = request.Nickname;
            await _userManager.UpdateAsync(user);

            return new ProfileDto(user);
        }
    }
}
