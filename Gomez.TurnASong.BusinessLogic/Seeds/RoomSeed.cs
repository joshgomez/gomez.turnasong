﻿using Gomez.TurnASong.Configurations.Constants.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Repositories.Areas.Multimedia;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.TurnASong.BusinessLogic.Seeds
{
    public static class RoomSeed
    {
        public static async Task RunAsync(IRoomRepository repo, UserManager<ApplicationUser> userManager)
        {
            var user = await userManager.FindByNameAsync(UserConstant.USERNAME_ADMIN);
            var rooms = new List<Room>(){
                new Room()
                {
                    Id = "c9760820-1b75-4d77-8e82-fd5e60e603b7",
                    Name = "Mixed",
                    Owner = new Models.Areas.Common.UserIdentity()
                    {
                        Id = user.Id,
                        Name = string.IsNullOrEmpty(user.Nickname) ? user.UserName : user.Nickname
                    }
                },
                new Room()
                {
                    Id = "06afbc4b-a0a6-4056-8b6f-82d4be210f72",
                    Name = "Pop",
                    Owner = new Models.Areas.Common.UserIdentity()
                    {
                        Id = user.Id,
                        Name = string.IsNullOrEmpty(user.Nickname) ? user.UserName : user.Nickname
                    }
                },
                new Room()
                {
                    Id = "be000092-e276-4519-b69b-fa6f8e1dc5f3",
                    Name = "Rock",
                    Owner = new Models.Areas.Common.UserIdentity()
                    {
                        Id = user.Id,
                        Name = string.IsNullOrEmpty(user.Nickname) ? user.UserName : user.Nickname
                    }
                }
            };

            foreach (var room in rooms)
            {
                if (!await repo.RoomIdExistsAsync(room.Id))
                    await repo.CreateAsync(room);
            }
        }
    }
}
