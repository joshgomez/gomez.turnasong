﻿using Gomez.Core.Configurations.Constants.Areas.Identity;
using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Configurations.Constants.Areas.Identity;
using Gomez.TurnASong.Models.Areas.Identity;
using Microsoft.AspNetCore.Identity;

namespace Gomez.TurnASong.BusinessLogic.Seeds
{
    public class ApplicationDbInitializer : Core.Cosmos.Models.Contexts.ApplicationDbInitializer<ApplicationUser,ApplicationRole>
    {
        private readonly IDocumentDbContext _db;

        public ApplicationDbInitializer(
            IDocumentDbContext db,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager) : base(userManager, roleManager)
        {
            _db = db;
            this.AddRole(RoleConstant.SUPER_ADMINISTRATOR);
            this.AddUser(UserConstant.USERNAME_ADMIN, "Password!1", RoleConstant.SUPER_ADMINISTRATOR);
        }
    }
}
