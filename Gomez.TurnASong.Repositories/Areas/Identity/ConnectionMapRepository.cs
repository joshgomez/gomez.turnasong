﻿using Gomez.Core.Cosmos.Repositories;
using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Models.Areas.Identity;

namespace Gomez.TurnASong.Repositories.Areas.Identity
{
    public class ConnectionMapRepository : ConnectionMapRepository<ApplicationUser, ApplicationRole>, IConnectionMapRepository
    {
        public ConnectionMapRepository(IDocumentDbContext context, string collectionName, string hubName) : base(context, collectionName, hubName)
        {
        }
    }
}
