﻿using Gomez.Core.Cosmos.Repositories;
using Gomez.TurnASong.Models.Areas.Identity;

namespace Gomez.TurnASong.Repositories.Areas.Identity
{
    public interface IConnectionMapRepository : IConnectionMapRepository<ApplicationUser, ApplicationRole>
    {
    }
}
