﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Models.Areas.Multimedia;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public interface IRoomUserStateRepository : IDocumentRepository<RoomUserState>
    {
        Task<int> GetUsersEndedCountAsync(string roomId);
        Task<int> GetUsersReadyCountAsync(string roomId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roomId"></param>
        /// <param name="durationAsSeconds">The state will be deleted after the amount of seconds</param>
        /// <param name="state"></param>
        /// <returns></returns>
        Task SetUserStateAsync(string userId,
            string roomId,
            double durationAsSeconds,
            MediaPlayerStateType state);
    }
}
