﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Media;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public interface IRoomPlaylistItemRepository : IDocumentRepository<RoomPlaylistItem>
    {
        Task<int> CountMyQueuedItemsAsync(string userId, string roomId);
        Task DeleteQueuedItemAsync(string id, string roomId, string userId);
        Task<IContinuedList<PlaylistItem>> GetAllPlayedItemsAsync(string roomId, string continuationToken);
        Task<IEnumerable<string>> GetAllWaitingUserIdsAsync(string roomId, int maxItems);
        Task<IContinuedList<PlaylistItem>> GetMyItemsAsync(string userId, string roomId, bool hasPlayed, string continuationToken);
        Task<RoomPlaylistItem> PopItemAsync(string roomId);
    }
}
