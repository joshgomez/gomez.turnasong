﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Room;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public class RoomUserRepository : TypedDocumentRepository<RoomUser>, IRoomUserRepository
    {
        public RoomUserRepository(IDocumentDbContext context) : base(
            context,
            DatebaseConstant.COLLECTION_APPLICATION,
            x => x.PartitionKey,typeof(RoomUser).Name)
        {
        }

        public IAsyncEnumerable<RoomUserListItem> GetAllActiveUsersInRoomAsync(string roomId)
        {
            var query = GetQuery(new QueryOptions() { PartitionKey = roomId })
                .Where(x => x.IsActive == true)
                .Select(x => new RoomUserListItem() { 
                    Id = x.Id.Substring(0,x.Id.IndexOf('_')),
                    NickName = x.Nickname
                });

            return _context.ToAsyncEnumerable(query);
        }

        public async Task<bool> GetIsUsersActiveAsync(string roomId)
        {
            var query = GetQuery(new QueryOptions() { PartitionKey = roomId })
                .Where(x => x.IsActive == true)
                .Select(x => x.Id);

            return (await _context.FirstOrDefaultAsync(query)) != null;
        }

        public async Task<RoomUser> LeaveAsync(string userId, string roomId)
        {
            string id = $"{userId}_{roomId}";

            var roomUser = await ReadFromQueryAsync(
                new QueryOptions() { PartitionKey = roomId }, 
                new ItemOptions() { PartitionKey = roomId }
                ,x => x.IsActive == true && x.Id == id);

            return await UpdateAsync(roomUser,u => {
                u.IsActive = false;
                return u;
            });
        }

        public async Task<Dictionary<string, string>> GetAllNicknamesByUserIdsAsync(IList<string> userIds, string roomId)
        {
            var ids = new string[userIds.Count];
            for (int i = 0; i < userIds.Count; i++)
            {
                ids[i] = $"{userIds[i]}_{roomId}";
            }

            var query = GetQuery(new QueryOptions() { PartitionKey = roomId })
                .Where(x => ids.Contains(x.Id))
                .Select(x => new { x.Id, x.Nickname });

            return await _context.ToDictionaryAsync(query, x => x.Id.Substring(0,x.Id.Length - $"_{roomId}".Length), x => x.Nickname);
        }
    }
}
