﻿using Gomez.Core.DocumentDb;
using Gomez.Core.Models.Exceptions;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Media;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public class RoomPlaylistItemRepository : TypedDocumentRepository<RoomPlaylistItem>, IRoomPlaylistItemRepository
    {
        public RoomPlaylistItemRepository(IDocumentDbContext context) : base(
            context,
            DatebaseConstant.COLLECTION_APPLICATION,
            x => x.PartitionKey,typeof(RoomPlaylistItem).Name)
        {
        }

        public async Task<RoomPlaylistItem> PopItemAsync(string roomId)
        {
            var query = GetQuery(new QueryOptions() { PartitionKey = roomId, MaxItemCount = 1 })
                .Where(x => x.DeletedAt == null)
                .OrderByDescending(x => x.DeletedAt)
                .ThenBy(x => x.OrderNumber)
                .ThenBy(x => x.CreatedAt); //composite index

            var item = await _context.FirstOrDefaultAsync(query);
            if(item != null)
            {
                return await UpdateAsync(item, i =>
                {
                    i.DeletedAt = System.DateTime.UtcNow;
                    return i;
                });
            }

            return null;
        }

        private IQueryable<PlaylistItem> ToMappedObject(IQueryable<RoomPlaylistItem> query)
        {
            return query.Select(x => new PlaylistItem()
            {
                Id = x.Id,
                VideoId = x.Media.Id,
                Thumbnail = x.Media.Thumbnails.StandardUrl,
                Title = x.Media.Title,
                DurationAsSeconds = x.Media.DurationAsSeconds,
                UserId = x.UserId
            });
        }

        private IQueryable<PlaylistItem> ToMyMappedObject(IQueryable<RoomPlaylistItem> query)
        {
            return query.Select(x => new PlaylistItem()
            {
                Id = x.Id,
                VideoId = x.Media.Id,
                Thumbnail = x.Media.Thumbnails.StandardUrl,
                Title = x.Media.Title,
                DurationAsSeconds = x.Media.DurationAsSeconds
            });
        }

        public async Task<IContinuedList<PlaylistItem>> GetAllPlayedItemsAsync(string roomId, string continuationToken)
        {
            var query = GetQuery(new QueryOptions() { 
                PartitionKey = roomId, 
                MaxItemCount = 50, 
                RequestContinuation = continuationToken 
            })
                .Where(x => x.DeletedAt != null)
                .OrderByDescending(x => x.CreatedAt);

            return await _context.ToContinuedListAsync(ToMappedObject(query));
        }

        public async Task<IEnumerable<string>> GetAllWaitingUserIdsAsync(string roomId, int maxItems)
        {
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = roomId,
                MaxItemCount = maxItems
            })
                .Where(x => x.DeletedAt == null)
                .OrderByDescending(x => x.DeletedAt)
                .ThenBy(x => x.OrderNumber)
                .ThenBy(x => x.CreatedAt); //composite index

            var results = await _context.ToListAsync(query.Select(x => x.UserId).Take(maxItems));
            return results.Distinct();
        }

        public async Task<IContinuedList<PlaylistItem>> GetMyItemsAsync(string userId, string roomId, bool hasPlayed, string continuationToken)
        {
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = roomId,
                MaxItemCount = 50,
                RequestContinuation = continuationToken
            })
                .Where(x => x.UserId == userId);

            if (hasPlayed)
            {
                query = query.Where(x => x.DeletedAt != null);
            }
            else
            {
                query = query.Where(x => x.DeletedAt == null);
            }

            query = query
                .OrderBy(x => x.UserId)
                .ThenByDescending(x => x.CreatedAt); //composite index

            return await _context.ToContinuedListAsync(ToMyMappedObject(query));
        }

        public async Task<int> CountMyQueuedItemsAsync(string userId, string roomId)
        {
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = roomId,
            })
                .Where(x => x.UserId == userId && x.DeletedAt == null);

            query = query
                .OrderBy(x => x.UserId)
                .ThenByDescending(x => x.DeletedAt); //composite index

            return await _context.CountAsync(query);
        }

        public async Task DeleteQueuedItemAsync(string id, string roomId, string userId)
        {
            var item = await ReadAsync(id, roomId);
            if (item == null)
            {
                throw new NotFoundException(id);
            }

            if (!string.IsNullOrEmpty(userId) && item.UserId != userId)
            {
                throw new AccessDeniedException(nameof(userId));
            }

            if(item.DeletedAt != null)
            {
                throw new CompletedException(item.Media.Id);
            }

            await DeleteAsync(item);
        }
    }
}
