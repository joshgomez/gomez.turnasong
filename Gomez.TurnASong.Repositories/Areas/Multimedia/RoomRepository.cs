﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.Lookups;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Room;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public class RoomRepository : TypedDocumentRepository<Room>, IRoomRepository
    {
        private readonly IRoomLookupRepository _lookupRepo;
        protected RoomRepository(IDocumentDbContext context) : base(
            context, 
            DatebaseConstant.COLLECTION_APPLICATION, 
            x => x.PartitionKey,
            typeof(Room).Name)
        {

        }

        public RoomRepository(IDocumentDbContext context, IRoomLookupRepository roomLookupRepository) : this(context)
        {
            _lookupRepo = roomLookupRepository;
        }

        public async Task<bool> RoomIdExistsAsync(string id)
        {
            var query = GetQuery(new QueryOptions() { PartitionKey = id, MaxItemCount = 1 })
                .Where(x => x.Id == id).Select(x => x.Id);
            return (await _context.FirstOrDefaultAsync(query)) != null;
        }

        public async Task<IContinuedList<RoomListItem>> IndexAsync(string search, string continuesToken)
        {
            var query = _lookupRepo
                .GetQuery(new QueryOptions() 
                { 
                    PartitionKey = typeof(RoomLookup).Name, 
                    MaxItemCount = 30,
                    RequestContinuation = continuesToken
                });

            if (!string.IsNullOrEmpty(search))
            {
                search = search.ToUpperInvariant();
                query = query.Where(x => x.NormalizedName.Contains(search));
            }

            var resultQuery = query.Select(x =>
                new RoomListItem()
                {
                    Id = x.Id.Substring(0, x.Id.Count() - "_lookup".Count()),
                    Name = x.Name,
                    Thumbnail = x.CurrentMedia.Thumbnails.StandardUrl,
                    OnlineCount = x.OnlineCount
                });

            var result =  await _context.ToContinuedListAsync(resultQuery);
            return new ContinuedList<RoomListItem>()
            {
                ContinuesToken = result.ContinuesToken,
                Documents = result.Documents
            };
        }

        public new async Task<Room> CreateAsync(Room entity)
        {
            var lookup = new RoomLookup(entity);
            await _lookupRepo.CreateAsync(lookup);
            return await base.CreateAsync(entity);
        }

        public new async Task DeleteAsync(Room entity)
        {
            var transaction = _context.CreateTransaction(_collectionName, entity.Id);
            var queryIds = _context.GetQuery<IDocumentEntity>(_collectionName, new QueryOptions() { PartitionKey = entity.Id })
                .Select(x => x.Id);

            var ids = await _context.ToListAsync(queryIds);

            foreach (var id in ids)
            {
                transaction = transaction.DeleteItem(id, new TransactionOptions() { EnableContentResponseOnWrite = false });
            }


            await foreach (var item in _context.ExecuteTransactionAsync(transaction))
            {
                //Completed
            }

            await _context.DeleteAsync<RoomLookup>(_collectionName, entity.Id + "_lookup", new ItemOptions() { PartitionKey = nameof(RoomLookup) });
        }
    }
}
