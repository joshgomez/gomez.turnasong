﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.Lookups;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public class RoomLookupRepository : TypedDocumentRepository<RoomLookup>, IRoomLookupRepository
    {
        private readonly string roomTypeName = typeof(Room).Name;

        public RoomLookupRepository(IDocumentDbContext context) : base(
            context,
            DatebaseConstant.COLLECTION_APPLICATION,
            x => x.PartitionKey,
            typeof(RoomLookup).Name)
        {
        }

        public Task<IContinuedList<string>> GetAllActiveRoomIdsAsync(string continuationToken = null, int maxItems = 100)
        {
            var inActiveDate = DateTime.UtcNow.Date.AddDays(-5);
            var query = GetQuery(new QueryOptions() 
                {
                        MaxItemCount = maxItems, 
                        RequestContinuation = continuationToken,
                        PartitionKey = nameof(RoomLookup)
                })
                .Where(x => x.UpdatedAt >= inActiveDate)
                .Select(x => x.Id.Substring(0, x.Id.Count() - "_lookup".Count()));

            return _context.ToContinuedListAsync(query);
        }



        public async Task<RoomLookup> SyncLookupAsync(Room entity)
        {
            if (entity.Id.EndsWith("_lookup") || roomTypeName != entity.Type)
            {
                return null;
            }

            var lookup = await ReadAsync(entity.Id + "_lookup", nameof(RoomLookup));
            bool requireUpdate = false;
            if (
                entity.Name != lookup.Name ||
                entity.CurrentMedia?.StartTime != lookup.CurrentMedia?.StartTime ||
                entity.CurrentMedia?.Id != lookup.CurrentMedia?.Id)
            {
                requireUpdate = true;
            }

            if (!requireUpdate)
            {
                return lookup;
            }

            return await UpdateAsync(lookup, l => {
                l.Name = entity.Name;
                l.CurrentMedia = entity.CurrentMedia;
                return l;
            });
        }

        public async Task<RoomLookup> UpdateOnlineCountAsync(string roomId, int online)
        {
            if (string.IsNullOrEmpty(roomId))
            {
                return null;
            }

            var lookup = await ReadAsync(roomId + "_lookup", nameof(RoomLookup));
            return await UpdateAsync(lookup, l => {
                l.OnlineCount = online;
                return l;
            });
        }
    }
}
