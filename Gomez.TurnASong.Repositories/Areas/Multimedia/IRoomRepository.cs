﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Room;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public interface IRoomRepository : IDocumentRepository<Room>
    {
        Task<IContinuedList<RoomListItem>> IndexAsync(string search, string continuesToken);
        Task<bool> RoomIdExistsAsync(string id);
    }
}
