﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Configurations.Constants;
using Gomez.TurnASong.Models.Areas.Multimedia;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public class RoomUserStateRepository : TypedDocumentRepository<RoomUserState>, IRoomUserStateRepository
    {
        public RoomUserStateRepository(IDocumentDbContext context) : base(
            context,
            DatebaseConstant.COLLECTION_APPLICATION,
            x => x.PartitionKey,typeof(RoomUserState).Name)
        {
        }

        public async Task<int> GetUsersReadyCountAsync(string roomId)
        {
            return await GetUsersCountAsync(roomId, MediaPlayerStateType.Ready);
        }

        public async Task<int> GetUsersEndedCountAsync(string roomId)
        {
            return await GetUsersCountAsync(roomId, MediaPlayerStateType.Ended);
        }

        private async Task<int> GetUsersCountAsync(string roomId, MediaPlayerStateType state)
        {
            var query = GetQuery(new QueryOptions() { PartitionKey = roomId })
                .Where(x => x.MediaPlayerState == state);
            return await _context.CountAsync(query);
        }

        public async Task SetUserStateAsync(
            string userId,
            string roomId,
            double durationAsSeconds,
            MediaPlayerStateType state)
        {
            await UpsertAsync(new RoomUserState(userId, roomId)
            {
                MediaPlayerState = state,
                TimeToLive = Convert.ToInt32(durationAsSeconds)
            });
        }
    }
}
