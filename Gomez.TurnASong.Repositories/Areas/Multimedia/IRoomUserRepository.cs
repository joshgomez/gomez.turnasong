﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.MappingObjects.Room;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public interface IRoomUserRepository : IDocumentRepository<RoomUser>
    {
        IAsyncEnumerable<RoomUserListItem> GetAllActiveUsersInRoomAsync(string roomId);
        Task<Dictionary<string, string>> GetAllNicknamesByUserIdsAsync(IList<string> userIds, string roomId);
        Task<bool> GetIsUsersActiveAsync(string roomId);
        Task<RoomUser> LeaveAsync(string userId, string roomId);
    }
}
