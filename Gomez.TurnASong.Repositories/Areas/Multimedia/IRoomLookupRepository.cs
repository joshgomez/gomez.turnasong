﻿using Gomez.Core.DocumentDb;
using Gomez.TurnASong.Models.Areas.Multimedia;
using Gomez.TurnASong.Models.Areas.Multimedia.Lookups;
using System.Threading.Tasks;

namespace Gomez.TurnASong.Repositories.Areas.Multimedia
{
    public interface IRoomLookupRepository : IDocumentRepository<RoomLookup>
    {
        Task<IContinuedList<string>> GetAllActiveRoomIdsAsync(string continuationToken = null, int maxItems = 100);
        Task<RoomLookup> SyncLookupAsync(Room entity);
        Task<RoomLookup> UpdateOnlineCountAsync(string roomId, int online);
    }
}
